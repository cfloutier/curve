﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Curves.Tools.MeshFusion.Editor
{
    public class GUITools
    {
        public static string GUILayoutPath(string label, string curFile, string[] extensions, bool relative, bool save)
        {
            GUILayout.BeginHorizontal();
            string result = EditorGUILayout.TextField(label, curFile, GUILayout.ExpandWidth(true));
            if (GUILayout.Button("..."))
            {
                result = Browse(result, extensions, relative, save);
            }
            GUILayout.EndHorizontal();
            return result;
        }

        /// <summary>
        /// Rebuild an url removing ., .. useless / ...
        /// </summary>
        /// <param name="basepath">the base path ex http://myserver/dir</param>
        /// <param name="relativePath">relative path using ../ etc</param>
        /// <returns>a valid url</returns>
        public static string concatUrl(string basepath, string relativePath)
        {
            //Debug.Log("concatUrl basepath:" + basepath);
            //Debug.Log("concatUrl relativePath:" + relativePath);

            relativePath = relativePath.Replace('\\', '/');
            string[] listBase = basepath.Split('/');
            List<string> list = new List<string>();

            string[] listRel = relativePath.Split('/');
            for (int i = 0; i < listBase.Length; i++)
            {
                //if (listBase[i].Length > 0)
                list.Add(listBase[i]);
            }

            for (int i = 0; i < listRel.Length; i++)
            {
                string str = listRel[i];
                if (str == "..")
                {
                    list.RemoveAt(list.Count - 1);
                }
                else if (str != "." && str.Length > 0)
                {
                    list.Add(listRel[i]);
                }
            }

            string res = "";
            for (int i = 0; i < list.Count - 1; i++)
            {
                res += (list[i] as string) + "/";
            }
            res += (list[list.Count - 1] as string);
            //Debug.Log("concatUrl res:" + res);
            return res;
        }

        public static string BuildRelativePath(string folderAbsPath, string fileAbsPath)
        {
            fileAbsPath = fileAbsPath.Replace('\\', '/');
            folderAbsPath = folderAbsPath.Replace('\\', '/');
            string[] directoriesFile = fileAbsPath.Split('/');
            string[] directoriesFolder = folderAbsPath.Split('/');

            if (directoriesFile.Length == 0 || directoriesFolder.Length == 0 || directoriesFolder[0] != directoriesFile[0])
                return fileAbsPath; // no common parent path
            int i = 1;
            for (; i < directoriesFolder.Length; i++)
            {
                if (directoriesFolder[i] != directoriesFile[i])
                    break;
            }

            string relativePath = "";

            for (int j = i; j < directoriesFolder.Length; j++)
            {
                relativePath += "../";
            }

            for (int j = i; j < directoriesFile.Length; j++)
            {
                relativePath += directoriesFile[j];
                if (j != directoriesFile.Length - 1)
                    relativePath += "/";
            }

            return relativePath;
        }

        public static string Browse(string curFile, string[] extensions, bool relative, bool save)
        {
            //   Debug.Log("relative " + relative);

            //extension = extension.ToLower();
            bool dirMode = false;

            for (int i = 0; i < extensions.Length; i++)
                extensions[i] = extensions[i].ToLower();

            if (extensions.Length == 1)
            {
                string ex = extensions[0];
                if (ex == "folder" || ex == "directory" || ex == "dir")
                    dirMode = true;
            }

            string directory = "";
            string filePath = curFile.Replace('\\', '/');
            int lastSlash = filePath.LastIndexOf('/');
            if (curFile == "" || lastSlash == -1)
            {
                directory = Application.dataPath;
            }
            else
            {
                filePath = filePath.Substring(0, lastSlash);

                if (filePath.StartsWith("Assets/"))
                {
                    directory = Application.dataPath + filePath.Substring(6);
                }
                else if (filePath.StartsWith(".."))
                {
                    directory = concatUrl(Application.dataPath, filePath);
                }
                else
                    directory = filePath;
            }

            //directory = "C:/dev/Visualisation3D/ProjetEntreDeux/Assets/Layers_Carte/";
            //Debug.Log("directory " + directory);
            string path = "";

            if (dirMode)
                path = EditorUtility.OpenFolderPanel("Select Directory", directory, "");
            else
            {
                if (extensions.Length == 1)
                {
                    if (save)
                        path = EditorUtility.SaveFilePanel("Select file (." + extensions[0] + ")", directory, "fileName", extensions[0]);
                    else
                        path = EditorUtility.OpenFilePanel("Select file (." + extensions[0] + ")", directory, extensions[0]);
                }
                else
                {
                    // create filters
                    string[] filters = new string[extensions.Length * 2 + 2];
                    filters[0] = "All Supported files";
                    filters[1] = string.Join(",", extensions);
                    for (int i = 0; i < extensions.Length; i++)
                    {
                        filters[i * 2 + 2] = "." + extensions[i] + " Files";
                        filters[i * 2 + 3] = extensions[i];
                    }

                    path = EditorUtility.OpenFilePanelWithFilters("Select file", directory, filters);
                }
            }

            if (path == "")
            {
                return curFile;
            }
            //Debug.Log("path " + path);

            if (relative)
            {
                path = BuildRelativePath(Application.dataPath, path);
            }


            return path;
        }

    }

}