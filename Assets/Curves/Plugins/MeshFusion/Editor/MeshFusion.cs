﻿
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


namespace Curves.Tools.MeshFusion.Editor
{
    public class MeshFusion : EditorWindow
    {
        [MenuItem("Tools/Mesh Fusion/Open windows", false, 0)]
        static void Init()
        {
            // Get existing open window or if none, make a new one:
            MeshFusion window = (MeshFusion)EditorWindow.GetWindow(typeof(MeshFusion));
            window.titleContent = new GUIContent( "Mesh - Fusion");
            window.Show();
        }

        GameObject sourceObject;
        GameObject destObject;
        
        enum FusionMode
        {
            NoFusion,
            SingleFusion,
            MultiFusion,
        }

        FusionMode fusionMode = FusionMode.SingleFusion;

        bool saveResult = true;
        bool moveMaterialToSavePath = true;
        string savePath = "";
        string finalSavePath = "";
        bool savePrefab = true;
        bool saveMultiPrefabs = true;
        //bool saveObjectStructure = false;

        void OnGUI()
        {
            GUILayout.Label("Mesh Fusion", EditorStyles.boldLabel);
            sourceObject = (GameObject)EditorGUILayout.ObjectField("Source Scene Object", sourceObject, typeof(GameObject), true);

            if (sourceObject == null)
                return;

            if (sourceObject.scene.name == null)
            {
                GUILayout.Label("Error, only Object from an active scene can be fusionned");
                return;
            }

            GUILayout.Space(5);
            GUILayout.Label("Options :");

            fusionMode = (FusionMode)EditorGUILayout.EnumPopup("Fusion Mode", fusionMode);

            saveResult = GUILayout.Toggle(saveResult, "Save Assets");
            if (saveResult)
            {
                savePath = GUITools.GUILayoutPath("Save Directory Path", savePath, new string[] { "dir" }, true, false);               
                moveMaterialToSavePath = GUILayout.Toggle(moveMaterialToSavePath, "Move Existing Assets to 'Save Directory'");
                savePrefab = GUILayout.Toggle(savePrefab, "Build Prefab'");
                if (savePrefab && fusionMode == FusionMode.MultiFusion)
                {
                    saveMultiPrefabs = GUILayout.Toggle(saveMultiPrefabs, "Save Multiple prefabs'");
                }
            }

            if (GUILayout.Button("Apply", GUILayout.Height(30)))
                Apply();
        }

        void OnEnable()
        {
            LoadPrefs();
        }

        void OnDisable()
        {
            savePrefs();
        }

        void LoadPrefs()
        {
            fusionMode = (FusionMode) EditorPrefs.GetInt("MeshFusion.fusionMode", (int) FusionMode.SingleFusion);
            saveResult = EditorPrefs.GetBool("MeshFusion.saveResult", true);
            moveMaterialToSavePath = EditorPrefs.GetBool("MeshFusion.moveMaterialToSavePath", moveMaterialToSavePath);
            savePath = EditorPrefs.GetString("MeshFusion.savePath", "FusionResult");
            savePrefab = EditorPrefs.GetBool("MeshFusion.savePrefab", true);
            saveMultiPrefabs = EditorPrefs.GetBool("MeshFusion.saveMultiPrefabs", true);
        }

        void savePrefs()
        {
             EditorPrefs.SetInt("MeshFusion.fusionMode", (int) fusionMode);
            saveResult = EditorPrefs.GetBool("MeshFusion.saveResult", saveResult);
            moveMaterialToSavePath = EditorPrefs.GetBool("MeshFusion.moveMaterialToSavePath", moveMaterialToSavePath);
            savePath = EditorPrefs.GetString("MeshFusion.savePath", savePath);
            savePrefab = EditorPrefs.GetBool("MeshFusion.savePrefab", savePrefab);
            saveMultiPrefabs = EditorPrefs.GetBool("MeshFusion.saveMultiPrefabs", saveMultiPrefabs);

        }

        void buildDestination()
        {
            destObject = new GameObject(sourceObject.name);

            destObject.transform.parent = sourceObject.transform.parent;
            destObject.transform.localPosition = sourceObject.transform.localPosition;
            destObject.transform.localRotation = sourceObject.transform.localRotation;
            destObject.transform.localScale = sourceObject.transform.localScale;
            destObject.name = sourceObject.name + "_fusion";
            sourceObject.SetActive(false);
        }

        void Apply()
        {
            savePrefs();
            buildDestination();

            switch (fusionMode)
            {
                default:
                case FusionMode.NoFusion:
                    break;
                case FusionMode.SingleFusion:
                    SingleFusion();
                    break;
                case FusionMode.MultiFusion:
                    MultiFusion();
                    break;   
            }

            if (saveResult)
            {
                finalSavePath = "Assets/" + savePath + "/" + sourceObject.name;

                CreateDirectoryRecursive(Application.dataPath + "/" + savePath + "/" + sourceObject.name + "/Textures");
                CreateDirectoryRecursive(Application.dataPath + "/" + savePath + "/" + sourceObject.name + "/Meshes");
                CreateDirectoryRecursive(Application.dataPath + "/" + savePath + "/" + sourceObject.name + "/Materials");

                saveMaterials();
                saveMeshes();

                if (savePrefab)
                {
                    switch (fusionMode)
                    {
                        case FusionMode.NoFusion:
                        case FusionMode.SingleFusion:
                            BuildSinglePrefab();
                            break;
                        case FusionMode.MultiFusion:
                            if (saveMultiPrefabs)
                                BuildMultiPrefabs();
                            else
                                BuildSinglePrefab();
                            break;
                        default:
                            break;
                    }

                }
            }
        }

        int totalProgress = 0;
        int progress = 0;
        bool canceled = false;

        public List<MeshFilter> ListMeshFilter(Transform tr)
        {
            MeshFilter[] filters = tr.GetComponentsInChildren<MeshFilter>();
            totalProgress += filters.Length;
            List < MeshFilter > result = new List<MeshFilter>();
            result.AddRange(filters);
            return result;
        }

        protected bool onProgress()
        {
            float Progress = (float)progress / totalProgress;
            string text = progress + " / " + totalProgress;

            canceled = false;
            if (Progress < 0 || Progress >= 1)
            {
                EditorUtility.ClearProgressBar();
                return false;
            }

            canceled = EditorUtility.DisplayCancelableProgressBar("Progression", text, Progress);

            if (canceled)
                EditorUtility.ClearProgressBar();

            return canceled;
        }

        void SingleFusion()
        {
            List<MeshFilter> meshes = ListMeshFilter(sourceObject.transform);
            destObject.transform.copyPosition(sourceObject.transform);

            MeshMerge.Merge(meshes, destObject.name, destObject.transform);
        }

        void MultiFusion()
        {
            List<List<MeshFilter>> lists = new List<List<MeshFilter>>();
            for (int i = 0; i < sourceObject.transform.childCount; i++)
            {
                List<MeshFilter> list = ListMeshFilter(sourceObject.transform.GetChild(i));
                lists.Add(list);
            }

            try
            {
                for (int i = 0; i < sourceObject.transform.childCount; i++)
                {
                    List<MeshFilter> list = lists[i];
                    Transform trSource = sourceObject.transform.GetChild(i);
                   
                    GameObject child = new GameObject();
                    child.name = trSource.name;
                   
                    child.transform.parent = destObject.transform;
                    child.transform.copyPosition(trSource);
                  

                    MeshMerge.Merge(list, trSource.name, child.transform);
                }
            }
            finally
            {
                EditorUtility.ClearProgressBar();
            }
        }




        void saveTextures(Material m)
        {
            Shader shader = m.shader;

            for (int i = 0; i < ShaderUtil.GetPropertyCount(shader); i++)
            {
                if (ShaderUtil.GetPropertyType(shader, i) == ShaderUtil.ShaderPropertyType.TexEnv)
                {
                    Texture2D texture = m.GetTexture(ShaderUtil.GetPropertyName(shader, i)) as Texture2D;

                    if (texture != null)
                    {
                        string texturePath = AssetDatabase.GetAssetPath(texture);
                        if (string.IsNullOrEmpty(texturePath))
                        {
                            GUID id = GUID.Generate();

                            System.IO.File.WriteAllBytes(texturePath, texture.EncodeToPNG());
                            texturePath = finalSavePath + "/Textures/" + id + ".png";

                            AssetDatabase.ImportAsset(texturePath);
                            texture = (Texture2D)AssetDatabase.LoadMainAssetAtPath(texturePath);

                            m.SetTexture(ShaderUtil.GetPropertyName(shader, i), texture);
                        }
                        else if (moveMaterialToSavePath)
                        {
                            string dest = finalSavePath + "/Textures/" + texturePath.Substring(texturePath.LastIndexOf('/') + 1);
                            if (texturePath != dest)
                            {
                                Debug.Log("Moved asset to " + dest);
                                AssetDatabase.MoveAsset(texturePath, dest);
                            }
                        }
                    }

                }
            }
        }

        void saveMaterial(Material m)
        {
            string path = AssetDatabase.GetAssetPath(m);
            if (string.IsNullOrEmpty(path))
            {
                GUID id = GUID.Generate();

                // not in base, just save
                path = finalSavePath + "/Materials/" + id + ".mat";
                AssetDatabase.DeleteAsset(path);
                AssetDatabase.CreateAsset(m, path);
            }
            else if (moveMaterialToSavePath)
            {
                string dest = finalSavePath + "/Materials/" + path.Substring(path.LastIndexOf('/') + 1);
                if (path != dest)
                {
                    Debug.Log("Moved asset to " + dest);
                    AssetDatabase.MoveAsset(path, dest);
                }
            }
        }

        void findComponents<T>(List<T> list, Transform tr) where T : Component
        {
            T component = tr.GetComponent<T>();
            if (component != null)
                list.Add(component);

            for (int i = 0; i < tr.childCount; i++)
            {
                Transform trChild = tr.GetChild(i);
                /*   if (Filter &&
                       DontAdd.IndexOf(trChild.name) != -1)
                       continue;*/

                findComponents<T>(list, trChild);
            }
        }

        public static void CreateDirectoryRecursive(string fullPath)
        {
            if (!System.IO.Directory.Exists(fullPath))
            {
                string[] dirs = fullPath.Split('/');
                string path = dirs[0];
                for (int i = 1; i < dirs.Length; i++)
                {
                    path += "/" + dirs[i];
                    //  Debug.Log("path " + path);
                    if (!System.IO.Directory.Exists(path))
                    {
                        System.IO.Directory.CreateDirectory(path);
                    }
                }
            }
        }

        void saveMaterials()
        {
            if (canceled) return;

            AssetDatabase.Refresh();

            List<MeshRenderer> renderers = new List<MeshRenderer>();
            findComponents<MeshRenderer>(renderers, destObject.transform);
            totalProgress = renderers.Count;
            progress = 0;

            try
            {
                for (progress = 0; progress < renderers.Count; progress++)
                {
                    var renderer = renderers[progress];
                    Material[] mats = renderer.sharedMaterials;
                    for (int j = 0; j < mats.Length; j++)
                    {
                        Material m = mats[j];
                        if (m == null)
                            continue;

                        saveTextures(m);
                        saveMaterial(m);
                    }

                    if (onProgress())
                        return;
                }
            }
            finally
            {
                EditorUtility.ClearProgressBar();
            }
        }

 
        void saveMeshes()
        {
            if (canceled) return;
            

            List<MeshFilter> filters = new List<MeshFilter>();
            findComponents<MeshFilter>(filters, destObject.transform);

            totalProgress = filters.Count;
            try
            {
                for (progress = 0; progress < totalProgress; progress++)
                {
                    var filter = filters[progress];
                    if (filter == null)
                        continue;

                    Mesh m = filter.sharedMesh;
                    if (m == null)
                        continue;

                    string path = AssetDatabase.GetAssetPath(m);
                    if (string.IsNullOrEmpty(path))
                    {
                        GUID id = GUID.Generate();

                        // not in base, just save
                        path = finalSavePath + "/Meshes/" + id + ".asset";
                        AssetDatabase.DeleteAsset(path);
                        AssetDatabase.CreateAsset(m, path);
                    }
                    else if (moveMaterialToSavePath)
                    {
                        string dest = finalSavePath + "/Meshes/" + path.Substring(path.LastIndexOf('/') + 1);
                        if (path != dest)
                        {
                            Debug.Log("Moved asset to " + dest);
                            AssetDatabase.MoveAsset(path, dest);
                        }
                    }

                    if (onProgress())
                        return;

                }
            }
            finally
            {
                EditorUtility.ClearProgressBar();
            }
        }

        void BuildSinglePrefab()
        {
            string path = finalSavePath + "/" + sourceObject.name + ".prefab";
            var prefab = PrefabUtility.CreatePrefab(path, destObject);
            PrefabUtility.ConnectGameObjectToPrefab(destObject, prefab);
        }

        void BuildMultiPrefabs()
        {
            for (int i = 0; i < destObject.transform.childCount; i++)
            {
                var go = destObject.transform.GetChild(i).gameObject;
                string path = finalSavePath + "/" + go.name + ".prefab";
                var prefab = PrefabUtility.CreatePrefab(path, go);
                PrefabUtility.ConnectGameObjectToPrefab(go, prefab);
            }





        }






    }
}