﻿#if UNITY_EDITOR

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

namespace Curves.Tools.MeshFusion
{
  
    public class MeshMerge
    {
        static public void Merge_AddChild(List<MeshFilter> meshes, string name, Transform parent)
        {
            GameObject newObject = new GameObject();
            newObject.name = name;
            newObject.transform.parent = parent;
            newObject.transform.setIdentity();

            Merge(meshes, name, newObject.transform);
        }

        // merge a list of mesh, and check if there is not more than 65536 verticles
        static public void Merge(List<MeshFilter> meshes, string name, Transform destination)
        {
            int NbVerticles = 0;
            for (int i = 0; i < meshes.Count; i++)
            {
                if (meshes[i] == null || meshes[i].sharedMesh == null)
                {
                    meshes.RemoveAt(i);
                    i--;
                }

                NbVerticles += meshes[i].sharedMesh.vertexCount;
            }
            destination.name = name;
            if (NbVerticles >= 65536)
            {
                List<MeshFilter> tempList = new List<MeshFilter>();

                // need split
                NbVerticles = 0;
                int indexSub = 1;
                for (int i = 0; i < meshes.Count; i++)
                {
                    NbVerticles += meshes[i].sharedMesh.vertexCount;
                    if (NbVerticles > 65536)
                    {
                        Merge_AddChild(tempList, name + "_" + indexSub, destination);
                        indexSub++;
                        tempList.Clear();
                        NbVerticles = meshes[i].sharedMesh.vertexCount;
                    }

                    tempList.Add(meshes[i]);
                }
                // last one
                MergeFiltered_AddChild(tempList, name + "_" + indexSub, destination);
            }
            else
            {
                MergeFiltered_To(meshes, destination);
            }
        }

        public class MaterialReference
        {
            public MeshReference mesh;
            public int subMeshIndex;
        }

        public class MeshReference
        {
            public Mesh mesh;
            public bool invertTriangles;
            public int startVertexIndex;
        }

        static List<MeshReference> listMeshes = new List<MeshReference>();
        static Dictionary<Material, List<MaterialReference>> dicoMat = new Dictionary<Material, List<MaterialReference>>();
        static MeshReference findMeshRef(Mesh mesh)
        {
            for (int i = 0; i < listMeshes.Count; i++)
            {
                if (mesh == listMeshes[i].mesh)
                    return listMeshes[i];
            }
            return null;
        }

        static public List<MeshFilter> ListMeshFilter(Transform tr)
        {
            MeshFilter[] filters = tr.GetComponentsInChildren<MeshFilter>();
            List<MeshFilter> result = new List<MeshFilter>();
            result.AddRange(filters);
            return result;
        }

        static public void MergeFiltered_AddChild(List<MeshFilter> meshes, string name, Transform parent)
        {
            GameObject newObject = new GameObject();
            newObject.name = name;
            newObject.transform.parent = parent;
            newObject.transform.setIdentity();

            MergeFiltered_To(meshes, newObject.transform);
        }

        static Vector4 multiplyTangent(Matrix4x4 m, Vector4 src)
        {
            Vector3 v = new Vector3(src.x, src.y, src.z);
            v = m.MultiplyVector(src).normalized;
            Vector4 res = new Vector4(v.x, v.y, v.z, src.w);

            return res;
        }

        static public void MergeFiltered_To(List<MeshFilter> meshes, Transform destination)
        {
            Mesh tileMesh = new Mesh();
            tileMesh.name = destination.name;

            List<Vector3> vertices = new List<Vector3>();
            List<List<int>> triangles = new List<List<int>>();
            List<Vector3> normals = new List<Vector3>();
            List<Vector4> tangents = new List<Vector4>();
            List<Vector2> uvs = new List<Vector2>();

            Bounds resultbound = new Bounds();
            MeshRenderer renderer;
            dicoMat.Clear();
            listMeshes.Clear();

            int startVertexIndex = 0;
            for (int indexMesh = 0; indexMesh < meshes.Count; indexMesh++)
            {
                MeshFilter filter = meshes[indexMesh];
                renderer = filter.GetComponent<MeshRenderer>();
                if (renderer == null)
                {
                    continue;
                }
                Mesh mesh = filter.sharedMesh;

                Bounds bound = renderer.bounds;
                if (indexMesh == 0)
                    resultbound = bound;
                else
                    resultbound.Encapsulate(bound);

                MeshReference meshRef = new MeshReference();
                meshRef.startVertexIndex = startVertexIndex;
                meshRef.mesh = mesh;
                listMeshes.Add(meshRef);
                Matrix4x4 tr1 = filter.transform.localToWorldMatrix;

                Vector3 sc = filter.transform.lossyScale;

                // check scalar invertion
                bool scaleInverted = false;
                if (sc.x < 0)
                    scaleInverted = !scaleInverted;
                if (sc.y < 0)
                    scaleInverted = !scaleInverted;
                if (sc.z < 0)
                    scaleInverted = !scaleInverted;

                meshRef.invertTriangles = scaleInverted;
                startVertexIndex += mesh.vertexCount;

                Vector3[] sourceVertices = mesh.vertices;

                for (int j = 0; j < sourceVertices.Length; j++)
                {
                    Vector3 pos = tr1.MultiplyPoint(sourceVertices[j]);
                    vertices.Add(pos);
                }

                Vector3[] sourceNormals = mesh.normals;
                if (sourceNormals == null || sourceNormals.Length != sourceVertices.Length)
                    mesh.RecalculateNormals();

                sourceNormals = mesh.normals;

                for (int j = 0; j < sourceNormals.Length; j++)
                {
                    Vector3 n = tr1.MultiplyVector(sourceNormals[j]).normalized;
                    normals.Add(n);
                }

                Vector4[] sourceTangents = mesh.tangents;
                if (sourceTangents == null || sourceTangents.Length != sourceVertices.Length)
                    mesh.RecalculateTangents();

                sourceTangents = mesh.tangents;
                for (int j = 0; j < sourceTangents.Length; j++)
                { 
                    Vector4 n = multiplyTangent(tr1, sourceTangents[j]);
                    tangents.Add(n);
                }

                if (mesh.uv.Length != sourceVertices.Length)
                {
                    // create zero uv
                    for (int j = 0; j < sourceVertices.Length; j++)
                        uvs.Add(Vector2.zero);
                }
                else
                    uvs.AddRange(mesh.uv);

                // for each submesh, we just add a reference to a list
                // it will be added in a new sorted array latter
                var sharedMaterials = renderer.sharedMaterials;
                for (int indexsubMesh = 0; indexsubMesh < mesh.subMeshCount; indexsubMesh++)
                {
                    int indexMaterial = indexsubMesh;
                    if (indexsubMesh >= sharedMaterials.Length)
                        indexMaterial = sharedMaterials.Length - 1;

                    if (indexMaterial < 0)
                        continue;

                    Material mat = sharedMaterials[indexMaterial];
                    var mRef = new MaterialReference();
                    mRef.mesh = meshRef;
                    mRef.subMeshIndex = indexsubMesh;

                    if (dicoMat.ContainsKey(mat))
                    {
                        List<MaterialReference> list = dicoMat[mat];
                        list.Add(mRef);
                    }
                    else
                    {
                        List<MaterialReference> list = new List<MaterialReference>();
                        list.Add(mRef);
                        dicoMat[mat] = list;
                    }
                }
            }

            List<Material> mats = new List<Material>();

            int subMeshIndex = 0;

            // for each material search in the dico which mesh/submesh use it
            // add it then to the 
            foreach (Material mat in dicoMat.Keys)
            {
                mats.Add(mat);

                var listMatReference = dicoMat[mat];
                List<int> subMeshTriangles = new List<int>();

                for (int i = 0; i < listMatReference.Count; i++)
                {
                    var matReference = listMatReference[i];

                    var meshref = matReference.mesh;
                    int startIndex = meshref.startVertexIndex;

                    int[] srcTriangles = meshref.mesh.GetTriangles(matReference.subMeshIndex);
                    if (meshref.invertTriangles)
                    {
                        for (int indexpoint = 0; indexpoint < srcTriangles.Length; indexpoint += 3)
                        {
                            int tmp = srcTriangles[indexpoint + 1];
                            srcTriangles[indexpoint + 1] = srcTriangles[indexpoint + 2];
                            srcTriangles[indexpoint + 2] = tmp;
                        }
                    }

                    for (int j = 0; j < srcTriangles.Length; j++)
                    {
                        subMeshTriangles.Add(srcTriangles[j] + startIndex);
                    }
                }

                triangles.Add(subMeshTriangles);

                subMeshIndex++;
            }

     //       destination.position = resultbound.center;
            Matrix4x4 tr2 = destination.worldToLocalMatrix;

            for (int indexVertex = 0; indexVertex < vertices.Count; indexVertex++)
            {
                vertices[indexVertex] = tr2.MultiplyPoint(vertices[indexVertex]);
                normals[indexVertex] = tr2.MultiplyVector(normals[indexVertex]).normalized;
                tangents[indexVertex] = multiplyTangent(tr2, tangents[indexVertex]);
            }

            tileMesh.subMeshCount = triangles.Count;
            tileMesh.vertices = vertices.ToArray();
            tileMesh.uv = uvs.ToArray();
            tileMesh.normals = normals.ToArray();
            tileMesh.tangents = tangents.ToArray();

            for (int indexsubMesh = 0; indexsubMesh < triangles.Count; indexsubMesh++)
            {
                tileMesh.SetTriangles(triangles[indexsubMesh], indexsubMesh);
            }

            tileMesh.RecalculateBounds();

            var endFilter = destination.gameObject.GetOrAddComponent<MeshFilter>();
            endFilter.sharedMesh = tileMesh;

            renderer = destination.gameObject.GetOrAddComponent<MeshRenderer>();
            renderer.materials = mats.ToArray();
        }

    }
}


#endif