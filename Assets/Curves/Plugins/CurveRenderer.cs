﻿using UnityEngine;
using System.Collections;
using System;

namespace Curves
{
    public interface ICurveMeshBuilder
    {
        void CleanUpRenderer();
        void UpdateRenderer(CurveRenderer renderer);

#if UNITY_EDITOR
        void drawGizmos(Transform tr);
#endif

    }

    // parent class for all curve renderers
    [AddComponentMenu("Curves/Curve Renderer")]
    [ExecuteInEditMode]
    public class CurveRenderer : MonoBehaviour
    {
#if UNITY_EDITOR
        public void OnDrawGizmosSelected()
        {
            if (drawGizmos && meshBuilder != null)
                meshBuilder.drawGizmos(transform);
        }
#endif

        public enum Type
        {
            BillboardLine,
            Steps,
            FlatLine,
            Extrusion
        }

        public Type type;
        Type oldType;

        // use a canvas renderer instead of a Mesh Renderer
        public bool useCanvasRenderer = false;

        // adds a mesh Collider
        public bool addCollider = false;

        public enum WidthMethod
        {
            Curve,
            Points
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="indexPoint"></param>
        /// <returns></returns>
        public float getWidth(int indexPoint)
        {
            if (widthMethod == WidthMethod.Points)
            {
                return curve.Sizes[indexPoint];
            }
            else
            {
                float ratio = (float)indexPoint / curve.Polyline.Length;
                return getWidth(ratio);
            }
        }

        public WidthMethod widthMethod = WidthMethod.Curve;

        public float minWidth;
        public float maxWidth;

        public AnimationCurve widthCurve = new AnimationCurve();

        public Color[] colors = new Color[] { Color.gray, Color.gray };

        public enum Sides
        {
            Out,
            In,
            Both
        }
        public Sides sides = Sides.Out;

        public bool normalizedUV = true;

        public float UvAdaptation = 1;
        public float UVBorders = 0;

        public Material material = null;
        public bool drawGizmos = false;

        public int focusOnTriangle = -1;
        public bool checkTrianglesInversion = false;

        [Range(0,50)]
        public int NbTriangleCorrection = 0;

        [Range(3,90)]
        public int NbFaces = 4;

        [Range(-180, 180)]
        public int Angle = 0;

        
        public Vector2 deltaPos = Vector2.zero;


        public bool FullRing = true;

       
        public int StartAngle = 0;

       
        public int EndAngle = 360;

        ICurveMeshBuilder meshBuilder;

        public Color getColor(float ratio)
        {
            if (colors.Length == 0)
            {
                return Color.white;
            }
            else if (colors.Length == 1)
            {
                return colors[0];
            }

            ratio = Mathf.Clamp(ratio, 0, 1);
            if (ratio == 1)
                return colors[colors.Length - 1];

            int indexColor = (int)Mathf.Floor(ratio * (colors.Length - 1));
            float ratioColor = ratio * (colors.Length - 1) - indexColor;


            return Color.Lerp(colors[indexColor], colors[indexColor + 1], ratioColor);
        }

        public float getWidth(float ratio)
        {
            return Mathf.Clamp(minWidth + widthCurve.Evaluate(ratio) * (maxWidth - minWidth), 0, float.MaxValue);
        }

        bool isDirty = true;
        void setChanged()
        {
            isDirty = true;
        }

        int versionIndex = -1;
        protected Curve curve = null;

        void getCurve()
        {
            if (curve == null)
            {
                curve = gameObject.GetComponent<Curve>();
                if (curve == null)
                {
                    curve = gameObject.AddComponent<Curve>();
                }
            }
        }

        void createMeshBuilder()
        {          
            if (meshBuilder != null)
                meshBuilder.CleanUpRenderer();

            switch (type)
            {
                case Type.BillboardLine:
                    meshBuilder = new CurveLine();
                    break;
                case Type.Steps:
                    meshBuilder = new CurveStep();
                    break;              
                case Type.FlatLine:
                    meshBuilder = new CurveFlatLine();
                    break;
                case Type.Extrusion:
                    meshBuilder = new CurveExtrusion();
                    break;
                default:
                    break;
            }

        }

        public void Update()
        {
            getCurve();

            if (oldType != type || meshBuilder == null)
            {
                createMeshBuilder();
                isDirty = true;
                oldType = type;
            }

            if (meshBuilder != null)
            {
                if (versionIndex != curve.versionIndex || isDirty)
                {
                    meshBuilder.UpdateRenderer(this);
                    versionIndex = curve.versionIndex;
                    isDirty = false;
                }
            }
        }


#if UNITY_EDITOR
        void OnValidate()
        {
            isDirty = true;
        }

        public void CleanUp()
        {
            MeshFilter filter = GetComponent<MeshFilter>();
            if (filter != null)
                filter.sharedMesh = null;


            MeshCollider col = GetComponent<MeshCollider>();
            if (col != null)
                col.sharedMesh = null;
        }

        public void Rebuild()
        {
            versionIndex = -1;
            Update();
        }
#endif

        }

}