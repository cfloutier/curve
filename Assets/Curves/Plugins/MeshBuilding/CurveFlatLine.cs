using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Curves
{
    public class CurveFlatLine : CurveMeshBuilder
    {
        public class Triangle
        {
            public int a;
            public int b;
            public int c;

            public bool right = false;

            public bool inverted = false;

            public bool needCorrection = false;

            List<Vector3> p;

            public bool enabled = true;

            public Vector3 pa
            {
                get { return p[a]; }
            }

            public Vector3 pb
            {
                get { return p[b]; }
            }

            public Vector3 pc
            {
                get { return p[c]; }
            }

            public void setVertices(List<Vector3> vertices)
            {
                this.p = vertices;

                Vector3 va = p[a] - p[b];
                Vector3 vb = p[a] - p[c];
                normal = Vector3.Cross(va.normalized, vb.normalized);

                center = (p[a] + p[b] + p[c]) / 3;
            }

            public Vector3 center;
            public Vector3 normal;
        }


        void replaceVertice(int oldIndex, int newIndex, int indexTriangle)
        {
            for (int indexT = indexTriangle; indexT >= 0; indexT--)
            {
                Triangle t = trianglesList[indexT];
                if (t.a == oldIndex) t.a = newIndex;
                else if (t.b == oldIndex) t.b = newIndex;
                else if (t.c == oldIndex) t.c = newIndex;
                else break;
            }

            for (int indexT = indexTriangle + 1; indexT < trianglesList.Count; indexT++)
            {
                Triangle t = trianglesList[indexT];
                if (t.a == oldIndex) t.a = newIndex;
                else if (t.b == oldIndex) t.b = newIndex;
                else if (t.c == oldIndex) t.c = newIndex;
                else break;

            }

        }


        void correctWrongTriangles()
        {
            if (trianglesList == null || trianglesList.Count == 0) return;

            for (int i = 0; i < trianglesList.Count; i++)
            {
                Triangle t = trianglesList[i];
                if (!t.enabled)
                    continue;

                if (t.needCorrection)
                {
                    if (t.right)
                    {

                        t.enabled = false;

                        Vector3 center = (vertices[t.a] + vertices[t.c]) / 2;

                        vertices[t.c] = center;
                        replaceVertice(t.a, t.c, i);
                    }
                    else
                    {
                        t.enabled = false;

                        Vector3 center = (vertices[t.a] + vertices[t.b]) / 2;

                        replaceVertice(t.a, t.b, i);


                        vertices[t.b] = center;
                    }
                }
            }
        }

        bool detectInvertedTriangles(bool test)
        {
            if (trianglesList == null || trianglesList.Count == 0) return false;

            bool hasInvertion = false;
            Triangle prev = null;
            for (int i = 0; i < trianglesList.Count; i++)
            {
                Triangle t = trianglesList[i];

                t.setVertices(vertices);

                Vector3 normal = t.normal;

                if (prev == null)
                {
                    // no prev
                    prev = t;
                    continue;
                }

                float dot = Vector3.Dot(prev.normal, normal);

                t.inverted = dot < 0;

                prev = t;
            }

            int NbInvertion = 0;
            prev = null;
            for (int i = 1; i < trianglesList.Count; i++)
            {
                Triangle t = trianglesList[i];

                if (prev == null)
                {
                    // no prev
                    prev = t;
                    continue;
                }

                if (t.inverted)
                    NbInvertion++;
                else
                    NbInvertion = 0;

                if (NbInvertion == 2)
                {
                    NbInvertion = 0;
                    prev.needCorrection = true;
                    hasInvertion = true;
                }

                prev = t;

            }
            return hasInvertion;
        }

        void cleanUpDisableTriangles()
        {

            for (int i = 1; i < trianglesList.Count; i++)
            {
                Triangle t = trianglesList[i];
                if (!t.enabled)
                {
                    trianglesList.RemoveAt(i);
                    i--;

                }

            }
        }


#if UNITY_EDITOR

        void drawGizmosSphere(Vector3 Pos, float size)
        {
            float adapt = HandleUtility.GetHandleSize(Pos);
            Gizmos.DrawSphere(Pos, size * adapt);
        }


        void drawGizmosCube(Vector3 Pos, float size)
        {
            float adapt = HandleUtility.GetHandleSize(Pos);
            Gizmos.DrawCube(Pos, Vector3.one * size * adapt);
        }

        public override void drawGizmos(Transform tr)
        {
            Gizmos.color = Color.cyan;

            if (trianglesList == null)
                return;

            detectInvertedTriangles(true);

            for (int i = 0; i < trianglesList.Count; i++)
            {
                Triangle t = trianglesList[i];
                if (!t.enabled)
                    continue;

                if (renderer.focusOnTriangle < 0 || renderer.focusOnTriangle == i)
                {
                    if (t.right)
                    {
                        Gizmos.color = Color.red;

                        Gizmos.DrawLine(tr.TransformPoint(vertices[t.a]), tr.TransformPoint(vertices[t.c]));

                        Gizmos.color = Color.yellow;
                        Gizmos.DrawLine(tr.TransformPoint(vertices[t.a]), tr.TransformPoint(vertices[t.b]));

                        Gizmos.color = Color.cyan;
                        Gizmos.DrawLine(tr.TransformPoint(vertices[t.c]), tr.TransformPoint(vertices[t.b]));
                    }
                    else
                    {
                        Gizmos.color = Color.red;
                        Gizmos.DrawLine(tr.TransformPoint(vertices[t.a]), tr.TransformPoint(vertices[t.b]));

                        Gizmos.color = Color.yellow;
                        Gizmos.DrawLine(tr.TransformPoint(vertices[t.b]), tr.TransformPoint(vertices[t.c]));

                        Gizmos.color = Color.cyan;
                        Gizmos.DrawLine(tr.TransformPoint(vertices[t.a]), tr.TransformPoint(vertices[t.c]));
                    }

                    Gizmos.color = Color.green;


                    if (renderer.focusOnTriangle > 0)
                    {
                        Gizmos.color = Color.cyan;
                        Vector3 pos = tr.TransformPoint(vertices[t.a]);
                        drawGizmosSphere(pos, 0.1f);
                        Handles.Label(pos, "     A");

                        Gizmos.color = Color.yellow;
                        pos = tr.TransformPoint(vertices[t.b]);
                        drawGizmosSphere(pos, 0.1f);
                        Handles.Label(pos, "     B");

                        Gizmos.color = Color.red;
                        pos = tr.TransformPoint(vertices[t.c]);
                        drawGizmosSphere(pos, 0.1f);
                        Handles.Label(pos, "     C");
                    }


                    if (renderer.checkTrianglesInversion)
                    {
                        if (t.inverted)
                            Gizmos.color = Color.red;

                        else
                            Gizmos.color = Color.green;

                        Vector3 pos = tr.TransformPoint(t.center);

                        Vector3 normal = tr.TransformDirection(t.normal);
                        Gizmos.DrawLine(pos, pos + normal);

                        if (t.right)
                        {
                            Gizmos.color = Color.cyan;
                            Handles.Label(pos, "     Right " + i);
                        }
                        else
                        {
                            Gizmos.color = Color.red;
                            Handles.Label(pos, "     Left " + i);
                        }

                        if (t.needCorrection)
                        {
                            drawGizmosCube(pos, 0.3f);
                        }
                    }
                }
            }
        }
#endif 

        List<Triangle> trianglesList;

        void buildVerticles()
        {
            int len = curve.Polyline.Length;

            vertices = new List<Vector3>();
            Vector3 right = Vector3.zero;

            for (int i = 0; i < len; i++)
            {
                right = curve.Orientations[i] * Vector3.right;
                right.Normalize();


                float width = renderer.getWidth(i);
                // float width = renderer.getWidth(ratio);

                vertices.Add(curve.Polyline[i] - right * width);
                vertices.Add(curve.Polyline[i] + right * width);
            }
        }

        void buildNormals()
        {
            int len = curve.Polyline.Length;


            normals = new List<Vector3>();

            for (int i = 0; i < len; i++)
            {
                Vector3 v = curve.Orientations[i] * Vector3.up; ;
                normals.Add(v);
                normals.Add(v);
            }
        }

        void buildUVs()
        {
            int len = curve.Polyline.Length;

            uvs = new List<Vector2>();

            float x = 0;

            if (renderer.normalizedUV)
            {
                float totalLen = curve.totalLenght;
                if (totalLen == 0)
                    totalLen = 1;

                if (renderer.UVBorders > 0)
                {

                    float sizeStartBorder = renderer.getWidth(0)*2;
                    float sizeEndBorder = renderer.getWidth(len - 1)*2;

                    float removedLen = sizeStartBorder + sizeEndBorder;
                    sizeEndBorder = totalLen - sizeEndBorder;
                    if (removedLen > totalLen)
                        removedLen = totalLen;

                    float uvBorder = renderer.UVBorders;

                    float progress = 0;
                    for (int i = 0; i < len; i++)
                    {
                        progress += curve.length[i];
                        if (progress < sizeStartBorder)
                        {
                            float ratio = progress / sizeStartBorder;

                            x = ratio * uvBorder;
                            uvs.Add(new Vector2(x, 0));
                            uvs.Add(new Vector2(x, 1));
                        }
                        else if (progress > sizeEndBorder)
                        {
                            float ratio = Mathf.InverseLerp(sizeEndBorder, totalLen, progress);
                            x = Mathf.Lerp(1 - uvBorder, 1, ratio);
                            uvs.Add(new Vector2(x, 0));
                            uvs.Add(new Vector2(x, 1));
                        }
                        else
                        {
                            float ratio = Mathf.InverseLerp(sizeStartBorder, sizeEndBorder, progress);
                            x = Mathf.Lerp(uvBorder, 1 - uvBorder, ratio);
                            uvs.Add(new Vector2(x, 0));
                            uvs.Add(new Vector2(x, 1));
                        }

                    }
                }
                else
                {
                    for (int i = 0; i < len; i++)
                    {
                        x += curve.length[i] / totalLen;
                        uvs.Add(new Vector2(x, 0));
                        uvs.Add(new Vector2(x, 1));
                    }
                }

            }
            else if (renderer.UvAdaptation != 0)
            {
                float adapt = 1 / renderer.UvAdaptation;
                for (int i = 0; i < len; i++)
                {
                    x += curve.length[i] * adapt;
                    uvs.Add(new Vector2(x, 0));
                    uvs.Add(new Vector2(x, 1));
                }

            }

        }


        void buildColors()
        {
            int len = curve.Polyline.Length;
            int size = len * 2;
            if (renderer.sides == CurveRenderer.Sides.Both)
                size = size * 2;

            colors = new List<Color>();

            float deltax = 1.0f / len;

            float x = 0;
            for (int i = 0; i < len; i++)
            {
                float ratio = (float)i / len;

                Color c = renderer.getColor(ratio);
                colors.Add(c);
                colors.Add(c);

                x += deltax;
            }
        }

        void convertTriangleIndexes()
        {
            triangles = new List<int>();
            // triangleIndexes = new int[triangles.Count * 3];

            for (int i = 0; i < trianglesList.Count; i++)
            {
                Triangle t = trianglesList[i];
                if (t.enabled)
                {
                    triangles.Add(t.a);
                    triangles.Add(t.b);
                    triangles.Add(t.c);
                }

            }
        }

        void buildTriangles()
        {

            trianglesList = new List<Triangle>();

            int len = curve.Polyline.Length;
            if (len == 0)
                return;





            for (int i = 0; i < len - 1; i++)
            {
                int mainPointIndex = i * 2;


                Triangle t = new Triangle();
                t.right = false;
                t.a = mainPointIndex;
                t.b = mainPointIndex + 2;
                t.c = mainPointIndex + 1;
                trianglesList.Add(t);

                t = new Triangle();
                t.right = true;
                t.a = mainPointIndex + 1;
                t.b = mainPointIndex + 2;
                t.c = mainPointIndex + 3;

                trianglesList.Add(t);


            }
        }

        protected override void FillArrays()
        {
            mesh.name = "FlatLine 2";


            buildVerticles();
            buildTriangles();
            buildNormals();
            buildUVs();
            buildColors();


            if (renderer.checkTrianglesInversion)
            {
                if (detectInvertedTriangles(false))
                {
                    for (int i = 0; i < renderer.NbTriangleCorrection; i++)
                    {
                        correctWrongTriangles();
                        cleanUpDisableTriangles();
                        if (!detectInvertedTriangles(false))
                            break;
                    }
                }
            }

            convertTriangleIndexes();
        }
    }

}