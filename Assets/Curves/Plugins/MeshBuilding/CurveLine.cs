using UnityEngine;
using System;
using System.Collections;

namespace Curves
{


    public class CurveLine : ICurveMeshBuilder
    {
        public void drawGizmos(Transform t)
        {

        }

        LineRenderer m_lineRenderer = null;

        Curve curve;
        GameObject gameObject;
        //  CurveRenderer renderer;

        public void CleanUpRenderer()
        {
            if (m_lineRenderer != null)
                GameObject.DestroyImmediate(m_lineRenderer);
        }

        public void UpdateRenderer(CurveRenderer renderer)
        {
            //      this.renderer = renderer;
            curve = renderer.GetComponent<Curve>();
            gameObject = renderer.gameObject;

            if (m_lineRenderer == null)
            {
                m_lineRenderer = gameObject.GetComponent<LineRenderer>();
                if (m_lineRenderer == null)
                {
                    m_lineRenderer = gameObject.AddComponent<LineRenderer>();
                }
            }

            m_lineRenderer.positionCount = curve.Polyline.Length;

            for (int i = 0; i < curve.Polyline.Length; i++)
            {
                m_lineRenderer.SetPosition(i, curve.Polyline[i]);
            }

            m_lineRenderer.useWorldSpace = false;

            m_lineRenderer.startWidth = renderer.minWidth;
            m_lineRenderer.endWidth = renderer.maxWidth;
            m_lineRenderer.material = renderer.material;
            m_lineRenderer.startColor = renderer.getColor(0);
            m_lineRenderer.endColor = renderer.getColor(1);
        }

    }

}