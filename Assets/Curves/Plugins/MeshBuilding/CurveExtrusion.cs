using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Curves
{
    public class CurveExtrusion : CurveMeshBuilder
    {

#if UNITY_EDITOR

        void drawGizmosSphere(Vector3 Pos, float size)
        {
            float adapt = HandleUtility.GetHandleSize(Pos);
            Gizmos.DrawSphere(Pos, size * adapt);
        }

        void drawGizmosCube(Vector3 Pos, float size)
        {
            float adapt = HandleUtility.GetHandleSize(Pos);
            Gizmos.DrawCube(Pos, Vector3.one * size * adapt);
        }

        public override void drawGizmos(Transform tr)
        {
            Gizmos.color = Color.white;

            for (int i = 0; i < vertices.Count; i++)
            {
                Vector3 pos = tr.TransformPoint(vertices[i]);

                drawGizmosCube(pos, 0.1f);
                Handles.Label(pos, "    "+ i); 
            }

        }
#endif 

        void buildVerticles()
        {
            int len = curve.Polyline.Length;

            vertices = new List<Vector3>();
            uvs = new List<Vector2>();
            normals = new List<Vector3>();
            colors = new List<Color>();
            triangles = new List<int>();

            Vector3 right = Vector3.zero;
            Vector3 up = Vector3.zero;

            float u = 0;
            float adapt = 1;

            if (renderer.normalizedUV)
            {
                adapt = 1 / curve.totalLenght;
            }
            else if (renderer.UvAdaptation != 0)
            {
                adapt = 1 / renderer.UvAdaptation;
            }

            int firstIndex = 0;
            int nbFaces = renderer.NbFaces;
           
            float deltaAngle = Mathf.PI * 2 / nbFaces;
            float startAngle = renderer.Angle * Mathf.Deg2Rad;
            if (!renderer.FullRing)
            {
                startAngle = renderer.StartAngle * Mathf.Deg2Rad;
                float endAngle = renderer.EndAngle * Mathf.Deg2Rad;
                while (endAngle < startAngle)
                    endAngle += Mathf.PI * 2;

                deltaAngle =( endAngle - startAngle) / nbFaces;
            }


            float[] sinuses = new float[nbFaces + 1];
            float[] cosinuses = new float[nbFaces + 1];
            float[] v = new float[nbFaces + 1];

            for (int j = 0; j < nbFaces + 1; j++)
            {
                float angle = startAngle + deltaAngle * j;

                cosinuses[j] = Mathf.Cos(angle);
                sinuses[j] = Mathf.Sin(angle);
                v[j] = (float)j / nbFaces;
            }
            Vector2 deltaPos = renderer.deltaPos;
            for (int i = 0; i < len; i++)
            {
                u += curve.length[i] * adapt;

                right = curve.Orientations[i] * Vector3.right;
                right.Normalize();
                up = curve.Orientations[i] * Vector3.up;
                up.Normalize();

                float ratio = (float)i / len;

                float width = renderer.getWidth(i);
                Color c = renderer.getColor(ratio);

                firstIndex = vertices.Count;

                Vector3 pos = curve.Polyline[i] + deltaPos.x * right + deltaPos.y * up;

                for (int j = 0; j < nbFaces  + 1; j++)
                {

                    vertices.Add(pos - (cosinuses[j] * right + sinuses[j] * up) * width);
                    normals.Add(-cosinuses[j] * right - sinuses[j] * up);
                    uvs.Add(new Vector2(u, v[j]));
                    colors.Add(c);
                }

                if (i < len - 1)
                {                
                    for (int j = 0; j < nbFaces; j++)
                    {
                        int pt = firstIndex + j;

                     //   Debug.Log("pt" + pt);

                        int Pa = pt;
                        int Pb = Pa + 1;
                        int Pc = pt + nbFaces + 1;
                        int Pd = Pc + 1;

                        triangles.Add(Pa);
                        triangles.Add(Pb);
                        triangles.Add(Pd);

                        triangles.Add(Pa);
                        triangles.Add(Pd);
                        triangles.Add(Pc);
                    }
                 }
            }
        }

        protected override void FillArrays()
        {
            mesh.name = "Extrusion Mesh";

            buildVerticles();
        }
    }
}