using UnityEngine;
using System.Collections.Generic;
using Curves.Dependencies;

namespace Curves
{
    [ExecuteInEditMode]
    public class CurveMeshBuilder : ICurveMeshBuilder
    {
        public virtual void drawGizmos(Transform t)
        {


        }

        protected MeshFilter m_meshFilter = null;
        protected MeshRenderer m_meshRenderer = null;
        protected MeshCollider m_collider;

        protected CurveUIRenderer m_uiRenderer = null;

        protected Curve curve;
        protected GameObject gameObject;
        protected CurveRenderer renderer;
        protected Mesh mesh;

        protected List<Vector3> vertices;
        protected List<Vector3> normals;
        protected List<Vector2> uvs;
        protected List<int> triangles;
        protected List<Color> colors;

        public void CleanUpRenderer()
        {
            if (m_meshFilter != null)
                GameObject.DestroyImmediate(m_meshFilter);

            if (m_meshRenderer)
                GameObject.DestroyImmediate(m_meshRenderer);

            if (m_collider)
                GameObject.DestroyImmediate(m_collider);

            if (m_uiRenderer)
            {
                GameObject.DestroyImmediate(m_uiRenderer);

                CanvasRenderer renderer = gameObject.GetComponent<CanvasRenderer>();
                if (renderer)
                    GameObject.DestroyImmediate(renderer);
            }

            m_meshFilter = null;
            m_meshRenderer = null;
            m_uiRenderer = null;



        }

        public void UpdateRenderer(CurveRenderer renderer)
        {
            this.renderer = renderer;
            curve = renderer.GetComponent<Curve>();
            gameObject = renderer.gameObject;

            BuildRenderers();
            updateMesh();
        }

        void BuildRenderers()
        {
            if (renderer.useCanvasRenderer)
            {
                if (m_uiRenderer == null)
                {
                    CleanUpRenderer();
                    gameObject.GetOrAddComponent<RectTransform>();
                    gameObject.GetOrAddComponent<CanvasRenderer>();
                    m_uiRenderer = gameObject.GetOrAddComponent<CurveUIRenderer>();
                }
            }
            else
            {
                if (m_meshRenderer == null)
                {
                    CleanUpRenderer();

                    m_meshRenderer = gameObject.GetOrAddComponent<MeshRenderer>();
                    m_meshFilter = gameObject.GetOrAddComponent<MeshFilter>();
                }
            }



            if (renderer.addCollider)
            {

                m_collider = gameObject.GetOrAddComponent<MeshCollider>();

            }
            else
            {
                if (m_collider != null)
                {
                     GameObject.DestroyImmediate(m_collider); ;
                }
            }



        }


        void invert()
        {
            int Nb = vertices.Count;

            // normal is inverted
            for (int i = 0; i < Nb; i++)
            {
                normals[i] = -normals[i];
            }

            Nb = triangles.Count / 3;
            // vertice index is shifted and order is reversed
            for (int i = 0; i < Nb; i++)
            {
                int a = triangles[i * 3];
                int b = triangles[i * 3 + 1];
                int c = triangles[i * 3 + 2];

                triangles[i * 3] = a;
                triangles[i * 3 + 1] = c;
                triangles[i * 3 + 2] = b;
            }
        }

        void buildDoubleSide()
        {
            int Nb = vertices.Count;
            // double vertices because normal must be inverted
            for (int i = 0; i < Nb; i++)
            {
                vertices.Add(vertices[i]);
            }

            // normal is inverted
            for (int i = 0; i < Nb; i++)
            {
                normals.Add(-normals[i]);
            }

            // uv are equal
            for (int i = 0; i < Nb; i++)
            {
                uvs.Add(uvs[i]);
            }

            // colors are equals
            for (int i = 0; i < Nb; i++)
            {
                colors.Add(colors[i]);
            }

            int firstIndex = Nb;
            Nb = triangles.Count / 3;

            // vertice index is shifted and order is reversed
            for (int i = 0; i < Nb; i++)
            {
                triangles.Add(triangles[i * 3] + firstIndex);
                triangles.Add(triangles[i * 3 + 2] + firstIndex);
                triangles.Add(triangles[i * 3 + 1] + firstIndex);
            }
        }

        protected virtual void FillArrays()
        {

        }

        void updateMesh()
        {
            // build lines
            mesh = new Mesh();

            FillArrays();

            if (renderer.sides == CurveRenderer.Sides.In)
                invert();
            else if (renderer.sides == CurveRenderer.Sides.Both)
                buildDoubleSide();

            mesh.vertices = vertices.ToArray();
            mesh.normals = normals.ToArray();
            mesh.uv = uvs.ToArray();
            mesh.triangles = triangles.ToArray();
            mesh.colors = colors.ToArray();

            if (renderer.useCanvasRenderer)
            {
                m_uiRenderer.setMesh(mesh, renderer.material);
            }
            else
            {
                m_meshFilter.mesh = mesh;
                m_meshRenderer.sharedMaterial = renderer.material;
            }

            if (m_collider != null)
                m_collider.sharedMesh = mesh;

        }
    }
}