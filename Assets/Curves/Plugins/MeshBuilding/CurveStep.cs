using UnityEngine;
using System.Collections.Generic;


namespace Curves
{
    [ExecuteInEditMode]
    public class CurveStep : CurveMeshBuilder
    {     
        void buildStepsVertices()
        {
            vertices = new List<Vector3>();

            int len = curve.Loop ? curve.Polyline.Length - 1 : curve.Polyline.Length;
            if (len <= 0)
                return;

            int i;
            Vector3 forward = Vector3.zero;
            Vector3 right = Vector3.zero;

            for (i = 0; i < len; i++)
            {
                forward = curve.Orientations[i] * Vector3.forward;
                right = curve.Orientations[i] * Vector3.right;
                Vector3 pt = curve.Polyline[i];
           
                float width = renderer.getWidth(i);
                
                vertices.Add(pt + right * width  + forward * width );
                vertices.Add(pt - right * width  + forward * width );
                vertices.Add(pt + right * width  - forward * width );
                vertices.Add(pt - right * width  - forward * width );
            }
        }

        void buildStepsNormals()
        {
            normals = new List<Vector3>();

            int len = curve.Loop ? curve.Polyline.Length - 1 : curve.Polyline.Length;

            for (int i = 0; i < len; i++)
            {
                Vector3 v = curve.Orientations[i] * Vector3.up;

                normals.Add(v);
                normals.Add(v);
                normals.Add(v);
                normals.Add(v);
            }
        }

        void buildStepsUVs()
        {
            uvs = new List<Vector2>();

            int len = curve.Loop ? curve.Polyline.Length - 1 : curve.Polyline.Length;

            for (int i = 0; i < len; i++)
            {
                uvs.Add(new Vector2(0, 1));
                uvs.Add(new Vector2(1, 1));
                uvs.Add(new Vector2(0, 0));
                uvs.Add(new Vector2(1, 0));
            }
        }

        void buildStepsColors()
        {
            colors = new List<Color>();

            int len = curve.Loop ? curve.Polyline.Length - 1 : curve.Polyline.Length;
            float deltax = 1.0f / curve.Polyline.Length;

            float x = 0;
            for (int i = 0; i < len; i++)
            {
                float ratio = (float)i / curve.Polyline.Length;

                Color c = renderer.getColor(ratio);
                colors.Add(c);
                colors.Add(c);
                colors.Add(c);
                colors.Add(c);
                x += deltax;
            }
        }

        void buildStepsTriangles()
        {
            triangles = new List<int>();
            int len = curve.Loop ? curve.Polyline.Length - 1 : curve.Polyline.Length;
           
            for (int i = 0; i < len; i++)
            {
                int mainPointIndex = i * 4;
               
                triangles.Add(mainPointIndex + 3);
                triangles.Add(mainPointIndex + 1);
                triangles.Add(mainPointIndex + 0);

                triangles.Add(mainPointIndex + 2);
                triangles.Add(mainPointIndex + 3);
                triangles.Add(mainPointIndex + 0);
            }
        }

        protected override void FillArrays()
        {
            mesh.name = "CurveSteps";

            buildStepsVertices();
            buildStepsNormals();
            buildStepsUVs();
            buildStepsTriangles();
            buildStepsColors();
        }



    }
}