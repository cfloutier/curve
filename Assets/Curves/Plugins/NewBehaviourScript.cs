﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace Curves
{
    public static class Extensions
    {
        public static void setIdentity(this Transform tr)
        {
            tr.localPosition = Vector3.zero;
            tr.localRotation = Quaternion.identity;
            tr.localScale = Vector3.one;
        }

        public static void copyPosition(this Transform tr, Transform src)
        {
            Transform oldParent = tr.parent;
            tr.parent = src;
            tr.setIdentity();
            tr.parent = oldParent;
        }

        /// <summary>
        /// Find the first a parent component
        /// </summary>
        public static void cleanChildren(this Transform tr)
        {
            while (tr.childCount != 0)
                GameObject.DestroyImmediate(tr.GetChild(0).gameObject);
        }

        /// <summary>
        /// Find the first a parent component
        /// </summary>
        public static T GetOrAddComponent<T>(this GameObject G) where T : Component
        {
            T compo = G.GetComponent<T>();
            if (compo != null) return compo;
            return G.AddComponent<T>();

        }
    }
}