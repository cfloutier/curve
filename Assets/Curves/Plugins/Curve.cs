using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Curves.Maths;

namespace Curves
{
    [AddComponentMenu("Curves/Curve")]
    [ExecuteInEditMode]

    /// <summary>
    /// BezierCurve create a list of points based on bezier curve edition
    /// There is no rendering, you should use a BezierLineRenderer class to make it visible
    /// 
    /// There is 3 modes of edition : 
    /// * Line (no curve mode)
    /// * auto bezier (just setup points where the curve should cross)
    /// * complet bezier (each point is editable)
    ///  
    /// There is also 2 modes of final poly-line creation
    /// * Points Division : each segement between points is divided into a constant number of lines. each segment can have differents lenght depending on curve control points
    /// * Constant Distance : the curve is separated into segments that will all have the same difstance. It useful for animations
    /// 
    /// </summary>
    public class Curve : MonoBehaviour
    {
        #region public fields

        /// <summary>
        /// The type of curve.
        /// </summary>
        public enum CurveType
        {
            /// <summary>Spline : The curve is just defined by the list of points. 
            /// It's less precise than Bezier but really easy to configure by script</summary>
            Spline,

            /// <summary>
            /// Curves parts are defined by Control Points. More precesise but more difficult to define by script
            /// </summary>
            Bezier,
        }

        public enum SplineType
        {
            Lines,
            Cubic,
            CatmullRom,
            Centripetal,
            Hermite
        }

        public enum BezierType
        {
            /// <summary>
            /// each part is defined using 2 Control Points Each poitn share a common control
            /// </summary>
            Quad,

            /// <summary>
            /// 2 different control point. The more options are found here
            /// </summary>
            Cubic
        }

        /// <summary>
        /// The way interpolation is made for rendering or animation
        /// </summary>
        public enum SamplingMode
        {
            /// <summary>
            /// Each part of the curve is split into a predefined number of segment.
            /// Each segment can't have the same lenght because of differents curves properties.
            /// This kind of interpolation is the best for rendering, by lame for animation, because it can guarante a constant speed
            /// </summary>
            PointsDivision,

            /// <summary>
            /// A more complex curve interpolation.
            /// The curve is firsty split into a large number of segments
            /// Depending on precision we try to find the nearest point to have all segments the same size.
            /// It can't be guaranted that the curvbe will cross main control points.
            /// And this split is much more longuer to produce than points division.
            /// But the curve result is ready for animation or for step rendering.
            /// Do not animate such curve !!!!
            /// </summary>
            ConstantDistance
        }

        [HideInInspector]
        /// <summary>The type of curve 
        public CurveType curve_type__ = CurveType.Spline;
        public CurveType Curve_Type
        {
            get
            {
                if (sharePoints != null)
                    return sharePoints.curve_type__;
                else
                    return curve_type__;
            }
        }


        [HideInInspector]
        /// <summary>The type of curve for splines
        public SplineType splineType__ = SplineType.CatmullRom;
        public SplineType Spline_Type
        {
            get
            {
                if (sharePoints != null)
                    return sharePoints.splineType__;
                else
                    return splineType__;
            }
        }


        [HideInInspector]
        /// <summary>The type of curve for bezier
        public BezierType bezierType__ = BezierType.Quad;
        public BezierType Bezier_Type
        {
            get
            {
                if (sharePoints != null)
                    return sharePoints.bezierType__;
                else
                    return bezierType__;
            }
        }

        [HideInInspector]
        /// <summary>The tension of curve for Centripetal and Hermite Splines
        public float CurveTension__ = 0.5f;
        public float Curve_Tension
        {
            get
            {
                if (sharePoints != null)
                    return sharePoints.CurveTension__;
                else
                    return CurveTension__;
            }

            set
            {
                if (sharePoints != null)
                    sharePoints.CurveTension__ = value;
                else
                    CurveTension__ = value;
            }
        }

        [HideInInspector]
        /// <summary>The Bias of curve for Hermite Splines
        public float CurveBias__ = 0.5f;
        public float Curve_Bias
        {
            get
            {
                if (sharePoints != null)
                    return sharePoints.CurveBias__;
                else
                    return CurveBias__;
            }

            set
            {
                if (sharePoints != null)
                    sharePoints.CurveBias__ = value;
                else
                    CurveBias__ = value;
            }
        }

        [HideInInspector]
        public bool SimplifySourceByDistance__ = false;
        public bool Simplify_Source_By_Distance
        {
            get
            {
                if (sharePoints != null)
                    return sharePoints.SimplifySourceByDistance__;
                else
                    return SimplifySourceByDistance__;
            }
        }

        [HideInInspector]
        public float SimplifyMinDistance__ = 1;
        public float Simplify_Min_Distance
        {
            get
            {
                if (sharePoints != null)
                    return sharePoints.SimplifyMinDistance__;
                else
                    return SimplifyMinDistance__;
            }
        }


        [HideInInspector]
        /// <summary>
        /// The control Points. changing them will not rebuild a polyline until rebuildLine() is called
        /// </summary>
        public CurveControlPoint[] controlPoints = new CurveControlPoint[0];





        [HideInInspector]
        /// <summary>
        /// Loop the whole curve
        /// </summary>
        public bool loop__ = false;
        public bool Loop
        {
            get
            {
                if (sharePoints == null)
                    return loop__;
                else
                    return sharePoints.loop__;
            }
        }

        [HideInInspector]
        /// <summary>Place each Control point on the ground</summary>
        public bool VerticalRaycast__ = false;
        public bool Vertical_Raycast
        {
            get
            {
                if (sharePoints == null)
                    return Vertical_Raycast;
                else
                    return sharePoints.Vertical_Raycast;
            }
        }

        [HideInInspector]
        /// <summary>Place each point of the line on the ground</summary>
        public bool VerticalRaycastLine__ = false;
        public bool Vertical_Raycast_Line
        {
            get
            {
                if (sharePoints == null)
                    return VerticalRaycastLine__;
                else
                    return sharePoints.VerticalRaycastLine__;
            }
        }

        [HideInInspector]
        /// <summary>The height over the ground</summary>
        public float raycastHeight__ = 0.1f;
        public float Raycast_Height
        {
            get
            {
                if (sharePoints == null)
                    return raycastHeight__;
                else
                    return sharePoints.raycastHeight__;
            }
        }


        /// <summary>
        /// Draw Gizmos
        /// </summary>
        /// 
        [HideInInspector]
        public bool drawGizmos = false;

        [HideInInspector]
        public bool drawGizmosPolyline = true;

        [HideInInspector]
        public bool drawGizmosControlPoints = false;

        [HideInInspector]
        public bool drawGizmosOrientation = false;

        [HideInInspector]
        public bool drawGizmosSizes = false;

        [HideInInspector]
        public bool drawRatios = false;



        /// <summary>
        /// You can use points from another curve using this reference.
        /// Just assign this field in the inspector. Edition of point in this curve will edit the source points. 
        /// </summary>
        public Curve sharePoints = null;
        [HideInInspector]
        /// <summary>For Simple Division interpolation only : the number of segment per curve part</summary>
        public int nbPtsPerCurve = 20;

        [HideInInspector]
        /// <summary>For constant distance interpolation only : a precision ratio (multiplier for the number of points created before segment's lenght step)</summary>
        public int precision = 10;

        [HideInInspector]
        /// <summary>For constant distance interpolation only : the wanted distance between 2 points</summary>
        public float constantSegmentLenght = 1;

        [HideInInspector]
        /// <summary>For constant distance interpolation only : a ratio to place the first step position</summary>
        public float firstPointPos = 0;

        [HideInInspector]
        /// <summary>For constant distance interpolation only : The error factor. Try to have a zero value using the editor functions (computed)</summary>
        public float globalErrorFactor = 0;

        [HideInInspector]
        /// <summary>For constant distance interpolation only : The error factor. Try to have a zero value using the editor functions (computed)</summary>
        public float lastSegmentErrorFactor = 0;

        public enum SizeInterpolationMethod
        {
            Linear,
            Cosinus,
            Cubic,
            Hermite
        }

        // the way Width is interpolated
        [HideInInspector]
        public SizeInterpolationMethod widthInterpolationMethod;

        // used only for hermine Interpolation
        [HideInInspector]
        public float tensionWidth = 0f;
        // used only for hermine Interpolation
        [HideInInspector]
        public float biasWidth = 0f;

        public enum OrientationInterpolationMethod
        {
            Linear,
            Cosinus
        }

        // the way orientation is interpolated
        [HideInInspector]
        public OrientationInterpolationMethod orientationInterpolationMethod;

        [HideInInspector]
        /// <summary>The interpolation mode (simple division, contant distance...)</summary>
        public SamplingMode samplingMode = SamplingMode.PointsDivision;


        [HideInInspector]
        /// simplify the result using Min Distance algo
        public bool SimplifyResultByDistance = false;

        [HideInInspector]
        /// simplify the result using Min Distance algo
        public float SimplifyResultMinDist = 1;

        [HideInInspector]
        /// simplify the using Bend Algo
        public bool SimplifyResultByBend = false;

        [HideInInspector]
        /// simplify the result using Bend Algo
        public float SimplifyResultMinBend = 1;

        /// <summary>Computed : the result position of each point of the curve (local)</summary>
       // [SerializeField, HideInInspector]
        protected Vector3[] resultPolyline = null;
        public Vector3[] Polyline
        {
            get
            {
                if (resultPolyline == null || resultPolyline.Length == 0)
                    rebuildLine();

                return resultPolyline;
            }
        }

        public void setControlPoints(Vector3[] points, bool applyTransform = true)
        {
            List<CurveControlPoint> CtrlPoints = new List<CurveControlPoint>();
            foreach (var v in points)
            {
                CurveControlPoint pt = new CurveControlPoint();
                pt.main = v;
                if (applyTransform)
                {
                    pt.main = transform.InverseTransformPoint(pt.main);
                }
                CtrlPoints.Add(pt);
            }
            controlPoints = CtrlPoints.ToArray();
        }


        public void setControlPoints(List<Vector3> points, bool applyTransform = true)
        {
            setControlPoints(points.ToArray(), applyTransform);
        }


        public void ReverseOrder()
        {
            List<CurveControlPoint> newPoints = new List<CurveControlPoint>();
            for (int i = controlPoints.Length - 1; i >= 0; i--)
            {
                var pt = controlPoints[i];
                var newPoint = new CurveControlPoint(pt, true);
                newPoints.Add(newPoint);
            }
            controlPoints = newPoints.ToArray();
            rebuildLine();
        }

        public void RecenterPivot()
        {
            Vector3 center = Vector3.zero;
            for (int i = 0; i < controlPoints.Length; i++)
            {

                var ctrl = controlPoints[i];
                center += ctrl.main;

                ctrl.main = transform.TransformPoint(ctrl.main);
                ctrl.control1 = transform.TransformPoint(ctrl.control1);
                ctrl.control2 = transform.TransformPoint(ctrl.control2);
            }

            center = center / controlPoints.Length;
            Vector3 newPos = transform.TransformPoint(center);

            transform.localPosition = newPos;

            for (int i = 0; i < controlPoints.Length; i++)
            {

                var ctrl = controlPoints[i];

                ctrl.main = transform.InverseTransformPoint(ctrl.main);
                ctrl.control1 = transform.InverseTransformPoint(ctrl.control1);
                ctrl.control2 = transform.InverseTransformPoint(ctrl.control2);
            }
        }


        // for each point the ratio is the position in the control point array.
        // for instance a value of 3.2 means that it is between index 3 and 4 at a ratio of 0.2 between this 2 points.
        // [SerializeField, HideInInspector]
        protected float[] resultRatios;
        public float[] Ratios
        {
            get
            {
                if (resultRatios == null)
                    rebuildLine();

                return resultRatios;
            }
        }

        /// <summary>Computed : the result orientation of each poitn of the curve (local)</summary>
       // [SerializeField, HideInInspector]
        protected bool orientationReady = false;
        // [SerializeField, HideInInspector]
        protected Quaternion[] resultOrientation = null;
        public Quaternion[] Orientations
        {
            get
            {
                if (!orientationReady)
                    buildOrientation();

                return resultOrientation;
            }
        }

        // [SerializeField, HideInInspector]
        protected float[] resultUpOrientation = null;
        public float[] upOrientations
        {
            get
            {
                if (!orientationReady)
                    buildOrientation();

                return resultUpOrientation;
            }
        }


        // [SerializeField, HideInInspector]
        protected bool sizeReady = false;
        // [SerializeField, HideInInspector]
        protected float[] resultSizes = null;
        public float[] Sizes
        {
            get
            {
                if (!sizeReady)
                    buildSizes();

                return resultSizes;
            }
        }

        // [SerializeField, HideInInspector]
        protected bool lengthReady = false;
        // [SerializeField, HideInInspector]
        protected float[] resultLengths = null;
        public float[] length
        {
            get
            {
                if (!lengthReady)
                    buildLength();

                return resultLengths;
            }
        }

        // [SerializeField, HideInInspector]
        protected float total_Lenght_ = 0;
        public float totalLenght
        {
            get
            {
                if (!lengthReady)
                    buildLength();

                return total_Lenght_;
            }
        }

        [HideInInspector]
        /// <summary>The version of the curve result : incermented each time you move a point on the editor or when rebuild line is called</summary>
        public int versionIndex = 0;

        #endregion

        #region Points positions functions

        void addCubicPoints(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float startRatio, int nbPts)
        {
            float dt = 1.0f / nbPts;
            float t = dt;
            pointsList.Add(p0);
            ratiosList.Add(startRatio);

            for (int i = 1; i < nbPts; i++)
            {
                pointsList.Add(Vector3InterpolationMethods.CubicBezier(p0, p1, p2, p3, t));
                ratiosList.Add(startRatio + t);
                t += dt;
            }
        }

        void addSplinePoints(Vector3 a, Vector3 b, Vector3 c, Vector3 d, float startRatio, int nbPts)
        {
            float dt = 1.0f / nbPts;
            float t = dt;

            pointsList.Add(b);
            ratiosList.Add(startRatio);

            if (Spline_Type == SplineType.Centripetal)
            {
                Cubic3DPoly cubicpoly = new Cubic3DPoly();
                cubicpoly.Init(a, b, c, d, Curve_Tension);
                for (int i = 1; i < nbPts; i++)
                {
                    pointsList.Add(cubicpoly.eval(t));
                    ratiosList.Add(startRatio + t);
                    t += dt;
                }
            }
            else if (Spline_Type == SplineType.Hermite)
            {
                Curve_Tension = Mathf.Clamp(Curve_Tension, -1, 1);
                Curve_Bias = Mathf.Clamp(Curve_Bias, -2, 2);

                for (int i = 1; i < nbPts; i++)
                {
                    pointsList.Add(Vector3InterpolationMethods.HermiteInterpolate(a, b, c, d, t, Curve_Tension, Curve_Bias));
                    ratiosList.Add(startRatio + t);
                    t += dt;
                }
            }
            else
            {
                FourPointSplineConstruction method = Vector3InterpolationMethods.catmullRom;
                if (Spline_Type == SplineType.Cubic)
                    method = Vector3InterpolationMethods.cubic;

                for (int i = 1; i < nbPts; i++)
                {
                    pointsList.Add(method(a, b, c, d, t));
                    ratiosList.Add(startRatio + t);
                    t += dt;
                }
            }
        }

        void addQuadPoints(Vector3 st, Vector3 ct, Vector3 en, float startRatio, int nbPts)
        {
            float dt = 1.0f / nbPts;
            float t = dt;
            //   list.Add(st);
            pointsList.Add(st);
            ratiosList.Add(startRatio);


            for (int i = 1; i < nbPts; i++)
            {
                pointsList.Add(Vector3InterpolationMethods.QuadBezier(st, ct, en, t));
                ratiosList.Add(startRatio + t);
                t += dt;
            }
        }

        delegate Vector3 TwoPointSplineConstruction(Vector3 p0, Vector3 p1, float mu);
        delegate Vector3 FourPointSplineConstruction(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float mu);

        void addLinePoints(Vector3 p0, Vector3 p1, float startRatio, int nbPts)
        {
            float dt = 1.0f / nbPts;
            float t = dt;

            pointsList.Add(p0);
            ratiosList.Add(startRatio);

            TwoPointSplineConstruction method = Vector3InterpolationMethods.linear;

            for (int i = 1; i < nbPts; i++)
            {
                Vector3 p = method(p0, p1, t);
                pointsList.Add(p);
                ratiosList.Add(startRatio + t);

                t += dt;
            }
        }

        void build_Line_PolyLine(int nbPts)
        {

            for (int i = 1; i < simplifiedPoints.Count; i++)
            {
                addLinePoints(simplifiedPoints[i - 1], simplifiedPoints[i], i - 1, nbPts);
            }

            if (Loop)
            {
                addLinePoints(simplifiedPoints[simplifiedPoints.Count - 1], simplifiedPoints[0], simplifiedPoints.Count - 1, nbPts);

                pointsList.Add(simplifiedPoints[0]);
                ratiosList.Add(0);
            }
            else
            {
                pointsList.Add(simplifiedPoints[simplifiedPoints.Count - 1]);
                ratiosList.Add(simplifiedPoints.Count - 1);
            }
        }

        List<Vector3> simplifiedPoints;

        void BuildSimplifiedPoints()
        {
            List<Vector3> points = new List<Vector3>();
            CurveControlPoint[] pts = controlPoints;
            if (sharePoints != null)
                pts = sharePoints.controlPoints;

            for (int i = 0; i < pts.Length; i++)
            {
                points.Add(pts[i].main);
            }

            if (!Simplify_Source_By_Distance)
            {
                simplifiedPoints = points;
                return;
            }

            simplifiedPoints = DouglasPeuckerReduction.Apply(points, Simplify_Min_Distance);
        }

        void build_Spline_Polyline(int nbPts)
        {
            BuildSimplifiedPoints();

            switch (Spline_Type)
            {
                case SplineType.Lines:
                    build_Line_PolyLine(nbPts);
                    break;

                case SplineType.Centripetal:
                case SplineType.Cubic:
                case SplineType.CatmullRom:
                case SplineType.Hermite:
                    build_CatmullRom_PolyLine(nbPts);
                    break;
            }
        }

        void build_Bezier_Polyline(int nbPts)
        {
            switch (Bezier_Type)
            {
                case BezierType.Quad:
                    build_QuadBezier_PolyLine(nbPts);
                    break;

                case BezierType.Cubic:
                    build_CubicBezier_PolyLine(nbPts);
                    break;
            }
        }


        void build_CatmullRom_PolyLine(int nbPts)
        {
            if (simplifiedPoints.Count < 2)
                return;

            // prepare list
            Vector3[] vector3s;

            //populate calculate path;
            vector3s = new Vector3[simplifiedPoints.Count + 2];
            for (int i = 0; i < simplifiedPoints.Count; i++)
                vector3s[i + 1] = simplifiedPoints[i];

            if (Loop)
            {
                vector3s[0] = simplifiedPoints[simplifiedPoints.Count - 1];
                vector3s[vector3s.Length - 1] = simplifiedPoints[0];
            }
            else
            {
                //populate start and end control points:
                vector3s[0] = vector3s[1] + (vector3s[1] - vector3s[2]);
                vector3s[vector3s.Length - 1] = vector3s[vector3s.Length - 2] + (vector3s[vector3s.Length - 2] - vector3s[vector3s.Length - 3]);
            }

            for (int i = 0; i < vector3s.Length - 3; i++)
            {
                float startRatio = i;
                addSplinePoints(
                    vector3s[i],
                    vector3s[i + 1],
                    vector3s[i + 2],
                    vector3s[i + 3],

                   startRatio, nbPts);
            }

            if (Loop)
            {
                float startRatio = simplifiedPoints.Count - 1;

                addSplinePoints(
                    vector3s[vector3s.Length - 3],
                    vector3s[vector3s.Length - 2],
                    vector3s[1],
                    vector3s[2],
                    startRatio, nbPts);

                pointsList.Add(simplifiedPoints[0]);
                ratiosList.Add(0);
            }
            else
            {
                pointsList.Add(simplifiedPoints[simplifiedPoints.Count - 1]);
                ratiosList.Add(simplifiedPoints.Count - 1);
            }
        }

        void build_CubicBezier_PolyLine(int nbPts)
        {
            //list.Add(points[0].__main);
            CurveControlPoint[] pts = controlPoints;
            if (sharePoints != null)
                pts = sharePoints.controlPoints;

            for (int i = 1; i < pts.Length; i++)
            {
                switch (pts[i].pointMode)
                {
                    case CurveControlPoint.mode.Line:
                        addLinePoints(pts[i - 1].main, pts[i].main, i - 1, nbPts);
                        break;
                    case CurveControlPoint.mode.Symetric:
                    case CurveControlPoint.mode.Broken:
                        addCubicPoints(pts[i - 1].main,
                            pts[i - 1].control2,
                            pts[i].control1,
                            pts[i].main,
                            i - 1,
                            nbPts
                            );
                        break;
                }
            }

            if (Loop)
            {
                switch (pts[0].pointMode)
                {
                    case CurveControlPoint.mode.Line:
                        addLinePoints(pts[pts.Length - 1].main, pts[0].main, pts.Length - 1, nbPts);
                        break;
                    case CurveControlPoint.mode.Symetric:
                    case CurveControlPoint.mode.Broken:
                        addCubicPoints(pts[pts.Length - 1].main,
                            pts[pts.Length - 1].control2,
                            pts[0].control1,
                            pts[0].main,
                            pts.Length - 1,
                            nbPts
                            );
                        break;
                }

                pointsList.Add(pts[0].main);
                ratiosList.Add(0);
            }
            else
            {
                pointsList.Add(pts[pts.Length - 1].main);
                ratiosList.Add(pts.Length - 1);
            }
        }

        void build_QuadBezier_PolyLine(int nbPts)
        {
            //list.Add(points[0].__main);
            CurveControlPoint[] pts = controlPoints;
            if (sharePoints != null)
                pts = sharePoints.controlPoints;

            for (int i = 1; i < pts.Length; i++)
            {
                switch (pts[i].pointMode)
                {
                    case CurveControlPoint.mode.Line:
                        addLinePoints(pts[i - 1].main, pts[i].main, i - 1, nbPts);
                        break;
                    case CurveControlPoint.mode.Symetric:
                    case CurveControlPoint.mode.Broken:
                        addQuadPoints(pts[i - 1].main,
                            pts[i].control1,
                            pts[i].main,
                            i - 1,
                            nbPts
                            );
                        break;
                }
            }

            if (Loop)
            {
                switch (pts[0].pointMode)
                {
                    case CurveControlPoint.mode.Line:
                        addLinePoints(pts[pts.Length - 1].main, pts[0].main, pts.Length - 1, nbPts);
                        break;
                    case CurveControlPoint.mode.Symetric:
                    case CurveControlPoint.mode.Broken:
                        addQuadPoints(pts[pts.Length - 1].main,
                            pts[0].control1,
                            pts[0].main,
                            pts.Length - 1,
                            nbPts
                            );
                        break;
                }

                pointsList.Add(pts[0].main);
                ratiosList.Add(0);
            }
            else
            {
                //     list.Add(pts[pts.Length - 1].main);
                pointsList.Add(pts[pts.Length - 1].main);
                ratiosList.Add(pts.Length - 1);
            }
        }

        void verticalRaycastLine()
        {
            for (int i = 0; i < pointsList.Count; i++)
            {
                Vector3 pt = (Vector3)pointsList[i];
                pt.y = 0;

                Vector3 pos = transform.TransformPoint(pt);
                Vector3 startPos = pos + Vector3.up * 10000;
                Ray ray = new Ray(startPos, -Vector3.up);

                RaycastHit hit = new RaycastHit();
                if (Physics.Raycast(ray, out hit, Mathf.Infinity))
                {
                    Vector3 deltaPos = transform.InverseTransformPoint(
                        hit.point + Vector3.up * Raycast_Height);

                    pt = deltaPos;
                }

                pointsList[i] = pt;
            }
        }

        List<Vector3> pointsList;
        List<float> ratiosList;

        void buildPolyLine(int nbPtsperPart)
        {
            pointsList = new List<Vector3>();
            ratiosList = new List<float>();

            switch (Curve_Type)
            {
                case CurveType.Spline:
                    build_Spline_Polyline(nbPtsperPart);
                    break;
                case CurveType.Bezier:
                    build_Bezier_Polyline(nbPtsperPart);
                    break;
            }

            if (Vertical_Raycast_Line)
            {
                verticalRaycastLine();
            }

            if (Curve_Type == CurveType.Bezier || !Simplify_Source_By_Distance)
                computeForwardControls(nbPtsperPart);

            if (SimplifyResultByDistance)
            {
                var pointsToKeep = DouglasPeuckerReduction.GetPointsToKeep(pointsList, SimplifyResultMinDist);
                List<Vector3> returnPoints = new List<Vector3>();
                List<float> returnRatios = new List<float>();
                foreach (int index in pointsToKeep)
                {
                    returnPoints.Add(pointsList[index]);
                    returnRatios.Add(ratiosList[index]);
                }

                pointsList = returnPoints;
                ratiosList = returnRatios;
            }
        }

        void computeForwardControls(int nbPtsperPart)
        {
            // Compute forward dir for controls
            // If shared points, this curve is really managed via another one. 
            // Stops here. 
            if (sharePoints != null)
                return;

  //          Debug.Log("resultPolyline.Length = " + pointsList.Count);
            // compute main points orientations
            for (int i = 1; i < controlPoints.Length - 1; i++)
            {
                int indexPts = i * nbPtsperPart;
                controlPoints[i].forward = computeForward(pointsList[indexPts - 1], pointsList[indexPts], pointsList[indexPts + 1]);
            }

            if (Loop && pointsList.Count > 2)
            {
                //    Vector3 dir = polyline[1] - polyline[polyline.Length - 2];
                controlPoints[0].forward = computeForward(pointsList[pointsList.Count - 2], pointsList[0], pointsList[1]);

                int indexPts = (controlPoints.Length - 1) * nbPtsperPart;

                controlPoints[controlPoints.Length - 1].forward = computeForward(pointsList[indexPts - 1], pointsList[indexPts], pointsList[indexPts + 1]);
            }
            else
            {
                Vector3 dir = pointsList[1] - pointsList[0];
                controlPoints[0].forward = dir.normalized;

                dir = pointsList[pointsList.Count - 1] - pointsList[pointsList.Count - 2];
                controlPoints[controlPoints.Length - 1].forward = dir.normalized;
            }
        }


        Vector3 computeForward(Vector3 prevPt, Vector3 Pt, Vector3 nextPt)
        {
            Vector3 t1 = (prevPt - Pt).normalized;
            Vector3 t2 = (nextPt - Pt).normalized;

            Vector3 forward = (t2 - t1).normalized;

            return forward;
        }

        Quaternion buildQuat(CurveControlPoint pt)
        {
            if (pt.forward == Vector3.zero)
                return Quaternion.Euler(0, 0, pt.CurveUpOrientation);
            else
                return Quaternion.LookRotation(pt.forward) * Quaternion.Euler(0, 0, pt.CurveUpOrientation);
        }

        void buildOrientation()
        {
            CurveControlPoint[] pts = controlPoints;
            if (sharePoints)
                pts = sharePoints.controlPoints;

            // 2 points at least
            if (pts.Length < 2)
                return;

            // 3 pts for loop
            if (Loop && pts.Length < 3)
                return;

            if (resultRatios == null)
            {
                Debug.LogError("Internal error, no ratios");

                return;
            }

            if (resultRatios.Length != resultPolyline.Length)
            {
                Debug.LogError("Internal error, wrong sizes");
                Debug.LogError(resultRatios.Length + " ratios != points " + resultPolyline.Length);

                return;
            }

            resultOrientation = new Quaternion[resultPolyline.Length];
            resultUpOrientation = new float[resultPolyline.Length];

            // Build CurveUp values temp array, adding loop values to fit interpolation
            Quaternion[] CurveUpOrientations = new Quaternion[pts.Length + 3];
            if (Loop)
            {
                CurveUpOrientations[0] = buildQuat(pts[pts.Length - 1]);
                CurveUpOrientations[CurveUpOrientations.Length - 2] = buildQuat(pts[0]);
                CurveUpOrientations[CurveUpOrientations.Length - 1] = buildQuat(pts[1]);
            }
            else
            {
                CurveUpOrientations[0] = buildQuat(pts[0]);
                CurveUpOrientations[CurveUpOrientations.Length - 1] = buildQuat(pts[pts.Length - 1]);
            }

            for (int i = 0; i < pts.Length; i++)
            {
                CurveUpOrientations[i + 1] = buildQuat(pts[i]);
            }

            for (int i = 1; i < resultOrientation.Length - 1; i++)
            {
                float ratio = resultRatios[i];

                int indexControl = (int)ratio;

                float localratio = ratio - indexControl;

                // special case for last value

                Quaternion upQuat = Quaternion.identity;

                switch (orientationInterpolationMethod)
                {
                    case OrientationInterpolationMethod.Linear:
                        upQuat = Quaternion.SlerpUnclamped(CurveUpOrientations[indexControl + 1], CurveUpOrientations[indexControl + 2], localratio);
                        break;
                    default:
                    case OrientationInterpolationMethod.Cosinus:
                        float mu2 = (1 - Mathf.Cos(localratio * Mathf.PI)) / 2;
                        upQuat = Quaternion.SlerpUnclamped(CurveUpOrientations[indexControl + 1], CurveUpOrientations[indexControl + 2], mu2);
                        break;
                }

                Vector3 upDir = upQuat * Vector3.up;

                resultUpOrientation[i] = 0;
                Vector3 forward = computeForward(resultPolyline[i - 1], resultPolyline[i], resultPolyline[i + 1]);

                Quaternion quat = Quaternion.identity;
                if (forward == Vector3.zero)
                {

                    resultOrientation[i] = quat;
                }
                else
                {

                    resultOrientation[i] = Quaternion.LookRotation(forward, upDir);
                }
            }

            // first and last
            if (Loop && resultPolyline.Length > 2)
            {
                //Debug.Log("loop orientation"
                Vector3 forward = computeForward(resultPolyline[resultPolyline.Length - 2], resultPolyline[0], resultPolyline[1]);
                resultUpOrientation[0] = pts[0].CurveUpOrientation;
                if (forward == Vector3.zero)
                    resultOrientation[0] = Quaternion.identity;
                else
                {
                    resultOrientation[0] = Quaternion.LookRotation(forward) * Quaternion.Euler(0, 0, pts[0].CurveUpOrientation);
                }

                //	Debug.Log(dir.normalized);
                resultOrientation[resultPolyline.Length - 1] = resultOrientation[0];
                resultUpOrientation[resultPolyline.Length - 1] = pts[0].CurveUpOrientation;
            }
            else
            {
                Vector3 dir = resultPolyline[1] - resultPolyline[0];
                if (dir.normalized.magnitude == 0)
                    resultOrientation[0] = Quaternion.identity;
                else
                    resultOrientation[0] = Quaternion.LookRotation(dir.normalized) * Quaternion.Euler(0, 0, pts[0].CurveUpOrientation);

                resultUpOrientation[0] = pts[0].CurveUpOrientation;
                resultUpOrientation[resultPolyline.Length - 1] = pts[pts.Length - 1].CurveUpOrientation;

                dir = resultPolyline[resultPolyline.Length - 1] - resultPolyline[resultPolyline.Length - 2];
                if (dir.magnitude == 0)
                    resultOrientation[resultPolyline.Length - 1] = Quaternion.identity;
                else
                    resultOrientation[resultPolyline.Length - 1] = Quaternion.LookRotation(dir.normalized) * Quaternion.Euler(0, 0, pts[pts.Length - 1].CurveUpOrientation);
            }

            orientationReady = true;
        }

        void buildOrientationOldStyle()
        {
            CurveControlPoint[] pts = controlPoints;
            if (sharePoints)
                pts = sharePoints.controlPoints;

            if (resultRatios == null)
            {
                Debug.LogError("Internal error, no ratios");

                return;
            }

            if (resultRatios.Length != resultPolyline.Length)
            {
                Debug.LogError("Internal error, wrong sizes");
                Debug.LogError(resultRatios.Length + " ratios != points " + resultPolyline.Length);

                return;
            }

            // build quaternions
            resultOrientation = new Quaternion[resultPolyline.Length];

            // first and last
            if (Loop && resultPolyline.Length > 2)
            {
                //Debug.Log("loop orientation"
                Vector3 forward = computeForward(resultPolyline[resultPolyline.Length - 2], resultPolyline[0], resultPolyline[1]);

                if (forward == Vector3.zero)
                    resultOrientation[0] = Quaternion.identity;
                else
                {
                    resultOrientation[0] = Quaternion.LookRotation(forward) * Quaternion.Euler(0, 0, pts[0].CurveUpOrientation);
                }

                //	Debug.Log(dir.normalized);
                resultOrientation[resultPolyline.Length - 1] = resultOrientation[0];
            }
            else
            {
                Vector3 dir = resultPolyline[1] - resultPolyline[0];
                if (dir.normalized.magnitude == 0)
                    resultOrientation[0] = Quaternion.identity;
                else
                    resultOrientation[0] = Quaternion.LookRotation(dir.normalized) * Quaternion.Euler(0, 0, pts[0].CurveUpOrientation);

                dir = resultPolyline[resultPolyline.Length - 1] - resultPolyline[resultPolyline.Length - 2];
                if (dir.magnitude == 0)
                    resultOrientation[resultPolyline.Length - 1] = Quaternion.identity;
                else
                    resultOrientation[resultPolyline.Length - 1] = Quaternion.LookRotation(dir.normalized) * Quaternion.Euler(0, 0, pts[pts.Length - 1].CurveUpOrientation);
            }

            int indexCur = -1;
            Quaternion curQuat = Quaternion.identity;
            Quaternion nextQuat = Quaternion.identity;
            for (int i = 1; i < resultPolyline.Length - 1; i++)
            {
                float ratio = resultRatios[i];
                int indexControl = (int)ratio;
                float localratio = ratio - indexControl;

                if (indexCur != indexControl)
                {
                    indexCur = indexControl;
                    if (pts[indexControl].forward != Vector3.zero)
                        curQuat = Quaternion.LookRotation(pts[indexControl].forward) * Quaternion.Euler(0, 0, pts[indexControl].CurveUpOrientation);
                    else
                        curQuat = Quaternion.Euler(0, 0, pts[indexControl].CurveUpOrientation);

                    if (indexControl < pts.Length - 1)
                    {
                        if (pts[indexControl + 1].forward != Vector3.zero)
                            nextQuat = Quaternion.LookRotation(pts[indexControl + 1].forward) * Quaternion.Euler(0, 0, pts[indexControl + 1].CurveUpOrientation);
                        else
                            nextQuat = Quaternion.Euler(0, 0, pts[indexControl + 1].CurveUpOrientation);
                    }
                    else
                    {
                        nextQuat = Quaternion.LookRotation(pts[0].forward) * Quaternion.Euler(0, 0, pts[0].CurveUpOrientation);

                        if (pts[0].forward != Vector3.zero)
                            nextQuat = Quaternion.LookRotation(pts[0].forward) * Quaternion.Euler(0, 0, pts[0].CurveUpOrientation);
                        else
                            nextQuat = Quaternion.Euler(0, 0, pts[0].CurveUpOrientation);
                    }
                }

                Vector3 forward = computeForward(resultPolyline[i - 1], resultPolyline[i], resultPolyline[i + 1]);

                Quaternion quat = Quaternion.SlerpUnclamped(curQuat, nextQuat, localratio);

                if (forward == Vector3.zero)
                {
                    resultOrientation[i] = Quaternion.identity;
                }
                else
                {
                    //	Vector3 upDir = Quaternion.Euler(0, 0,  Angle)*Vector3.up;
                    Vector3 upDir = quat * Vector3.up;

                    //upDir = Vector3.up;
                    if (upDir == Vector3.zero)
                        resultOrientation[i] = Quaternion.LookRotation(forward, Vector3.up);
                    else
                        resultOrientation[i] = Quaternion.LookRotation(forward, upDir);
                }
            }

            orientationReady = true;
        }

        void buildSizes()
        {
            CurveControlPoint[] pts = controlPoints;
            if (sharePoints)
                pts = sharePoints.controlPoints;

            // 2 points at least
            if (pts.Length < 2)
                return;

            // 3 pts for loop
            if (Loop && pts.Length < 3)
                return;

            if (resultRatios == null)
            {
                Debug.LogError("Internal error, no ratios");

                return;
            }

            if (resultRatios.Length != resultPolyline.Length)
            {
                Debug.LogError("Internal error, wrong sizes");
                Debug.LogError(resultRatios.Length + " ratios != points " + resultPolyline.Length);

                return;
            }

            resultSizes = new float[resultPolyline.Length];

            float[] tmpSizes = new float[pts.Length + 3];

            if (Loop)
            {
                tmpSizes[0] = pts[pts.Length - 1].Size;
                tmpSizes[tmpSizes.Length - 2] = pts[0].Size;
                tmpSizes[tmpSizes.Length - 1] = pts[1].Size;
            }
            else
            {
                tmpSizes[0] = 2 * pts[0].Size - pts[1].Size;
                tmpSizes[tmpSizes.Length - 1] = 2 * pts[pts.Length - 1].Size - pts[pts.Length - 2].Size;
            }

            for (int i = 0; i < pts.Length; i++)
            {
                tmpSizes[i + 1] = pts[i].Size;
            }

            for (int i = 0; i < resultSizes.Length; i++)
            {
                float ratio = resultRatios[i];

                int indexControl = (int)ratio;

                float localratio = ratio - indexControl;

                // special case for last value
                float size = 0;

                switch (widthInterpolationMethod)
                {
                    case SizeInterpolationMethod.Linear:
                        size = InterpolationMethods.LinearInterpolate(tmpSizes[indexControl + 1], tmpSizes[indexControl + 2], localratio);
                        break;
                    case SizeInterpolationMethod.Cosinus:
                        size = InterpolationMethods.CosineInterpolate(tmpSizes[indexControl + 1], tmpSizes[indexControl + 2], localratio);
                        break;
                    case SizeInterpolationMethod.Cubic:
                        size = InterpolationMethods.CubicInterpolate(
                            tmpSizes[indexControl], tmpSizes[indexControl + 1], tmpSizes[indexControl + 2], tmpSizes[indexControl + 3],
                            localratio);
                        break;
                    case SizeInterpolationMethod.Hermite:
                        size = InterpolationMethods.HermiteInterpolate(
                            tmpSizes[indexControl], tmpSizes[indexControl + 1], tmpSizes[indexControl + 2], tmpSizes[indexControl + 3],
                            localratio,
                            tensionWidth, biasWidth);
                        break;
                    default:
                        break;
                }


                resultSizes[i] = size;

            }

            sizeReady = true;
        }

        void buildPolyline_ConstantDistance()
        {
            CurveControlPoint[] pts = controlPoints;
            if (sharePoints)
                pts = sharePoints.controlPoints;

            // reset all simplification flags
            SimplifyResultByDistance = false;
            SimplifyResultByBend = false;


            // Use approximation
            // first build a huge range of points (2000 per curve)

            int nbPtsPerPart = precision * 10;
            buildPolyLine(nbPtsPerPart);

            //	Debug.Log("lastPoint " + lastPoint);
            List<Vector3> subPoints = new List<Vector3>();
            List<float> subRatios = new List<float>();

            float firstStep = constantSegmentLenght * firstPointPos;
            Vector3 PrevPt = pointsList[0];
            int firstIndex = 0;
            // setup first point
            int i;
            float dist = 0;
            for (i = 0; i < pointsList.Count; i++)
            {
                dist = (pointsList[i] - PrevPt).magnitude;
                if (dist >= firstStep)
                {
                    // add first
                    subPoints.Add(pointsList[i]);
                    subRatios.Add(ratiosList[i]);

                    PrevPt = pointsList[i];
                    firstIndex = i;
                    break;
                }
            }
            float meanDist = 0;

            int startIndex = firstIndex;
            int controlPointIndex = (int)ratiosList[startIndex];

            if (sharePoints == null)
            {
                for (i = 0; i <= controlPointIndex; i++)
                {
                    // sets startIndexes
                    controlPoints[i].StartIndex = startIndex;
                }
            }


            for (i = firstIndex; i < pointsList.Count; i++)
            {
                dist = (pointsList[i] - PrevPt).magnitude;
                int ctrlIndex = (int)ratiosList[i];
                if (sharePoints == null)
                {
                    if (ctrlIndex != controlPointIndex)
                    {
                        for (int indexC = controlPointIndex + 1; indexC <= ctrlIndex; indexC++)
                        {
                            // sets startIndexes
                            controlPoints[indexC].StartIndex = i;
                        }

                        controlPointIndex = ctrlIndex;
                    }
                }


                if (dist >= constantSegmentLenght)
                {
                    subPoints.Add(pointsList[i]);
                    subRatios.Add(ratiosList[i]);

                    meanDist += dist;

                    PrevPt = pointsList[i];
                }
            }

            meanDist = meanDist / (pointsList.Count - 1);

            globalErrorFactor = (meanDist - constantSegmentLenght) / constantSegmentLenght;

            if (subPoints.Count <= 2)
            {
                subPoints.Add(pts[0].main);
                subPoints.Add(pts[pts.Length - 1].main);

                subRatios.Add(0);
                subRatios.Add(pts.Length - 1);

                lastSegmentErrorFactor = 0;
            }
            else
            {
                // at last we remove the last point to set the real last point
                // the last segment we have a error lenght set in errorFactor
                subPoints.RemoveAt(subPoints.Count - 1);
                subRatios.RemoveAt(subRatios.Count - 1);

                Vector3 lastPoint;
                if (Loop)
                {
                    lastPoint = pts[0].main;
                    subRatios.Add(0);
                }
                else
                {
                    lastPoint = pts[pts.Length - 1].main;
                    subRatios.Add(pts.Length - 1);
                }

                subPoints.Add(lastPoint);


                dist = ((Vector3)subPoints[subPoints.Count - 2] - lastPoint).magnitude;
                lastSegmentErrorFactor = (dist - constantSegmentLenght) / constantSegmentLenght;
            }

            resultPolyline = subPoints.ToArray();
            resultRatios = subRatios.ToArray();

        }

        #endregion

        #region public functions



        /// <summary>
        ///Rebuild the whole interpolated line.
        ///
        ///Call this every time controls points or curve parameter has changed
        ///This could be long expetially if raycast is done and if constant distance is set.
        /// </summary>
        public void rebuildLine()
        {
            //   Debug.Log("Rebuild Line " + Time.frameCount);


            orientationReady = false;
            sizeReady = false;
            lengthReady = false;

            //list.Add(points[0].__main);
            CurveControlPoint[] pts = controlPoints;
            if (sharePoints != null)
                pts = sharePoints.controlPoints;

            if (pts.Length < 2)
            {
                if (pts.Length == 0)
                {
                    orientationReady = true;
                    sizeReady = true;

                    resultPolyline = new Vector3[0];
                    resultOrientation = new Quaternion[0];
                    resultSizes = new float[0];
                }
                else
                {
                    orientationReady = true;
                    sizeReady = true;


                    resultPolyline = new Vector3[1];
                    resultPolyline[0] = pts[0].main;
                    resultOrientation = new Quaternion[1];
                    resultOrientation[0] = Quaternion.identity;
                    resultSizes = new float[1];
                    resultSizes[0] = pts[0].Size;
                }

                versionIndex++;
                return;
            }

            if (samplingMode == SamplingMode.PointsDivision)
            {
                buildPolyLine(nbPtsPerCurve);

                resultPolyline = pointsList.ToArray();
                resultRatios = ratiosList.ToArray();

                for (int i = 0; i < controlPoints.Length; i++)
                {
                    // sets startIndexes
                    controlPoints[i].StartIndex = i * nbPtsPerCurve;
                }
            }
            else if (samplingMode == SamplingMode.ConstantDistance)
            {
                buildPolyline_ConstantDistance();
            }

            versionIndex++;
        }

        void buildLength()
        {

            Vector3[] points = Polyline;
            if (points.Length == 0)
                return;

            resultLengths = new float[points.Length];
            resultLengths[0] = 0;
            total_Lenght_ = 0;

            for (int i = 1; i < resultLengths.Length; i++)
            {
                resultLengths[i] = (points[i] - points[i - 1]).magnitude;
                total_Lenght_ += resultLengths[i];
            }

            lengthReady = true;
        }

        /// <summary>
        /// Find the nearest position (vector3) to the curve. Warning it can take quite a long time !
        /// </summary>
        /// <param name="pos">the position to check (in global coordinates)</param>
        /// <returns>the nearest pos on curve (in global coordinates)</returns>
        public Vector3 findNearestPos(Vector3 pos)
        {
            pos = transform.InverseTransformPoint(pos);
            float minDist = 1000000000000000;
            int index = -1;
            for (int i = 0; i < resultPolyline.Length; i++)
            {
                float dist = (resultPolyline[i] - pos).sqrMagnitude;
                if (dist < minDist)
                {
                    index = i;
                    minDist = dist;
                }
            }

            return transform.TransformPoint(resultPolyline[index]);
        }

        /// <summary>
        /// Find the nearest position to the curve. Warning it can take quite a long time !
        /// </summary>
        /// <param name="pos">the position to check</param>
        /// <returns>the nearest time on curve</returns>
        public float findNearestTime(Vector3 pos)
        {
            pos = transform.InverseTransformPoint(pos);
            float minDist = float.MaxValue;
            int index = -1;
            for (int i = 0; i < resultPolyline.Length; i++)
            {
                float dist = (resultPolyline[i] - pos).sqrMagnitude;
                if (dist < minDist)
                {
                    index = i;
                    minDist = dist;
                }
            }

            return ((float)index) / (resultPolyline.Length - 1);
        }

        /// <summary>
        /// Gets a position depending on a global time.
        /// </summary>
        /// <param name="globalTime">0-1 value</param>
        /// <param name="pos">the returned position</param>
        /// <param name="rot">the returned rotation</param>
        public void getPos(float globalTime, ref Vector3 pos, ref Quaternion rot)
        {
            getPos(globalTime, ref pos, ref rot, false);
        }

        /// <summary>
        /// Gets a postion depending on a global time.
        /// </summary>
        /// <param name="globalTime">0-1 value</param>
        /// <param name="pos">the returned position</param>
        /// <param name="rot">the returned rotation</param>
        /// <param name="world">if true converts the value to world coordinates</param>
        public void getPos(float globalTime, ref Vector3 pos, ref Quaternion rot, bool world)
        {
            if (resultPolyline == null)
                rebuildLine();

            if (globalTime <= 0)
            {
                pos = resultPolyline[0];
                rot = Orientations[0];
            }
            else if (globalTime >= 1)
            {
                pos = resultPolyline[resultPolyline.Length - 1];
                rot = Orientations[Orientations.Length - 1];
            }
            else
            {
                int index = (int)((resultPolyline.Length - 1) * globalTime);
                if (index < (resultPolyline.Length - 1))
                {
                    float dt = 1.0f / (resultPolyline.Length - 1);
                    float startTime = dt * index;
                    float localTime = (globalTime - startTime) / dt;

                    pos = Vector3.Lerp(resultPolyline[index], resultPolyline[index + 1], localTime);
                    rot = Quaternion.Lerp(Orientations[index], Orientations[index + 1], localTime);
                }
                else
                {
                    pos = resultPolyline[resultPolyline.Length - 1];
                    rot = Orientations[resultPolyline.Length - 1];
                }
            }

            if (world)
            {
                pos = transform.TransformPoint(pos);
                rot = transform.rotation * rot;
            }
        }

        /// <summary>
        /// return a position gieven a lenght on the curve
        /// </summary>
        /// <param name="len">the lenght</param>

        /// <returns>a time</returns>
        public void getPosByLen(float len, ref Vector3 pos, ref Quaternion rot, bool world)
        {
            if (resultPolyline.Length < 2)
                return;

            else if (len <= 0)
            {
                Vector3 deltaPos = resultPolyline[1] - resultPolyline[0];
                float firstLen = resultLengths[1];
                float delta = len / firstLen;

                pos = resultPolyline[0] + delta * deltaPos;
                rot = Orientations[0];
                if (world)
                {
                    pos = transform.TransformPoint(pos);
                    rot = transform.rotation * rot;
                }
                return;
            }
            else if (len >= totalLenght)
            {
                Vector3 deltaPos = resultPolyline[resultPolyline.Length - 1] - resultPolyline[resultPolyline.Length - 2];
                float remain = len - totalLenght;

                float lastLen = resultLengths[resultLengths.Length - 1];

                float delta = remain / lastLen;

                pos = resultPolyline[resultPolyline.Length - 1] + delta * deltaPos;

                rot = Orientations[Orientations.Length - 1];
                if (world)
                {
                    pos = transform.TransformPoint(pos);
                    rot = transform.rotation * rot;
                }

                return;
            }

            float remaining = len;

            //       float total = totalLenght;

            int index = 0;
            while (remaining > resultLengths[index])
            {
                remaining -= resultLengths[index];
                index++;
                if (index >= resultLengths.Length)
                {
                    index--;
                    remaining = resultLengths[index];
                    pos = resultPolyline[resultPolyline.Length - 1];
                    rot = Orientations[Orientations.Length - 1];
                    if (world)
                    {
                        pos = transform.TransformPoint(pos);
                        rot = transform.rotation * rot;
                    }

                    return;
                }
            }


            if (index <= 0)
            {
                pos = resultPolyline[0];
                rot = Orientations[0];
                if (world)
                {
                    pos = transform.TransformPoint(pos);
                    rot = transform.rotation * rot;
                }

                return;
            }
            else if (index >= resultLengths.Length)
            {
                pos = resultPolyline[resultPolyline.Length - 1];
                rot = Orientations[Orientations.Length - 1];
                if (world)
                {
                    pos = transform.TransformPoint(pos);
                    rot = transform.rotation * rot;
                }

            }
            else
            {
                // in between point i-1 and i
                float ratio = Mathf.InverseLerp(0, resultLengths[index], remaining);

                pos = Vector3.Lerp(resultPolyline[index - 1], resultPolyline[index], ratio);
                rot = Quaternion.Lerp(Orientations[index - 1], Orientations[index], ratio);

                if (world)
                {
                    pos = transform.TransformPoint(pos);
                    rot = transform.rotation * rot;
                }
            }

        }

        /// <summary>
        /// Used for access to mid points : access to a position between 2 control points on the curve depending on a interpolation ratio (0-1)
        /// </summary>
        /// <param name="indexPoint">the index in the control points</param>
        /// <param name="t">the time [0-1] between this and the next control</param>
        /// <returns>a position</returns>
        public Vector3 getPos(int indexPoint, float t)
        {
            if (indexPoint < 0 || indexPoint >= controlPoints.Length) return Vector3.zero;

            int index = controlPoints[indexPoint].StartIndex;
            if (resultRatios == null || index < 0 || index >= resultRatios.Length)
                rebuildLine();

            float ratio = resultRatios[index];
            float wantedRatio = (float)indexPoint + t;
            while (ratio < wantedRatio)
            {
                index++;
                if (index >= resultRatios.Length)
                {
                    // does not exists, return last        
                    index--;
                    break;
                }
                ratio = resultRatios[index];
            }

            return resultPolyline[index];
        }

        /// <summary>
        /// Gets the whole polyline converted to world coordinates
        /// </summary>
        /// <returns>the whole polyline</returns>
        public Vector3[] WorldPolyLine
        {
            get
            {
                if (resultPolyline == null) rebuildLine();

                Vector3[] array = new Vector3[resultPolyline.Length];

                for (int i = 0; i < resultPolyline.Length; i++)
                {
                    array[i] = transform.TransformPoint(resultPolyline[i]);
                }

                return array;
            }

        }

        public Quaternion[] getWorldOrientations()
        {
            Quaternion[] array = new Quaternion[Orientations.Length];

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = transform.rotation * Orientations[i];
            }

            return array;
        }

        CurveControlPoint[] getCtrlPoints()
        {
            CurveControlPoint[] pts = controlPoints;
            if (sharePoints != null)
                pts = sharePoints.controlPoints;

            return pts;
        }

        public void resetPivotScaleRot()
        {
            CurveControlPoint[] pts = getCtrlPoints();

            float localScale = transform.localScale.x + transform.localScale.y + transform.localScale.z;
            localScale /= 3;


            // reset all points position according to localScale
            for (int i = 0; i < pts.Length; i++)
            {
                pts[i].main = transform.TransformPoint(pts[i].main) - transform.position;
                pts[i].control1 = transform.TransformPoint(pts[i].control1) - transform.position;
                pts[i].control2 = transform.TransformPoint(pts[i].control2) - transform.position;

                pts[i].Size = Mathf.Abs(pts[i].Size * localScale);
            }


            constantSegmentLenght = constantSegmentLenght * localScale;

            CurveRenderer renderer = gameObject.GetComponent<CurveRenderer>();
            if (renderer != null && renderer != null)
            {
                renderer.minWidth = renderer.minWidth * localScale;
                renderer.maxWidth = renderer.maxWidth * localScale;

                renderer.UvAdaptation = renderer.UvAdaptation * localScale;
            }

            transform.localScale = Vector3.one;
            transform.rotation = Quaternion.identity;
        }

        #endregion

        #region Mono Behaviour functions
        void Update()
        {
            if (sharePoints != null && sharePoints.versionIndex != versionIndex)
            {
                rebuildLine();
                versionIndex = sharePoints.versionIndex;
            }
        }
        #endregion
    }


}


