﻿using UnityEngine;
using System.Reflection;
using System;

namespace Curves.Attributes
{

    // The visibility Attribute is used by Other drawers to Hide/
    public class VisibleAttribute : PropertyAttribute
    {
        public string[] BooleanMembers;
        public bool invert = false;
        public bool grayed = false;

        public bool initOK = false;
        public MethodInfo[] methods;
        public FieldInfo[] fields;

        // if the BooleanMember ends with (), it will be a function
        public VisibleAttribute(string BooleanMember) : this(BooleanMember, false)
        {

        }

        public VisibleAttribute(string BooleanMember, bool invert)
        {
            this.BooleanMembers = new string[] { BooleanMember };
            this.invert = invert;
        }

        public VisibleAttribute(bool invert, params string[] BooleanMembers)
        {
            this.BooleanMembers = BooleanMembers;
            this.invert = invert;
        }

        public VisibleAttribute(params string[] BooleanMembers) : this(false, BooleanMembers)
        {

        }


        public void init(Type type)
        {
            if (initOK)
                return;

            methods = new MethodInfo[BooleanMembers.Length];
            fields = new FieldInfo[BooleanMembers.Length];
            for (int i = 0; i < BooleanMembers.Length; i++)
            {
                string member = BooleanMembers[i];
                if (string.IsNullOrEmpty(member))
                    continue;

                if (member.EndsWith("()"))
                {
                    string method = member.Substring(0, member.IndexOf('('));
                    MethodInfo theMethod = type.GetMethod(method,
                        BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

                    if (theMethod == null)
                    {
                        Debug.LogError("No Method with name " + method + " in " + type);
                        continue;
                    }

                    methods[i] = theMethod;
                }
                else
                {
                    FieldInfo field = type.GetField(member,
                          BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

                    if (field == null)
                    {

                        continue;
                    }

                    fields[i] = field;

                }
            }

            initOK = true;
        }

    }

}
