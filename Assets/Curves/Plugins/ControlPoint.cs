﻿using UnityEngine;


namespace Curves
{
    [System.Serializable]

    /// <summary>
    /// The Curve Control point. This class is used as a contrainer for control points information.
    /// You can edit them programmaticcaly without trouble but remember thats changes will occur only when rebuildLine is called on the curve object.
    /// 
    /// Depending on curve type not all values are needed
    /// 
    /// The curves controls points are in Curve.controlPoints's array
    /// </summary>
    public class CurveControlPoint
    {
        public CurveControlPoint() { }
        public CurveControlPoint(CurveControlPoint source, bool reverse)
        {
            this.pointMode = source.pointMode;
            this.main = source.main;
            if (reverse)
            {
                this.control1 = source.control2;
                this.control2 = source.control1;
            }
            else
            {
                this.control1 = source.control1;
                this.control2 = source.control2;
            }
            
            this.CurveUpOrientation = source.CurveUpOrientation;
            this.Size = source.Size;
           
        }


        /// <summary>
        /// The type of curve part (used only in cubic bezier)
        /// This type determine the way curve is set for the previous part of this point
        /// </summary>
        public enum mode
        {
            /// <summary> a simple Line  </summary>
            Line,
            /// <summary> c1 and c2 are symetrics  </summary>
            Symetric,
            /// <summary> c1 and c2 are differents  </summary>
            Broken
        }

        /// <summary> The kind of control point </summary>
        public mode pointMode = mode.Symetric;

        /// <summary> The main control point : for any time of curve, it will go through this point </summary>
        public Vector3 main = Vector3.zero;
        /// <summary> Used for Quad and cubic bezier </summary>
        public Vector3 control1 = Vector3.zero;
        /// <summary> Used for cubic bezier only </summary>
        public Vector3 control2 = Vector3.zero;

        /// <summary> The Up Direction of the curve. 0 is absolute up  </summary>
        public float CurveUpOrientation = 0; // 0 for up

        // the size of the node if needed by the renderer
        public float Size = 1;

        // the direction of the forward vector
        [HideInInspector]
        public Vector3 forward;

        // Start Index in the polyline for this control point
        [HideInInspector]
        public int StartIndex;
    }


}