﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Curves
{
    //! this class is just responsible of updating UI graphic when the curve renderer is used in UI MODE
    public class CurveUIRenderer : Graphic
    {

        protected override void UpdateGeometry()
        {
            canvasRenderer.SetMesh(myMesh);
        }

        protected override void UpdateMaterial()
        {
            canvasRenderer.materialCount = 1;
            canvasRenderer.SetMaterial(material, 0);
        }

        [HideInInspector]
        public Mesh myMesh = null;

        public void setMesh(Mesh mesh, Material mat)
        {
            canvasRenderer.materialCount = 1;
            myMesh = mesh;
            canvasRenderer.SetMesh(mesh);
            canvasRenderer.SetMaterial(mat, 0);

            material = mat;
           
            SetAllDirty();
        }
    }
}