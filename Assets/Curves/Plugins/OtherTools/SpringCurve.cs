using UnityEngine;
using System.Collections;

namespace Curves.Tools
{
    [ExecuteInEditMode]
    public class SpringCurve : MonoBehaviour
    {
        public float startAngle = 0;
        public float StartAngle { set { needChange = true; startAngle = value; } }

        public float nbTurns = 5;
        public float NbTurns { set { needChange = true; nbTurns = value; } }

        public float radius = 10;
        public float Radius { set { needChange = true; radius = value; } }

        public float height = 10;
        public float Height { set { needChange = true; height = value; } }

        public AnimationCurve radiusCurve;
        public AnimationCurve HeightCurve;

        public int nbPoints = 100;
        public float NbPoints { set { needChange = true; nbPoints = (int) value; } }

        public Curve curve;

        // Use this for initialization
        void Start()
        {

        }

        Vector3[] pos = null;

        /*void OnDrawGizmos()
		{
			if (pos == null) return;
			for (int i = 0; i < pos.Length-1; i++)
			{
				Gizmos.DrawSphere(transform.TransformPoint( pos[i]), 1);
				Gizmos.DrawLine(transform.TransformPoint(pos[i]), transform.TransformPoint(pos[i+1]));

			}

			Gizmos.DrawSphere(pos[pos.Length - 1], 1);
		}*/

        bool needChange = false;

        private void OnValidate()
        {
            needChange = true;
        }

        // Update is called once per frame
        void Update()
        {
            if (!needChange) return;

            needChange = false;


            // auto generate only in edit mode
            if (curve == null)
                curve = GetComponent<Curve>();

            if (curve == null || nbPoints <= 0)
                return;

            float endAngle = 360 * nbTurns + startAngle;

            float angle = Mathf.Deg2Rad * startAngle;
            float deltaAngle = Mathf.Deg2Rad * (endAngle - startAngle) / (nbPoints - 1);
            float progress = 0;
            float delta = 1f / (nbPoints - 1);

            pos = new Vector3[nbPoints];

            //	float 

            for (int i = 0; i < nbPoints; i++)
            {
                //	Debug.Log("progress " + progress);

                float r = radiusCurve.Evaluate(progress) * radius;

                //	Debug.Log("r " + r);
                float h = Mathf.Lerp(-height / 2, height / 2, HeightCurve.Evaluate(progress));

                pos[i] = new Vector3(

                    Mathf.Cos(angle) * r,
                    h,
                    Mathf.Sin(angle) * r
                    );

                progress += delta;


                angle += deltaAngle;
            }

            curve.controlPoints = new CurveControlPoint[nbPoints];

            for (int i = 0; i < nbPoints; i++)
            {
                curve.controlPoints[i] = new CurveControlPoint();
                curve.controlPoints[i].main = pos[i];
            }

            curve.rebuildLine();


        }
    }


}