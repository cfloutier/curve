﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Curves.Tools
{
    // usefull for cameras
    [AddComponentMenu("Curves/Mobe On Curve")]
    [ExecuteInEditMode]
    public class MoveOnPath : MonoBehaviour
    {
        public Curve curve;

        [Range(0, 1)]
        public float progress;

        public enum OrientationMode
        {
            Curve,
            Point,
            None
        }
        public OrientationMode orientMode = OrientationMode.Curve;

        [Tooltip("Used Only if orientMode is point")]
        public Transform lookAtPoint;


        private void Update()
        {
            if (curve == null) return;


            Vector3 pos = Vector3.zero;
            Quaternion rot = Quaternion.identity;
            curve.getPos(progress, ref pos, ref rot, true);

            switch (orientMode)
            {
                case OrientationMode.Curve:
                    {
                        transform.rotation = rot;
                    }
                    break;
                case OrientationMode.Point:
                    {
                        if (lookAtPoint != null)
                            transform.LookAt(lookAtPoint.position, Vector3.up);
                    }
                    break;
                case OrientationMode.None:
                    break;
                default:
                    break;
            }
            
            transform.position = pos;
        }

    }
}