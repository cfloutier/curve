﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Curves.Tools
{
    using System;

#if UNITY_EDITOR
    using UnityEditor;

    public class MyAssetModificationProcessor : AssetModificationProcessor
    {
        public static string[] OnWillSaveAssets(string[] paths)
        {
            var save = GameObject.FindObjectOfType<CleanupBeforeSave>();
            if (save != null)
                save.CleanUp();

            return paths;
        }
    }

#endif

    /// <summary>
    /// This class is used to cleanup the save before save 
    /// to avoid having huges meshes to save in the scene
    /// </summary>
    public class CleanupBeforeSave : MonoBehaviour
    {
        [Header("All curve renderers will be cleaned before saving")]
        public bool CleanUpRenderer = true;

        [Header("If set, all curve Dispatchers will be cleaned before saving")]
        public bool CleanUpDispatcher = true;

        [Header("If set, all curve rendererer will be rebuild on start")]
        public bool rebuildRenderersOnAwake = true;

        [Header("If set, all curve Dispatchers will be rebuild on start")]
        public bool rebuildDispatchersOnAwake = true;

        public void CleanUp()
        {
            Debug.Log("CleanUp curve and dispatch from '" + name + "'");
            if (CleanUpRenderer)
            {
                var renderers = GameObject.FindObjectsOfType<CurveRenderer>();
                foreach (var r in renderers)
                {
                    r.CleanUp();
                }
            }

            if (CleanUpDispatcher)
            {
                var dispatchers = GameObject.FindObjectsOfType<Dispatcher>();
                foreach (var d in dispatchers)
                {
                    d.CleanUp();
                }
            }
        }






        public void RebuildRenderers()
        {
            var renderers = GameObject.FindObjectsOfType<CurveRenderer>();
            foreach (var r in renderers)
            {
                r.Rebuild();
            }
        }

        public void RebuildDispatchers()
        {
            var dispatchers = GameObject.FindObjectsOfType<Dispatcher>();
            foreach (var d in dispatchers)
            {
                d.CleanupAndBuild();
            }
        }

        public void RebuildNow()
        {
            Awake();
        }



        private void Awake()
        {
            if (rebuildRenderersOnAwake)
            {
                RebuildRenderers();
            }

            if (rebuildDispatchersOnAwake)
            {
                RebuildDispatchers();
            }
        }
    }

}