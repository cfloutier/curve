﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.Events;





namespace Curves.Tools
{
    public class CurveEvent : MonoBehaviour
    {
        public PositionOnCurve sourceObject;

        public float currentCurvePos
        {
            set
            {


            }
        }

        float lastPos = -1;
        void Update()
        {
            if (sourceObject == null) return;
            float pos = sourceObject.position;
            if (pos == lastPos)
                return;

            lastPos = pos;
            bool inside = isInside(pos);

            if (isOn)
            {
                if (!inside)
                {
                    switchOff.Invoke();
                    isOn = false;
                }
            }
            else
            {
                if (inside)
                {
                    switchOn.Invoke();
                    isOn = true;
                }
            }
        }

        bool isInside(float value)
        {
            if (value < startPos)
            {
                if (value > startPos + Len - 1)
                {
                    return false;
                }
                return true;
            }
            else
            {
                if (value < startPos + Len)
                    return true;

                return false;
            }
        }

        [Range(0, 1)]
        public float startPos;

        [Range(0, 1)]
        public float Len;

        public UnityEvent switchOn;
        public UnityEvent switchOff;

        bool isOn = false;

#if UNITY_EDITOR
        public void OnDrawGizmosSelected()
        {
            if (sourceObject == null) return;
            if (sourceObject.curve == null)
                return;

            var curve = sourceObject.curve;
            Vector3 start = Vector3.zero;
            Quaternion rotation = Quaternion.identity;

            curve.getPos(startPos, ref start, ref rotation, true);
            float adapt = HandleUtility.GetHandleSize(start) * 0.1f;
            Gizmos.color = Color.green;
            Gizmos.DrawCube(start, adapt * Vector3.one);

            float endPos = startPos + Len;
            if (endPos > 1)
                endPos -= 1;

            Vector3 end = Vector3.zero;
            curve.getPos(endPos, ref end, ref rotation, true);
            adapt = HandleUtility.GetHandleSize(end) * 0.1f;
            Gizmos.color = Color.red;
            Gizmos.DrawCube(end, adapt * Vector3.one);
            Gizmos.color = Color.green;
            int NbPts = 50;
            Vector3 prev = start;
            Vector3 pos = prev;
            for (int i = 1; i < NbPts + 1; i++)
            {
                float ratio = ((float)i) / NbPts;
                float progress = startPos + Len * ratio;

                if (progress > 1)
                    progress -= 1;
                curve.getPos(progress, ref pos, ref rotation, true);

                Gizmos.DrawLine(prev, pos);
                prev = pos;
            }

            curve.getPos(sourceObject.position, ref pos, ref rotation, true);
            adapt = HandleUtility.GetHandleSize(pos) * 0.1f;
            Gizmos.color = isInside(sourceObject.position) ? Color.green : Color.red;
            Gizmos.DrawSphere(pos, adapt );

        }
#endif 
    }
}