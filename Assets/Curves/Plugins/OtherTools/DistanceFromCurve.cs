﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Curves.Tools
{
    [ExecuteInEditMode]
    public class DistanceFromCurve : MonoBehaviour
    {
        public Curve curve;

        public float distance;

        public static float SqrDistanceToPath(Vector3 v, List<Vector3> path)
        {
            float minDistance = float.MaxValue;

            for (int pathPointIdx = 1; pathPointIdx < path.Count; pathPointIdx++)
            {
                float d = SqrDistanceToSegment(v, path[pathPointIdx - 1], path[pathPointIdx]);

                if (d < minDistance)
                    minDistance = d;
            }

            return minDistance;
        }

        public static float SqrDistanceToSegment(Vector3 v, Vector3 a, Vector3 b)
        {
            Vector3 ab = b - a;
            Vector3 av = v - a;

            if (Vector3.Dot(av, ab) <= 0f)              // Point is lagging behind start of the segment, so perpendicular distance is not viable.
                return av.magnitude;          // Use distance to start of segment instead.

            Vector3 bv = v - b;

            if (Vector3.Dot(bv, ab) >= 0f)              // Point is advanced past the end of the segment, so perpendicular distance is not viable.
                return bv.magnitude;          // Use distance to end of the segment instead.

            // Point is within the line segment's start/finish boundaries, so perpendicular distance is viable.
            return Vector3.Cross(ab, av).sqrMagnitude / ab.sqrMagnitude; // Perpendicular distance of point to segment.
        }

        public static float CurveDistance(Vector3 p, Curve c)
        {
            List<Vector3> points = new List<Vector3>(c.Polyline);
            if (points.Count == 0)
                return 0;

            if (c.Loop)
            {
                points.Add(points[0]);
            }

            return Mathf.Sqrt(   SqrDistanceToPath(p, points));
        }


        // Update is called once per frame
        void Update()
        {
            if (curve == null)
                return;

            distance = CurveDistance(transform.position, curve);
        }
    }
}
