using UnityEngine;
using Curves.Attributes;


#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Curves.Tools
{


    [AddComponentMenu("Curves/Curve Object Dispatcher")]
    public class CurveDispatch : Dispatcher
    {
        [Header("Main")]
        public GameObject objectToDispatch;
        public Curve alongCurve;

        [Header("Random Setting")]
        [Range(0, 10000)]
        [Tooltip("Used for random position, change it to roll a new dice")]
        public float seed = 0;

        public enum PositionMode
        {
            addConstant,
            randomBetween
        }

        bool modeConstantPos() { return posMode == PositionMode.addConstant; }
        [Header("Position")]
        public PositionMode posMode = PositionMode.addConstant;

        [Visible("modeConstantPos()")]
        public Vector2 deltaPos = new Vector2(0, 0);

        [Visible("modeConstantPos()", true)]
        public Vector3 minRandomPos = new Vector3(0, 0);
        [Visible("modeConstantPos()", true)]
        public Vector3 maxRandomPos = new Vector3(0, 0);

        [Header("Count")]
        public int nb = 100;

        [Header("Position on Curve")]
        public bool randomPosOnCurve;


        public enum OrientationMode
        {
            FollowCurve,
            Random
        }

        [Header("Orientation")]
        public OrientationMode orientationMode;

        bool showFollowCurvePos()
        {
            return orientationMode == OrientationMode.FollowCurve;
        }

        [Visible("showFollowCurvePos()")]
        public Vector3 orientation;

        public bool addRandom;
        public bool stayVertical;

        [Visible("addRandom")]
        public Vector3 RandomOrientationRange;



        public enum ScaleMode
        {
            Prefab,
            Rescale,
            Random
        }

        public ScaleMode scaleMode = ScaleMode.Rescale;
        bool showScale()
        {
            return scaleMode == ScaleMode.Rescale;
        }

        [Visible("showScale")]
        public float Scale = 1;


        [Range(0, 1f)]
        public float startPos = 0;

        [Range(0, 1f)]
        public float endPos = 0;

        public bool drawGizmos = true;



        void OnValidate()
        {
            buildPositions();
        }
#if UNITY_EDITOR
        void OnDrawGizmosSelected()
        {
            if (!drawGizmos) return;

            if (positions == null) return;
            Color green = Color.green;
            green.a = 0.5f;
            Color red = Color.red;
            red.a = 0.5f;
            Color blue = Color.blue;
            blue.a = 0.5f;

            if (alongCurve != null)
            {
                Gizmos.color = green;

                Vector3 pos = Vector3.zero;
                Quaternion rot = Quaternion.identity;
                alongCurve.getPos(startPos, ref pos, ref rot);

                pos = alongCurve.transform.TransformPoint(pos);
                float adapt = HandleUtility.GetHandleSize(pos);

                Matrix4x4 m = new Matrix4x4();
                m.SetTRS(pos, rot, adapt * Vector3.one * 0.5f);

                Gizmos.matrix = m;

                Gizmos.DrawCube(new Vector3(0, 0, 0), new Vector3(1, 1, 1));

                Gizmos.color = red;

                alongCurve.getPos(endPos, ref pos, ref rot);
                pos = alongCurve.transform.TransformPoint(pos);
                adapt = HandleUtility.GetHandleSize(pos);
                m.SetTRS(pos, rot, adapt * Vector3.one * 0.5f);
                Gizmos.matrix = m;
                Gizmos.DrawCube(new Vector3(0, 0, 0), new Vector3(1, 1, 1));
            }

            for (int i = 0; i < positions.Length; i++)
            {
                Matrix4x4 m = new Matrix4x4();
                m.SetTRS(positions[i], orientations[i], scales[i]);
                Gizmos.matrix = m;
                Gizmos.color = blue;
                Gizmos.DrawCube(new Vector3(0, 0, 0), new Vector3(0.2f, 0.2f, 1));

                Gizmos.color = green;
                Gizmos.DrawCube(new Vector3(0, 0, 0), new Vector3(0.2f, 1, 0.2f));

                Gizmos.color = red;
                Gizmos.DrawCube(new Vector3(0, 0, 0), new Vector3(1, 0.2f, 0.2f));
            }
        }
#endif

        Vector3[] positions;
        Quaternion[] orientations;
        Vector3[] scales;

        public void buildPositions()
        {
            Random.InitState((int)seed);
            if (nb < 0)
                nb = 1;

            positions = new Vector3[nb];
            orientations = new Quaternion[nb];
            scales = new Vector3[nb];

            float t = startPos;

            float dt = 0;

            dt = (endPos - startPos) / (nb - 1);
            if (startPos > endPos)
            {
                dt = (endPos + 1 - startPos) / (nb - 1);
            }



            Quaternion addedorientation = Quaternion.Euler(orientation);

            for (int i = 0; i < nb; i++)
            {
                //GameObject newObj = (GameObject)GameObject.Instantiate(objectToDispatch);

                if (alongCurve == null)
                {
                    positions[i] = transform.position;
                    orientations[i] = Quaternion.identity;
                }
                else
                {
                    if (randomPosOnCurve)
                    {
                        if (startPos > endPos)
                        {
                            t = Random.Range(startPos, endPos + 1);
                            if (t > 1)
                                t = t - 1;
                        }
                        else
                            t = Random.Range(startPos, endPos);
                    }


                    Vector3 pos = Vector3.zero;
                    Quaternion rot = Quaternion.identity;
                    // get the position on curve
                    alongCurve.getPos(t, ref pos, ref rot);

                    positions[i] = alongCurve.transform.TransformPoint(pos);
                    orientations[i] = alongCurve.transform.rotation * rot;



                    t += dt;

                    if (t > 1)
                        t -= 1;
                }



                Matrix4x4 m = new Matrix4x4();
                m.SetTRS(positions[i], orientations[i], Vector3.one);
                Vector3 forward = m * Vector3.forward;
                Vector3 up = m * Vector3.up;
                Vector3 right = m * Vector3.right;

                if (posMode == PositionMode.addConstant)
                    positions[i] += right * deltaPos.x + up * deltaPos.y;
                else
                {
                    Vector3 added = new Vector3(
                       Random.Range(minRandomPos.x, maxRandomPos.x),
                       Random.Range(minRandomPos.y, maxRandomPos.y),
                       Random.Range(minRandomPos.z, maxRandomPos.z));

                    positions[i] += right * added.x + up * added.y + forward * added.z;
                }

                switch (scaleMode)
                {
                    case ScaleMode.Prefab:
                        scales[i] = Vector3.one;
                        break;
                    case ScaleMode.Rescale:
                        scales[i] = Vector3.one * Scale;
                        break;
                    case ScaleMode.Random:
                        scales[i] = Vector3.one * Random.Range(Scale, Scale);
                        break;
                }




                switch (orientationMode)
                {
                    case OrientationMode.FollowCurve:
                        orientations[i] = alongCurve.transform.rotation * orientations[i] * addedorientation;
                        break;
                    case OrientationMode.Random:
                        orientations[i] = Random.rotation * addedorientation;
                        break;
                    default:
                        break;
                }
                if (addRandom)
                {
                    orientations[i] *= Quaternion.Euler(
                        Random.Range(-RandomOrientationRange.x, RandomOrientationRange.x),
                       Random.Range(-RandomOrientationRange.y, RandomOrientationRange.y),
                       Random.Range(-RandomOrientationRange.z, RandomOrientationRange.z));
                }
                if (stayVertical)
                {
                    Vector3 euler = orientations[i].eulerAngles;
                    euler.x = euler.z = 0;
                    orientations[i] = Quaternion.Euler(euler);
                }

            }

        }

        public override void Build()
        {
            buildPositions();


            for (int i = 0; i < nb; i++)
            {
                GameObject newObj = (GameObject)GameObject.Instantiate(objectToDispatch);
                newObj.transform.position = positions[i];
                newObj.transform.rotation = orientations[i];
                if (scaleMode != ScaleMode.Prefab)
                    newObj.transform.localScale = scales[i];

                newObj.transform.parent = transform;
            }


        }


    }
}