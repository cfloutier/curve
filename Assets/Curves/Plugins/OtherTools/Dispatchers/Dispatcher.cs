using UnityEngine;
using Curves.Attributes;
using Curves.Tools.MeshFusion;


#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Curves.Tools
{
    public class Dispatcher : MonoBehaviour
    {
        public bool mergeResult = false;

        public virtual void CleanUp()
        {
            while (transform.childCount > 0)
            {
                GameObject.DestroyImmediate(transform.GetChild(0).gameObject);
            }
        }

        public virtual void Build()
        {
            Debug.Log("Not implemented");
        }

        public virtual void CleanupAndBuild()
        {
            CleanUp();
            Build();
            if (mergeResult)
                Merge();
        }

        public virtual void Merge()
        {
            var filters = MeshMerge.ListMeshFilter(transform);
            GameObject target = new GameObject();
            target.transform.SetParent(transform);
            target.transform.setIdentity();
            target.transform.SetParent(null);
            target.name = "merged";

            MeshMerge.Merge(filters, "merged", target.transform);
            CleanUp();
            target.transform.parent = transform;
        }
    }
}