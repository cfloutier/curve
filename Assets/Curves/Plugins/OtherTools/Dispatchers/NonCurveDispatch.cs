﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Curves.Tools
{
    public class NonCurveDispatch : Dispatcher
    {
        [Header("Main")]
        public GameObject[] objectsToDispatch;
        public Curve avoidCurve;

        public Vector2 minMaxDistance = new Vector2(1, 30);

        public LayerMask verticalRaycast;
        public float raycastH = 0;

        public int Nb;

        public Vector2 Range = Vector2.one;

        [Header("Random Setting")]
        [Range(0, 10000)]
        [Tooltip("Used for random position, change it to roll a new dice")]
        public int seed = 0;

        [Range(0,180)]
        public float rangeRotation;

        [Range(0, 2)]
        public float rangeRescale;

        Vector3 RandomPos
        {
            get
            {
                Vector3 pos = transform.position + new Vector3(Random.Range(-Range.x / 2, Range.x / 2), 0, Random.Range(-Range.y / 2, Range.y / 2));
                if (verticalRaycast.value != 0)
                {
                    RaycastHit hit = new RaycastHit();
                    if (Physics.Raycast(pos + Vector3.up * 10000, -Vector3.up, out hit, Mathf.Infinity, verticalRaycast.value))
                    {
                        pos = hit.point + Vector3.up * raycastH;
                    }
                }

                return pos;
            }
        }

        void addItem()
        {
            int indexObj = Random.Range(0, objectsToDispatch.Length);

            GameObject go = Instantiate(objectsToDispatch[indexObj]);
            Vector3 pos = RandomPos;
            if (avoidCurve != null)
            {
                float distance = -1;
                int tries = 0;
                while (tries < 20)
                {
                    distance = DistanceFromCurve.CurveDistance(pos, avoidCurve);
                    if (distance <= minMaxDistance.y && distance >= minMaxDistance.x)
                        break;

                    pos = RandomPos;
                    tries++;
                }
            }

            go.transform.position = pos;
            go.transform.localEulerAngles = new Vector3(0, Random.Range(-rangeRotation, rangeRotation));
            Vector3 scale = go.transform.localScale;
            float resc = Random.Range(-rangeRescale, rangeRescale);

            scale = scale * Mathf.Pow(2, resc);
            go.transform.localScale = scale;
            go.transform.parent = transform;


        }

        public override void Build()
        {
            Random.InitState(seed);


            for (int i = 0; i < Nb; i++)
                addItem();


        }

        public void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.cyan;

            Gizmos.DrawWireCube(transform.position, new Vector3(Range.x, 0, Range.y));
        }
    }
}