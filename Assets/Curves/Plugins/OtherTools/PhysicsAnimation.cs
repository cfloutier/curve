using UnityEngine;
using System.Collections;
using Curves;
using System;
using UnityEngine.Events;

namespace Curves.Tools
{

    [Serializable]
    public class FloatEvent : UnityEvent<float> { }

    [ExecuteInEditMode]
    public class PhysicsAnimation : MonoBehaviour
    {
        [Tooltip("The curve to follow")]
        public Curve curve;

        [Tooltip("Delta Position of this transform with to curve")]
        public Vector3 deltaPos = Vector3.up;

        [Tooltip("Start Position, and informative position during play")]
        [Range(0, 1f)]
        public float curPos = 0;

        [Tooltip("Start Speed, and informative position during play (m/s)")]
        public float speed;

        [Tooltip("if set the speed is set by outside control")]
        public bool _setSpeed = false;
        public bool setSpeed
        {
            set { _setSpeed = value; }
            get { return _setSpeed; }
        }


        [Tooltip("the value to set (in km/s)")]
        public float _setSpeedValue = 0;
        public float setSpeedValue
        {
            set { _setSpeedValue = value; }
            get { return _setSpeedValue; }
        }

        [Tooltip("speed restriction (in km/h)")]
        public float maxSpeed = 100;

        [Tooltip("Informative curve Size, Useless to change it")]
        public float curveSize = 1;

        [Tooltip("Main Engine Power, depends on Engine Ratio (in m/s�)")]
        public float _EnginePower = 10;
        public float EnginePower
        {
            set { _EnginePower = value; }
            get { return _EnginePower; }
        }

        [Tooltip("Current multiplier for engine, can be set using user inputs ")]
        [Range(-1, 1)]
        public float _EngineRatio = 0;
        public float EngineRatio
        {
            set { _EngineRatio = value; }
            get { return _EngineRatio; }
        }

        [Tooltip("Fricton of air and rails (in m/s�)")]
        public float Friction = 10;

        [Tooltip("Brake Or Not")]
        public bool Brake = false;

        [Tooltip("Brake power when set to ON")]
        public float BrakePower = 100;

        [Tooltip("Force speed to zero when under this value (in m/s)")]
        public float brakeMinSpeed;

        [Tooltip("Use Gravity power or not")]
        public bool addGravity = true;
        [Tooltip("Gravity (in m/s�)")]
        public float Gravity = 10;

        public UnityEngine.UI.Text speedMetter;

        [System.Serializable]
        public class Controls
        {
            public bool InputControls = true;
            public string engineControl = "Vertical";
            public string brake = "Fire2";
        }

        public Controls controls;



        public FloatEvent currentCurveEvent;

        void Start()
        {
            if (curve == null) return;
            // gets the size
            curveSize = curve.totalLenght;
        }

        void Update()
        {

            if (!Application.isPlaying) return;


            if (curve == null || curveSize == 0)
                return;


            // convert speed to position on curve
            float ratioSpeed = speed / curveSize;

            // adds delta pos to current
            curPos += ratioSpeed * Time.deltaTime;


            //	speed += Input.GetAxis("Vertical") * sensibilty * Time.deltaTime;
            if (controls.InputControls)
            {
                _EngineRatio = Input.GetAxis(controls.engineControl);
                Brake = Input.GetAxis(controls.brake) > 0;
            }

            if (_setSpeed)
            {
                float setSpeedMs = _setSpeedValue / 3.6f;
                speed = Mathf.Lerp(speed, setSpeedMs, Time.deltaTime);
            }
            else
            {
                speed -= speed * Friction / 100 * Time.deltaTime;
                if (addGravity)
                {
                    Vector3 gravityDirection = Vector3.down * Gravity;
                    gravityDirection = transform.InverseTransformDirection(gravityDirection);
                    speed += gravityDirection.z * Time.deltaTime;
                }

                speed += _EnginePower * _EngineRatio * Time.deltaTime;
                if (Brake)
                {
                    speed -= speed * BrakePower / 100 * Time.deltaTime;

                    if (Mathf.Abs(speed) < brakeMinSpeed)
                        speed = 0;
                }
            }



            float maxSpeedMs = maxSpeed / 3.6f;
            speed = Mathf.Clamp(speed, -maxSpeedMs, maxSpeedMs);


            setPOS();
        }





        void setPOS()
        {
            Quaternion rot = Quaternion.identity;
            Vector3 pos = Vector3.zero; ;

            // check that we're still on the range 0-1
            while (curPos > 1)
                curPos -= 1;
            while (curPos < 0)
                curPos += 1;

            // get values from the curve
            curve.getPos(curPos, ref pos, ref rot, true);

            // apply it to current transform
            transform.rotation = rot;
            transform.position = pos + rot * deltaPos;

            if (speedMetter != null)
            {
                speedMetter.text = "" + (speed * 3600 / 1000).ToString("0.0") + " Km/h";
            }

            currentCurveEvent.Invoke(curPos);
        }






        void OnValidate()
        {
            if (!Application.isPlaying)
            {
                setPOS();
            }
        }
    }
}