using UnityEngine;
using System.Collections;
using Curves;
namespace Curves.Tools
{
    /// <summary>
    /// The base class for all position related object on curve
    /// </summary>
    public class PositionOnCurve : MonoBehaviour
    {
        // the curve to anim on
        public Curve curve;

        [Range(0, 1f)]
        public float position;
    }


    [AddComponentMenu("Curves/Speed Curve Anim")]
    [ExecuteInEditMode]
    public class SpeedCurveAnim : PositionOnCurve
    {
        // the speed to set
        public float speedKMH;
        public float SpeedKMH
        {
            set
            {
                speedKMH = value;
            }
        }

        // a delta position to place the camera depending on the curve
        public Vector3 deltaPos = Vector3.up;

        // total curve's size
        float curveSize = 1;

        void Start()
        {
            if (curve == null) return;

            // gets the size
            curveSize = curve.totalLenght;
        }

        public bool stayVertical = false;

        [Header("Use arrows to go forward or back")]
        public bool useInput;

        void Update()
        {
            if (curve == null || curveSize == 0)
                return;

            Quaternion rot = Quaternion.identity;
            Vector3 pos = Vector3.zero; ;

            if (Application.isPlaying)
            {
                // convert to m/s
                float realSpeed = speedKMH / 3.6f;

                if (useInput)
                    realSpeed *= Input.GetAxis("Vertical") + Input.GetAxis("Horizontal");


                // convert speed to position on curve
                float ratioSpeed = realSpeed / curveSize;

                // adds delta pos to current
                position += ratioSpeed * Time.deltaTime;              
            }

            // check that we're still on the range 0-1
            while (position > 1)
                position -= 1;
            while (position < 0)
                position += 1;

            // get values from the curve
            curve.getPos(position, ref pos, ref rot, true);

            // apply it to current transform
           
            if (stayVertical)
            {
                Vector3 euler = rot.eulerAngles;
                euler.z = 0;
                euler.x = 0;
                transform.localEulerAngles = euler;
                

                
            }
            else
                transform.rotation = rot;

            Vector3 prevPos = transform.position;
            transform.position = pos + rot * deltaPos;
        }
    }
}