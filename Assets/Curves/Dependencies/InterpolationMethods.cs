﻿
using UnityEngine;

namespace Curves.Maths
{
    public class InterpolationMethods
    {
        public static float LinearInterpolate(
          float y1, float y2,
          float mu)
        {
            return (y1 * (1 - mu) + y2 * mu);
        }

        public static float CosineInterpolate(
           float y1, float y2,
           float mu)
        {
            float mu2 = (1 - Mathf.Cos(mu * Mathf.PI)) / 2;
            return (y1 * (1 - mu2) + y2 * mu2);
        }

        public static float CubicInterpolate(
            float y0, float y1,
            float y2, float y3,
            float mu)
        {
            float a0, a1, a2, a3, mu2;

            mu2 = mu * mu;
            a0 = y3 - y2 - y0 + y1;
            a1 = y0 - y1 - a0;
            a2 = y2 - y0;
            a3 = y1;

            return (a0 * mu * mu2 + a1 * mu2 + a2 * mu + a3);
        }

        public static float CatmulRomInterpolate(
           float y0, float y1,
           float y2, float y3,
           float mu)
        {
            float a0, a1, a2, a3, mu2;

            mu2 = mu * mu;
            a0 = -0.5f * y0 + 1.5f * y1 - 1.5f * y2 + 0.5f * y3;
            a1 = y0 - 2.5f * y1 + 2f * y2 - 0.5f * y3;
            a2 = -0.5f * y0 + 0.5f * y2;
            a3 = y1;

            return (a0 * mu * mu2 + a1 * mu2 + a2 * mu + a3);
        }


        /*
    Tension: 1 is high, 0 normal, -1 is low
    Bias: 0 is even,
         positive is towards first segment,
         negative towards the other
    */
        public static float HermiteInterpolate(
           float y0, float y1,
           float y2, float y3,
           float mu,
           float tension,
           float bias)
        {
            float m0, m1, mu2, mu3;
            float a0, a1, a2, a3;

            mu2 = mu * mu;
            mu3 = mu2 * mu;
            m0 = (y1 - y0) * (1 + bias) * (1 - tension) / 2;
            m0 += (y2 - y1) * (1 - bias) * (1 - tension) / 2;
            m1 = (y2 - y1) * (1 + bias) * (1 - tension) / 2;
            m1 += (y3 - y2) * (1 - bias) * (1 - tension) / 2;
            a0 = 2 * mu3 - 3 * mu2 + 1;
            a1 = mu3 - 2 * mu2 + mu;
            a2 = mu3 - mu2;
            a3 = -2 * mu3 + 3 * mu2;

            return (a0 * y1 + a1 * m0 + a2 * m1 + a3 * y2);
        }




    }


    public class Vector3InterpolationMethods
    {
        public static Vector3 linear(Vector3 p0, Vector3 p1, float mu)
        {
            return (1 - mu) * p0 + mu * p1;
        }

        public static Vector3 cosinus(Vector3 p0, Vector3 p1, float mu)
        {
            float mu2 = (1 - Mathf.Cos(mu * Mathf.PI)) / 2;
            return (p0 * (1 - mu2) + p1 * mu2);
        }

        public static Vector3 cubic(Vector3 y0, Vector3 y1, Vector3 y2, Vector3 y3, float mu)
        {
            Vector3 a0, a1, a2, a3;
            float mu2;

            mu2 = mu * mu;
            a0 = y3 - y2 - y0 + y1;
            a1 = y0 - y1 - a0;
            a2 = y2 - y0;
            a3 = y1;

            return (a0 * mu * mu2 + a1 * mu2 + a2 * mu + a3);
        }

        public static Vector3 catmullRom(Vector3 y0, Vector3 y1, Vector3 y2, Vector3 y3, float mu)
        {
            Vector3 a0, a1, a2, a3;
            float mu2;

            mu2 = mu * mu;
            a0 = -0.5f * y0 + 1.5f * y1 - 1.5f * y2 + 0.5f * y3;
            a1 = y0 - 2.5f * y1 + 2 * y2 - 0.5f * y3;
            a2 = -0.5f * y0 + 0.5f * y2;
            a3 = y1;

            return (a0 * mu * mu2 + a1 * mu2 + a2 * mu + a3);
        }

        public static Vector3 CubicBezier(Vector3 st, Vector3 c1, Vector3 c2, Vector3 en, float t)
        {
            float d = 1f - t;
            return d * d * d * st +
                    3 * d * d * t * c1 +
                    3 * d * t * t * c2 +
                    t * t * t * en;
        }

        public static Vector3 QuadBezier(Vector3 st, Vector3 ct, Vector3 en, float t)
        {
            float d = 1f - t;
            return d * d * st + 2f * d * t * ct + t * t * en;
        }


        /*
           Tension: 1 is high, 0 normal, -1 is low
           Bias: 0 is even,
                 positive is towards first segment,
                 negative towards the other
        */
        public static Vector3 HermiteInterpolate(
           Vector3 y0, Vector3 y1,
           Vector3 y2, Vector3 y3,
           float mu,
           float tension,
           float bias)
        {
            Vector3 m0, m1;
            float mu2, mu3;
            float a0, a1, a2, a3;

            mu2 = mu * mu;
            mu3 = mu2 * mu;
            m0 = (y1 - y0) * (1 + bias) * (1 - tension) / 2;
            m0 += (y2 - y1) * (1 - bias) * (1 - tension) / 2;
            m1 = (y2 - y1) * (1 + bias) * (1 - tension) / 2;
            m1 += (y3 - y2) * (1 - bias) * (1 - tension) / 2;
            a0 = 2 * mu3 - 3 * mu2 + 1;
            a1 = mu3 - 2 * mu2 + mu;
            a2 = mu3 - mu2;
            a3 = -2 * mu3 + 3 * mu2;

            return (a0 * y1 + a1 * m0 + a2 * m1 + a3 * y2);
        }
    }

    public class Cubic3DPoly
    {
        CubicPoly px, py, pz;

        public static float DistSqr(Vector3 p0, Vector3 p1)
        {
            return (p0 - p1).sqrMagnitude;
        }

        public void InitCatmullRom(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
        {
            Init(p0, p1, p2, p3, 0);
        }

        public void InitCentripedal(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
        {
            Init(p0, p1, p2, p3, 0.5f);
        }

        public void InitChordal(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
        {
            Init(p0, p1, p2, p3, 1);
        }

        public void Init(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float alpha)
        {
            px = new CubicPoly();
            py = new CubicPoly();
            pz = new CubicPoly();

            float dt0 = 1.0f;
            float dt1 = 1.0f;
            float dt2 = 1.0f;

            if (alpha > 1)
                alpha = 1;
            else if (alpha < 0)
                alpha = 0;

            if (alpha != 0)
            {
                dt0 = Mathf.Pow(DistSqr(p0, p1), alpha * 0.5f);
                dt1 = Mathf.Pow(DistSqr(p1, p2), alpha * 0.5f);
                dt2 = Mathf.Pow(DistSqr(p2, p3), alpha * 0.5f);
            }

            // safety check for repeated points
            if (dt1 < 1e-4f) dt1 = 1.0f;
            if (dt0 < 1e-4f) dt0 = dt1;
            if (dt2 < 1e-4f) dt2 = dt1;

            px.InitNonuniformCatmullRom(p0.x, p1.x, p2.x, p3.x, dt0, dt1, dt2);
            py.InitNonuniformCatmullRom(p0.y, p1.y, p2.y, p3.y, dt0, dt1, dt2);
            pz.InitNonuniformCatmullRom(p0.z, p1.z, p2.z, p3.z, dt0, dt1, dt2);
        }

        public Vector3 eval(float mu)
        {
            return new Vector3(px.eval(mu), py.eval(mu), pz.eval(mu));
        }
    }

    public class CubicPoly
    {
        float c0, c1, c2, c3;

        public float eval(float t)
        {
            float t2 = t * t;
            float t3 = t2 * t;
            return c0 + c1 * t + c2 * t2 + c3 * t3;
        }


        public void InitCubicPoly(float x0, float x1, float t0, float t1)
        {
            this.c0 = x0;
            this.c1 = t0;
            this.c2 = -3 * x0 + 3 * x1 - 2 * t0 - t1;
            this.c3 = 2 * x0 - 2 * x1 + t0 + t1;
        }

        // standard Catmull-Rom spline: interpolate between x1 and x2 with previous/following points x1/x4
        // (we don't need this here, but it's for illustration)
        public void InitCatmullRom(float x0, float x1, float x2, float x3)
        {
            // Catmull-Rom with tension 0.5
            InitCubicPoly(x1, x2, 0.5f * (x2 - x0), 0.5f * (x3 - x1));
        }

        // compute coefficients for a nonuniform Catmull-Rom spline
        public void InitNonuniformCatmullRom(float x0, float x1, float x2, float x3, float dt0, float dt1, float dt2)
        {
            // compute tangents when parameterized in [t1,t2]
            float t1 = (x1 - x0) / dt0 - (x2 - x0) / (dt0 + dt1) + (x2 - x1) / dt1;
            float t2 = (x2 - x1) / dt1 - (x3 - x1) / (dt1 + dt2) + (x3 - x2) / dt2;

            // rescale tangents for parametrization in [0,1]
            t1 *= dt1;
            t2 *= dt1;

            InitCubicPoly(x1, x2, t1, t2);
        }
    }
}