using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Curves.Dependencies
{
    public static class TransformExtensionMethods
    {

        /// <summary>
        /// Returns the .position attributes from a list of transforms.
        /// </summary>
        /// <param name='transforms'>
        /// Transforms.
        /// </param>
        public static Vector3[] Positions(this IList<Transform> transforms)
        {
            Vector3[] positions = new Vector3[transforms.Count];
            for (var i = 0; i < transforms.Count; i++)
                positions[i] = transforms[i].position;
            return positions;
        }

        /// <summary>
        /// Returns the .rotation attributes from a list of transforms.
        /// </summary>
        /// <param name='transforms'>
        /// Transforms.
        /// </param>
        public static Quaternion[] Rotations(this IList<Transform> transforms)
        {
            Quaternion[] rotations = new Quaternion[transforms.Count];
            for (var i = 0; i < transforms.Count; i++)
                rotations[i] = transforms[i].rotation;
            return rotations;
        }

        /// <summary>
        /// Returns a screen rect that is centered over this transform.
        /// </summary>
        /// <param name="tx">
        /// A <see cref="Transform"/>
        /// </param>
        /// <param name="width">
        /// A <see cref="System.Single"/> The width of the rectangle.
        /// </param>
        /// <param name="height">
        /// A <see cref="System.Single"/> The height of the rectangle.
        /// </param>
        /// <returns>
        /// A <see cref="Rect"/>
        /// </returns>
        public static Rect ScreenRect(this Transform tx, float width, float height)
        {
            var pos = Camera.main.WorldToScreenPoint(tx.position);
            pos.y = Screen.height - pos.y;
            var r = new Rect(pos.x - (width / 2), pos.y - (height / 2), width, height);
            return r;
        }

        /// <summary>
        /// Returns the closest transform from a list of transforms.
        /// </summary>
        /// <param name="tx">
        /// A <see cref="Transform"/>
        /// </param>
        /// <param name="list">
        /// A <see cref="IList(Transform)"/> The list of transforms to search through.
        /// </param>
        /// <returns>
        /// A <see cref="Transform"/>
        /// </returns>
        public static Transform FindClosest(this Transform tx, IList<Transform> list)
        {
            Transform t = null;
            var closest = -0f;
            foreach (var i in list)
            {
                var m = (i.position - tx.position).sqrMagnitude;
                if (t == null)
                {
                    closest = m;
                    t = i;
                }
                else
                {
                    if (m < closest)
                    {
                        t = i;
                        closest = m;
                    }
                }
            }
            return t;
        }

        /// <summary>
        /// Returnts the furthest transform from a list of transforms.
        /// </summary>
        /// <param name="tx">
        /// A <see cref="Transform"/>
        /// </param>
        /// <param name="list">
        /// A <see cref="IList(Transform)"/> The list of transforms to search through.
        /// </param>
        /// <returns>
        /// A <see cref="Transform"/>
        /// </returns>
        public static Transform FindFurthest(this Transform tx, IList<Transform> list)
        {
            Transform t = null;
            var furthest = -0f;
            foreach (var i in list)
            {
                var m = (i.position - tx.position).sqrMagnitude;
                if (t == null)
                {
                    furthest = m;
                    t = i;
                }
                else
                {
                    if (m > furthest)
                    {
                        t = i;
                        furthest = m;
                    }
                }
            }
            return t;
        }

        /// <summary>
        /// Distances to point.
        /// </summary>
        /// <returns>
        /// The to.
        /// </returns>
        /// <param name='tx'>
        /// Tx.
        /// </param>
        /// <param name='position'>
        /// Position.
        /// </param>
        public static float DistanceTo(this Transform tx, Vector3 position)
        {
            return (tx.position - position).magnitude;
        }

        /// <summary>
        /// Direction to point.
        /// </summary>
        /// <returns>
        /// The to.
        /// </returns>
        /// <param name='tx'>
        /// Tx.
        /// </param>
        /// <param name='position'>
        /// Position.
        /// </param>
        public static Vector3 DirectionTo(this Transform tx, Vector3 position)
        {
            return (tx.position - position).normalized;
        }

        /// <summary>
        /// Determines whether this instance is near the specified tx position + threshold.
        /// </summary>
        /// <returns>
        /// <c>true</c> if this instance is near the specified position + threshold; otherwise, <c>false</c>.
        /// </returns>
        /// <param name='tx'>
        /// If set to <c>true</c> tx.
        /// </param>
        /// <param name='position'>
        /// If set to <c>true</c> position.
        /// </param>
        /// <param name='threshold'>
        /// If set to <c>true</c> threshold.
        /// </param>
        public static bool IsNear(this Transform tx, Vector3 position, float threshold)
        {
            return (tx.position - position).sqrMagnitude <= (threshold * threshold);
        }

        /// <summary>
        /// Returns children of the transform.
        /// </summary>
        /// <param name='tx'>
        /// Tx.
        /// </param>
        public static Transform[] Children(this Transform tx)
        {
            var list = new List<Transform>();
            foreach (Transform t in tx)
            {
                list.Add(t);
            }
            return list.ToArray();
        }

        /// <summary>
        /// Find the first a parent component
        /// </summary>
        /*public static T findParentComponent<T>(this Transform tr) where T : Component
        {
            tr = tr.parent;
            if (tr == null)
                return null;

            T comp = tr.GetComponent<T>();
            if (comp == null)
                return tr.findParentComponent<T>();

            return comp;
        }*/


        /// <summary>
        /// Find the first a parent component
        /// </summary>
        public static void cleanChildren(this Transform tr)
        {
            while (tr.childCount != 0)
                GameObject.DestroyImmediate(tr.GetChild(0).gameObject);
        }

        public static void setIdentity(this Transform tr)
        {
            tr.localPosition = Vector3.zero;
            tr.localRotation = Quaternion.identity;
            tr.localScale = Vector3.one;
        }

        public static void activeAllChildren(this Transform tr, bool active)
        {
            for (int i = 0; i < tr.childCount; i++)
                tr.GetChild(i).gameObject.SetActive(active);
        }

        // search in components if one of them implements the interface set as parameter
        public static T GetInterface<T>(this Transform tr)
        {
            MonoBehaviour[] components = tr.GetComponents<MonoBehaviour>();

            foreach (MonoBehaviour component in components)
            {
                if (typeof(T).IsAssignableFrom(component.GetType()))
                    return (T)(object)component;
            }

            return default(T);
        }
    }

}