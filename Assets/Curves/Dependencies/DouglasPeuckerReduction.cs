﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DouglasPeuckerReduction
{

    /// <summary>
    /// Uses the Douglas Peucker algorithm to reduce the number of points. return just the list of points to keep
    /// </summary>
    /// <param name="Points">The points.</param>
    /// <param name="Tolerance">The tolerance.</param>
    /// <returns></returns>
    public static List<int>  GetPointsToKeep
        (List<Vector3> Points, float Tolerance)
    {
        List<int> pointIndexsToKeep = new List<int>();
        if (Points == null || Points.Count < 3)
            return pointIndexsToKeep;

        int firstPoint = 0;
        int lastPoint = Points.Count - 1;
        
        //Add the first and last index to the keepers
        pointIndexsToKeep.Add(firstPoint);
        pointIndexsToKeep.Add(lastPoint);

        //The first and the last point cannot be the same
        while (Points[firstPoint].Equals(Points[lastPoint]))
        {
            lastPoint--;
        }

        Reduction(Points, firstPoint, lastPoint,
        Tolerance, ref pointIndexsToKeep);
        pointIndexsToKeep.Sort();

        return pointIndexsToKeep;
    }



    /// <summary>
    /// Uses the Douglas Peucker algorithm to reduce the number of points.
    /// </summary>
    /// <param name="Points">The points.</param>
    /// <param name="Tolerance">The tolerance.</param>
    /// <returns></returns>
    public static List<Vector3> Apply
        (List<Vector3> Points, float Tolerance)
    {
        if (Points == null || Points.Count < 3)
            return Points;

        int firstPoint = 0;
        int lastPoint = Points.Count - 1;
        List<int> pointIndexsToKeep = new List<int>();

        //Add the first and last index to the keepers
        pointIndexsToKeep.Add(firstPoint);
        pointIndexsToKeep.Add(lastPoint);

        //The first and the last point cannot be the same
        while (Points[firstPoint].Equals(Points[lastPoint]))
        {
            lastPoint--;
        }

        Reduction(Points, firstPoint, lastPoint,
        Tolerance, ref pointIndexsToKeep);

        List<Vector3> returnPoints = new List<Vector3>();
        pointIndexsToKeep.Sort();
        foreach (int index in pointIndexsToKeep)
        {
            returnPoints.Add(Points[index]);
        }

        return returnPoints;
    }

    /// <summary>
    /// Douglases the peucker reduction.
    /// </summary>
    /// <param name="points">The points.</param>
    /// <param name="firstPoint">The first point.</param>
    /// <param name="lastPoint">The last point.</param>
    /// <param name="tolerance">The tolerance.</param>
    /// <param name="pointIndexsToKeep">The point index to keep.</param>
    private static void Reduction(List<Vector3>
        points, int firstPoint, int lastPoint, float tolerance,
        ref List<int> pointIndexsToKeep)
    {
        float maxDistance = 0;
        int indexFarthest = 0;

        for (int index = firstPoint; index < lastPoint; index++)
        {
            float distance = PerpendicularDistance
                (points[firstPoint], points[lastPoint], points[index]);
            if (distance > maxDistance)
            {
                maxDistance = distance;
                indexFarthest = index;
            }
        }

        if (maxDistance > tolerance && indexFarthest != 0)
        {
            //Add the largest point that exceeds the tolerance
            pointIndexsToKeep.Add(indexFarthest);
            Reduction(points, firstPoint,
            indexFarthest, tolerance, ref pointIndexsToKeep);
            Reduction(points, indexFarthest,
            lastPoint, tolerance, ref pointIndexsToKeep);
        }
    }

    /// <summary>
    /// The distance of a point from a line made from point1 and point2.
    /// </summary>
    /// <param name="pt1">The PT1.</param>
    /// <param name="pt2">The PT2.</param>
    /// <param name="p">The p.</param>
    /// <returns></returns>
    public static float PerpendicularDistance
        (Vector3 p1, Vector3 p2, Vector3 p)
    {
        Vector3 dir = (p2 - p1).normalized;
        return Vector3.Cross(dir, p - p1).magnitude;



    }
}
