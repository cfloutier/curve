using UnityEditor;
using UnityEngine;
using System.Collections;
using System;
using System.Reflection;
using Curves.Dependencies;

namespace Curves.UI
{
    [CustomEditor(typeof(Curve))]
    /// <summary>
    /// Bezier curve line editor directly usable into the scene view 
    /// 
    /// There is too part where the curve is edited
    /// - In the scene view, where points can be added, moved deleted and so on
    /// - in the inspector, 
    /// </summary>
    public class CurveEditor : Editor
    {
        #region Editor Variables

        int curPointIndex_ = 0;
        int curPointIndex
        {
            get { return curPointIndex_; }
            set { curPointIndex_ = value; }
        }

        enum EditionMode
        {
            Curve,
            Interpolation,
            EditPoints
        }

        public enum PointsMode
        {
            Position,
            Rotation,
            Size
        }

        static EditionMode editionMode = EditionMode.Curve;
        static PointsMode pointsMode = PointsMode.Position;

        int layer;
        Curve curve;
        #endregion

        #region main tools

        void OnEnable()
        {
            curve = (Curve)target;

            if (curve == null) return;

            layer = curve.gameObject.layer;
            curve.gameObject.layer = 2; // no raycast

            drawSceneViewBar = EditorPrefs.GetBool("drawSceneViewBar", drawSceneViewBar);

            Default_Handler_Hidden = editionMode == EditionMode.EditPoints && drawSceneViewBar;


            curPointIndex_ = EditorPrefs.GetInt("curPointIndex", curPointIndex_);
            editionMode = (EditionMode)EditorPrefs.GetInt("editionMode", (int)editionMode);
            pointsMode = (PointsMode)EditorPrefs.GetInt("pointsMode", (int)pointsMode);
        }

        void OnDisable()
        {
            if (editionMode == EditionMode.EditPoints && drawSceneViewBar)
            {
                Selection.activeTransform = curve.transform;
            }
            else
            {
                Default_Handler_Hidden = false;
                if (curve != null)
                    curve.gameObject.layer = layer;
            }

            EditorPrefs.SetBool("drawSceneViewBar", drawSceneViewBar);
            EditorPrefs.SetInt("editionMode", (int)editionMode);
            EditorPrefs.SetInt("pointsMode", (int)pointsMode);

            EditorPrefs.SetInt("curPointIndex", curPointIndex_);
        }

        void registerUndo()
        {
            Undo.RecordObject(curve, "Curve change");
        }

        static bool Default_Handler_Hidden
        {
            get
            {
                Type type = typeof(UnityEditor.Tools);
                FieldInfo field = type.GetField("s_Hidden", BindingFlags.NonPublic | BindingFlags.Static);
                return ((bool)field.GetValue(null));
            }
            set
            {
                Type type = typeof(UnityEditor.Tools);
                FieldInfo field = type.GetField("s_Hidden", BindingFlags.NonPublic | BindingFlags.Static);
                field.SetValue(null, value);
            }
        }
        #endregion

        #region Gui windows

        GUISkin skin__ = null;
        GUISkin skin
        {
            get
            {
                if (skin__ == null)
                {
                    string[] pathes = AssetDatabase.FindAssets("CurvesSkin");

                    if (pathes.Length == 0)
                        Debug.LogError("Skin not found");
                    else
                        skin__ = (GUISkin)(AssetDatabase.LoadAssetAtPath(AssetDatabase.GUIDToAssetPath(pathes[0]), typeof(GUISkin)));
                }

                return skin__ = GUI.skin;
            }
        }

        void mainGUI()
        {
            GUILayout.BeginVertical();


            EditionMode newMode = (EditionMode)GUILayout.SelectionGrid((int)editionMode, new string[] {
                "Curve", "Point Sampling", "Edit Points" }, 3, MyGuiStyle.LargeButton);

            if (newMode != editionMode)
            {
                editionMode = newMode;
                SceneView.RepaintAll();
            }

            switch (editionMode)
            {
                case EditionMode.Curve:
                    curveGUI();
                    break;
                case EditionMode.Interpolation:
                    interpolationGUI();
                    break;
                case EditionMode.EditPoints:
                    editPointsGUI();
                    break;
            }

            GUILayout.EndVertical();
        }

        FloatField raycatHeightField = new FloatField("Vertical Raycast Height : ", FloatField.SliderMode.Relative, new Vector2(-float.MaxValue, float.MaxValue));


        void curveGUI()
        {
            CurveControlPoint[] pts = curve.controlPoints;
            if (curve.sharePoints != null)
                pts = curve.sharePoints.controlPoints;

            if (curve.sharePoints)
            {
                GUILayout.Label("Curve Points are from " + curve.sharePoints.name);
                GUILayout.Label("Can not Edit Parameters ");
            }
            else
            {
                Curve.CurveType curve_type = curve.curve_type__;
                GUILayout.Label("Curve Type", MyGuiStyle.Title);

                if (curve_type == Curve.CurveType.Spline)
                {
                    Curve.SplineType splineType = curve.Spline_Type;
                    GUILayout.Label("Splines are defined by crossing points only");

                    switch (splineType)
                    {
                        case Curve.SplineType.Lines:
                            GUILayout.Label("Straight lines");
                            break;
                        case Curve.SplineType.Cubic:
                            GUILayout.Label("Cubic is the less Precise");
                            break;
                        case Curve.SplineType.CatmullRom:
                            GUILayout.Label("The more standard");
                            break;
                        case Curve.SplineType.Centripetal:
                            GUILayout.Label("A catmull Rom with a tension control parameter,\nmore complex to compute but avoids unwanted loops");
                            break;
                        case Curve.SplineType.Hermite:
                            GUILayout.Label("A different set of parameter for catmull Rom.\nFrom Linear to smooth curve");
                            break;
                    }
                }
                else
                {
                    GUILayout.Label("Bezier are defined by crossing and control points");
                    Curve.BezierType bezierType = curve.Bezier_Type;
                    switch (bezierType)
                    {
                        case Curve.BezierType.Cubic:
                            GUILayout.Label("Cubic use 2 control points per segment");
                            break;
                        case Curve.BezierType.Quad:
                            GUILayout.Label("Quad use 1 control point per segment");
                            break;

                    }
                }


                GUILayout.BeginHorizontal();

                curve_type = (Curve.CurveType)GUILayout.SelectionGrid((int)curve_type, new string[] { "Spline", "Bezier" }, 2, MyGuiStyle.LargeButton);

                if (curve_type != curve.curve_type__)
                {
                    registerUndo();
                    curve.curve_type__ = curve_type;
                    setChanged();
                }

                GUILayout.Space(20);

                if (curve_type == Curve.CurveType.Spline)
                {
                    Curve.SplineType splineType = curve.splineType__;

                    string[] textes = Enum.GetNames(typeof(Curve.SplineType));

                    splineType = (Curve.SplineType)GUILayout.SelectionGrid((int)splineType, textes, 3, MyGuiStyle.LargeButton);


                    if (splineType != curve.splineType__)
                    {
                        registerUndo();
                        curve.splineType__ = splineType;
                        setChanged();
                    }

                    if (splineType == Curve.SplineType.Centripetal)
                    {

                        GUILayout.EndHorizontal();
                        GUILayout.BeginHorizontal();
                        float CurveTension = curve.CurveTension__;
                        GUILayout.Label("Alpha " + CurveTension.ToString("0.00"), GUILayout.Width(150));


                        CurveTension = GUILayout.HorizontalSlider(CurveTension, 0, 1);

                        if (CurveTension != curve.CurveTension__)
                        {
                            registerUndo();
                            curve.CurveTension__ = CurveTension;
                            setChanged();
                        }

                        if (GUILayout.Button("0.5", GUILayout.Width(70)))
                        {
                            registerUndo();
                            curve.CurveTension__ = 0.5f;
                            setChanged();
                        }

                        GUILayout.EndHorizontal();
                        GUILayout.BeginHorizontal();

                        if (curve.CurveTension__ == 0)
                        {
                            GUILayout.Label("Linear : Please use Catmall Rom for quicker result");
                        }
                        else if (curve.CurveTension__ < 0.5f)
                        {
                            GUILayout.Label("Linear - Centripetal");
                        }
                        else if (curve.CurveTension__ == 0.5f)
                        {
                            GUILayout.Label("Centripetal");
                        }
                        else if (curve.CurveTension__ < 1)
                        {
                            GUILayout.Label("Centripetal - Chordal");
                        }
                        else
                        {
                            GUILayout.Label("Chordal");
                        }

                    }
                    else if (splineType == Curve.SplineType.Hermite)
                    {
                        GUILayout.EndHorizontal();
                        GUILayout.BeginHorizontal();
                        float CurveTension = curve.CurveTension__;
                        GUILayout.Label("Tension " + CurveTension.ToString("0.00"), GUILayout.Width(150));

                        CurveTension = GUILayout.HorizontalSlider(CurveTension, -1, 1);

                        if (CurveTension != curve.CurveTension__)
                        {
                            registerUndo();
                            curve.CurveTension__ = CurveTension;
                            setChanged();
                        }

                        GUILayout.EndHorizontal();
                        GUILayout.BeginHorizontal();

                        float CurveBias = curve.CurveBias__;
                        GUILayout.Label("Bias " + CurveBias.ToString("0.00"), GUILayout.Width(150));

                        CurveBias = GUILayout.HorizontalSlider(CurveBias, -2, 2);

                        if (CurveBias != curve.CurveBias__)
                        {
                            registerUndo();
                            curve.CurveBias__ = CurveBias;
                            setChanged();
                        }
                    }

                    GUILayout.EndHorizontal();

                    GUILayout.Label("Sources Points Simplifications", MyGuiStyle.Title);
                    GUILayout.Label("Simplify the sources Points using\na Douglas-Peucker reduction algorithm");
                    GUILayout.BeginHorizontal();

                    bool sp = GUILayout.Toggle(curve.SimplifySourceByDistance__, "Simplify Control Points");
                    if (sp != curve.SimplifySourceByDistance__)
                    {
                        registerUndo();
                        curve.SimplifySourceByDistance__ = sp;
                        setChanged();
                    }

                    if (sp)
                    {
                        float SimplifyMinDistance = EditorGUILayout.FloatField("max distance", curve.SimplifyMinDistance__);

                        if (SimplifyMinDistance != curve.SimplifyMinDistance__)
                        {
                            registerUndo();
                            curve.SimplifyMinDistance__ = SimplifyMinDistance;
                            setChanged();
                        }
                    }
                }
                else if (curve_type == Curve.CurveType.Bezier)
                {
                    Curve.BezierType bezierType = curve.bezierType__;

                    bezierType = (Curve.BezierType)GUILayout.SelectionGrid((int)bezierType, new string[] { "Quad", "Cubic" }, 2, MyGuiStyle.LargeButton);
                    if (bezierType != curve.bezierType__)
                    {
                        registerUndo();
                        curve.bezierType__ = bezierType;
                        setChanged();
                    }
                }

                GUILayout.EndHorizontal();

                GUILayout.Label("Global Options", MyGuiStyle.Title);
                GUILayout.BeginHorizontal();

                bool loop = GUILayout.Toggle(curve.loop__, "Loop");
                if (loop != curve.loop__)
                {
                    registerUndo();
                    curve.loop__ = loop;
                    setChanged();
                }

                bool VerticalRaycast = GUILayout.Toggle(curve.VerticalRaycast__, "Vertical RayCast");
                if (VerticalRaycast != curve.VerticalRaycast__)
                {
                    registerUndo();
                    if (!VerticalRaycast)
                    {
                        curve.VerticalRaycastLine__ = false;
                    }

                    curve.VerticalRaycast__ = VerticalRaycast;
                    if (VerticalRaycast)
                        rayCastAllControls();

                    setChanged();
                }

                GUILayout.EndHorizontal();

                if (VerticalRaycast)
                {
                    GUILayout.Label("Raycast Tools", MyGuiStyle.Title);


                    bool rayCastLine = GUILayout.Toggle(curve.VerticalRaycastLine__, "Whole Line");
                    if (rayCastLine != curve.VerticalRaycastLine__)
                    {
                        curve.VerticalRaycastLine__ = rayCastLine;
                        setChanged();
                    }

                    float raycastHeight = curve.raycastHeight__;
                    if (raycatHeightField.LayoutField(ref raycastHeight))
                    {
                        registerUndo();
                        curve.raycastHeight__ = raycastHeight;
                        setChanged();
                        rayCastAllControls();
                    }
                }

                if (pts.Length > 0)
                {
                    GUILayout.Label("Pivot Tools", MyGuiStyle.Title);

                    GUILayout.BeginHorizontal();
                    if (GUILayout.Button("Reset Scale-Rotation"))
                    {
                        resetPivotScaleRot();
                    }

                    if (GUILayout.Button("Center"))
                    {
                        centerPivot();
                    }

                    GUILayout.EndHorizontal();
                }
            }

            GUILayout.FlexibleSpace();
            // Debug Gizmo
            bool drawGizmos = GUILayout.Toggle(curve.drawGizmos, "Additional Gizmos");
            if (drawGizmos != curve.drawGizmos)
            {
                registerUndo();

                curve.drawGizmos = drawGizmos;
                setChanged();
            }

            if (curve.drawGizmos)
            {
                GUILayout.Label("Debug Gizmos", MyGuiStyle.Title);

                GUILayout.BeginHorizontal();

                bool drawGizmosPolyline = GUILayout.Toggle(curve.drawGizmosPolyline, "Polyline");
                if (drawGizmosPolyline != curve.drawGizmosPolyline)
                {
                    registerUndo();

                    curve.drawGizmosPolyline = drawGizmosPolyline;
                    setChanged();
                }

                bool drawPoints = GUILayout.Toggle(curve.drawGizmosControlPoints, "Control Points");
                if (drawPoints != curve.drawGizmosControlPoints)
                {
                    registerUndo();

                    curve.drawGizmosControlPoints = drawPoints;
                    setChanged();
                }

                bool drawGizmosOrientationGizmos = GUILayout.Toggle(curve.drawGizmosOrientation, "Orientation");
                if (drawGizmosOrientationGizmos != curve.drawGizmosOrientation)
                {
                    registerUndo();

                    curve.drawGizmosOrientation = drawGizmosOrientationGizmos;
                    setChanged();
                }

                bool drawGizmosSizes = GUILayout.Toggle(curve.drawGizmosSizes, "Sizes");
                if (drawGizmosSizes != curve.drawGizmosSizes)
                {
                    registerUndo();

                    curve.drawGizmosSizes = drawGizmosSizes;
                    setChanged();
                }


                bool drawRatios = GUILayout.Toggle(curve.drawRatios, "Ratios");
                if (drawRatios != curve.drawRatios)
                {
                    registerUndo();

                    curve.drawRatios = drawRatios;
                    setChanged();
                }

                GUILayout.EndHorizontal();
            }
        }

        bool drawSceneViewBar = false;

        void editPointsGUI()
        {
            if (curve.sharePoints != null)
            {
                GUILayout.Label("Curve Points are from " + curve.sharePoints.name);
                GUILayout.Label("Can not Edit Parameters ");
                return;
            }

            if (curve.SimplifySourceByDistance__)
            {
                drawSceneViewBar = false;
                GUILayout.Label("Curve Simplification is ON\nPoints can not be edited. Uncheck it to edit nodes");

                if (!GUILayout.Toggle(curve.SimplifySourceByDistance__, "Simplify Control Points"))
                {
                    registerUndo();
                    curve.SimplifySourceByDistance__ = false;
                    setChanged();
                }

                return;
            }
            if (curve.samplingMode == Curve.SamplingMode.ConstantDistance)
            {
                drawSceneViewBar = false;
                GUILayout.Label("Sampling Mode is Constant Distance\nPoints can not be edited. Uncheck it to edit nodes");
                bool constantDistance = true;
                if (!GUILayout.Toggle(constantDistance, "Constant Distance Mode"))
                {
                    registerUndo();
                    curve.samplingMode = Curve.SamplingMode.PointsDivision;
                    setChanged();
                }

                return;
            }

            GUILayout.BeginHorizontal();

            bool value = GUILayout.Toggle(drawSceneViewBar, "Scene View Tools");
            if (value != drawSceneViewBar)
            {
                drawSceneViewBar = value;
                SceneView.RepaintAll();
            }
            if (drawSceneViewBar)
                GUILayout.Label("Warning : in this mode you can't select\nany other object than nodes in the scene view.");

            GUILayout.EndHorizontal();
            drawControlBar();

            switch (pointsMode)
            {
                case PointsMode.Position:
                    pointsPositionsGUI();
                    break;
                case PointsMode.Rotation:
                    orientationPointsGUI();
                    break;
                case PointsMode.Size:
                    sizePointsGUI();
                    break;
            }
        }

        void selectPrev()
        {
            CurveControlPoint[] pts = getPoints();

            curPointIndex--;
            if (curPointIndex < 0)
                curPointIndex = pts.Length - 1;

            zoomOn(pts[curPointIndex].main);
        }

        void selectNext()
        {
            CurveControlPoint[] pts = getPoints();

            curPointIndex++;
            if (curPointIndex >= pts.Length)
                curPointIndex = 0;

            zoomOn(pts[curPointIndex].main);
        }

        void centerCurrent()
        {
            if (curPointIndex < 0 || curPointIndex >= curve.controlPoints.Length)
            {
                zoomOn(Vector3.zero);
            }
            else
            {
                zoomOn(curve.controlPoints[curPointIndex].main);
            }
        }

        void zoomOn(Vector3 pos)
        {
            pos = curve.transform.TransformPoint(pos);
            ArrayList views = UnityEditor.SceneView.sceneViews;
            foreach (SceneView view in views)
            {
                //	Debug.Log(view.camera.transform.rotation);
                view.LookAt(pos, view.camera.transform.rotation);
            }
        }

        void PointModeGUI()
        {
            if (!isPointSelected()) return;

            CurveControlPoint[] pts = getPoints();
            if (curPointIndex != 0 || curve.loop__)
            {
                if (curve.curve_type__ == Curve.CurveType.Bezier)
                {
                    if (curve.bezierType__ == Curve.BezierType.Quad)
                    {
                        CurveControlPoint.mode mode = pts[curPointIndex].pointMode;
                        mode = (CurveControlPoint.mode)GUILayout.SelectionGrid((int)mode,
                            new GUIContent[] {
                            new GUIContent(ButtonsIcons.line2),
                            new GUIContent(ButtonsIcons.quad)
                            }, 2, MyGuiStyle.BigSqButton);

                        if (mode != pts[curPointIndex].pointMode)
                        {
                            registerUndo();
                            pts[curPointIndex].pointMode = mode;
                            setChanged();

                            if (mode == CurveControlPoint.mode.Symetric)
                            {
                                pts[curPointIndex].control2 = 2 * pts[curPointIndex].main - pts[curPointIndex].control1;
                            }
                        }
                    }
                    else if (curve.bezierType__ == Curve.BezierType.Cubic)
                    {
                        CurveControlPoint.mode mode = pts[curPointIndex].pointMode;
                        mode = (CurveControlPoint.mode)GUILayout.SelectionGrid((int)mode,
                            new GUIContent[] {
                            new GUIContent(ButtonsIcons.line),
                            new GUIContent(ButtonsIcons.symetric),
                            new GUIContent( ButtonsIcons.broken)

                            }, 3, MyGuiStyle.BigSqButton);

                        if (mode != pts[curPointIndex].pointMode)
                        {
                            registerUndo();
                            pts[curPointIndex].pointMode = mode;
                            setChanged();

                            if (mode == CurveControlPoint.mode.Symetric)
                            {
                                pts[curPointIndex].control2 = 2 * pts[curPointIndex].main - pts[curPointIndex].control1;
                            }
                        }
                    }
                }
            }
        }

        void pointsPositionsGUI()
        {
            CurveControlPoint[] pts = getPoints();

            if (curve.sharePoints != null)
                return;

            if (isPointSelected())
            {
                GUILayout.Label("Current Point # " + curPointIndex + " (" + (curPointIndex + 1) + " / " + pts.Length + ")");

                PointModeGUI();

                CurveControlPoint point = pts[curPointIndex];

                Vector3 vec = EditorGUILayout.Vector3Field("Main", point.main);
                if (vec != point.main)
                {
                    registerUndo();
                    point.main = vec;
                    setChanged();
                }

                if (curve.curve_type__ == Curve.CurveType.Bezier)
                {
                    if (point.pointMode != CurveControlPoint.mode.Line)
                    {
                        vec = EditorGUILayout.Vector3Field("Control 1", point.control1);
                        if (vec != point.main)
                        {
                            registerUndo();
                            point.control1 = vec;
                            setChanged();
                        }

                        if (curve.bezierType__ == Curve.BezierType.Cubic)
                        {
                            vec = EditorGUILayout.Vector3Field("Control 2", point.control2);
                            if (vec != point.main)
                            {
                                registerUndo();
                                point.control2 = vec;
                                setChanged();
                            }
                        }

                    }
                }
            }
            else
            {
                if (pts.Length == 0)
                {
                    GUILayout.Label("Please insert a point by pressing the a + icon");
                }

                GUILayout.Label("No selection");
            }

            if (pts.Length != 0)
            {
                GUILayout.Label("All Points Operation", MyGuiStyle.Title);
                GUILayout.BeginHorizontal();
                if (GUILayout.Button(new GUIContent("Remove Z"), GUILayout.Height(20)))
                {
                    ResetAllZ();
                }

                if (GUILayout.Button(new GUIContent("Clear all"), GUILayout.Height(20)))
                {
                    ClearPts();
                }

                GUILayout.EndHorizontal();
                GUILayout.BeginHorizontal();
                if (GUILayout.Button(new GUIContent("Recenter Pivot"), GUILayout.Height(20)))
                {
                    curve.RecenterPivot();
                }

                if (GUILayout.Button(new GUIContent("reverse Order"), GUILayout.Height(20)))
                {
                    curve.ReverseOrder();
                }

                GUILayout.EndHorizontal();
            }
        }

        FloatField orientationField = new FloatField("Orientation : ", new Vector2(-180, 180), 10);

        void orientationPointsGUI()
        {
            CurveControlPoint[] pts = getPoints();

            if (isPointSelected())
            {
                GUILayout.Label("Current Point : " + (curPointIndex + 1) + " / " + pts.Length);

                controlDirectionGUI();

                float orientation = pts[curPointIndex].CurveUpOrientation;
                if (orientationField.LayoutField(ref orientation))
                {
                    registerUndo();
                    pts[curPointIndex].CurveUpOrientation = orientation;
                    setChanged();
                }
            }
            else
            {
                if (pts.Length == 0)
                {
                    GUILayout.Label("Use Control Points tab to insert points");
                }
                else
                {
                    GUILayout.Label("Clic twice on circles to select a control point");
                }
            }

            GUILayout.Label("All Points Operations", MyGuiStyle.Title);
            GUILayout.BeginHorizontal();
            if (GUILayout.Button(ButtonsIcons.less, MyGuiStyle.SmallSqButton))
            {
                changeAllOrientation(-5);
            }

            if (GUILayout.Button("Reset", MyGuiStyle.LargeButton, GUILayout.Width(150)))
            {
                resetAllOrientations();
            }

            if (GUILayout.Button(ButtonsIcons.add, MyGuiStyle.SmallSqButton))
            {
                changeAllOrientation(5);
            }

            GUILayout.EndHorizontal();

            GUILayout.Label("Interpolation Mode", MyGuiStyle.Title);

            Curve.OrientationInterpolationMethod orientationInterpolationMethod = curve.orientationInterpolationMethod;
            orientationInterpolationMethod = (Curve.OrientationInterpolationMethod)GUILayout.SelectionGrid((int)orientationInterpolationMethod,
                new string[] { "Linear", "Cosinus" }, 2, MyGuiStyle.LargeButton);

            if (orientationInterpolationMethod != curve.orientationInterpolationMethod)
            {
                registerUndo();
                curve.orientationInterpolationMethod = orientationInterpolationMethod;
                setChanged();
            }


        }

        FloatField sizeField = new FloatField("Size : ", FloatField.SliderMode.Relative, new Vector2(0, float.MaxValue));
        FloatField AllSizeField = new FloatField("Size : ", FloatField.SliderMode.Relative, new Vector2(0, float.MaxValue));

        float allsizes = 1;

        void sizePointsGUI()
        {
            //  GUILayout.Label("size  !!!");

            CurveControlPoint[] pts = getPoints();
            if (isPointSelected())
            {

                float size = pts[curPointIndex].Size;
                if (sizeField.LayoutField(ref size))
                {
                    registerUndo();
                    pts[curPointIndex].Size = size;
                    setChanged();
                }
            }


            if (pts.Length != 0)
            {
                GUILayout.Label("All Points Operation", MyGuiStyle.Title);

                if (AllSizeField.LayoutField(ref allsizes))
                {
                    registerUndo();
                    for (int i = 0; i < pts.Length; i++)
                    {
                        pts[i].Size = allsizes;
                    }

                    setChanged();
                }
            }

            GUILayout.Label("Interpolation Mode", MyGuiStyle.Title);

            Curve.SizeInterpolationMethod widthMethod = curve.widthInterpolationMethod;
            widthMethod = (Curve.SizeInterpolationMethod)GUILayout.SelectionGrid((int)widthMethod,
                new string[] { "Linear", "Cosinus", "Cubic" ,
                 "Hermite"}, 4, MyGuiStyle.LargeButton);

            if (widthMethod != curve.widthInterpolationMethod)
            {
                registerUndo();
                curve.widthInterpolationMethod = widthMethod;
                setChanged();
            }

            if (widthMethod == Curve.SizeInterpolationMethod.Hermite)
            {
                GUILayout.BeginHorizontal();

                float tension = curve.tensionWidth;
                GUILayout.Label("Tension " + tension.ToString("0.00"), GUILayout.Width(120));

                tension = GUILayout.HorizontalSlider(curve.tensionWidth, -1f, 1f);
                if (curve.tensionWidth != tension)
                {
                    registerUndo();
                    curve.tensionWidth = tension;
                    setChanged();
                }

                if (GUILayout.Button("0"))
                {
                    registerUndo();
                    curve.tensionWidth = 0;
                    setChanged();
                }

                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();

                float bias = curve.biasWidth;

                GUILayout.Label("Bias " + bias.ToString("0.00"), GUILayout.Width(120));

                bias = GUILayout.HorizontalSlider(curve.biasWidth, -1f, 1f);
                if (curve.biasWidth != bias)
                {
                    registerUndo();
                    curve.biasWidth = bias;
                    setChanged();
                }


                if (GUILayout.Button("0"))
                {
                    registerUndo();
                    curve.biasWidth = 0;
                    setChanged();
                }

                GUILayout.EndHorizontal();
            }
        }

        FloatField stepField = new FloatField("Segment lenght : ", FloatField.SliderMode.Relative, new Vector2(0, float.MaxValue));
        FloatField firstPointPosField = new FloatField("First point Pos : ", FloatField.SliderMode.Relative, new Vector2(0, float.MaxValue));
        IntField NbPointsFields = new IntField("Nb Points : ", IntField.SliderMode.Relative, 1, 1000);

        void interpolationGUI()
        {
            //CurveControlPoint[] pts = getPoints(curve);

            GUILayout.Label("Points Sampling Method", MyGuiStyle.Title);

            Curve.SamplingMode mode = curve.samplingMode;
            mode = (Curve.SamplingMode)GUILayout.SelectionGrid((int)mode, new string[] { "Simple Division", "Const distance" }, 2, MyGuiStyle.LargeButton);
            if (mode != curve.samplingMode)
            {
                registerUndo();
                curve.samplingMode = mode;
                setChanged();
            }

            if (curve.samplingMode == Curve.SamplingMode.PointsDivision)
            {
                GUILayout.Label("Each segment is divided with the same number of points");
                GUILayout.BeginHorizontal();

                int nbPtsPerCurve = curve.nbPtsPerCurve;

                if (NbPointsFields.LayoutField(ref nbPtsPerCurve))
                {

                    //     Debug.Log("Nb pts " + nbPtsPerCurve);
                    registerUndo();
                    curve.nbPtsPerCurve = nbPtsPerCurve;
                    setChanged();
                }
                GUILayout.EndHorizontal();

                GUILayout.Label("Simplification of the Result line", MyGuiStyle.Title);

                bool sp = GUILayout.Toggle(curve.SimplifyResultByDistance, "Simplify By Distance ");
                if (sp != curve.SimplifyResultByDistance)
                {
                    registerUndo();
                    curve.SimplifyResultByDistance = sp;
                    setChanged();
                }

                if (sp)
                {
                    float SimplifyResultMinDist = EditorGUILayout.FloatField("Min distance", curve.SimplifyResultMinDist);

                    if (SimplifyResultMinDist != curve.SimplifyResultMinDist)
                    {
                        registerUndo();
                        curve.SimplifyResultMinDist = SimplifyResultMinDist;
                        setChanged();
                    }
                }

                sp = GUILayout.Toggle(curve.SimplifyResultByBend, "Simplify By Bend");
                if (sp != curve.SimplifyResultByBend)
                {
                    registerUndo();
                    curve.SimplifyResultByBend = sp;
                    setChanged();
                }

                if (sp)
                {
                    float SimplifyResultMinBend = EditorGUILayout.FloatField("Min Bend Angle", curve.SimplifyResultMinBend);

                    if (SimplifyResultMinBend != curve.SimplifyResultMinBend)
                    {
                        registerUndo();
                        curve.SimplifyResultMinBend = SimplifyResultMinBend;
                        setChanged();
                    }
                }

            }
            else if (curve.samplingMode == Curve.SamplingMode.ConstantDistance)
            {
                GUILayout.Label(@"More complex. Each segment is first divided with
the same number of points then a sub list of 
point is selected to fit the segment length.");
                GUILayout.Label("Precision = " + curve.precision + " (curve part subdivision factor)");

                int precision = (int)GUILayout.HorizontalSlider(curve.precision, 1, 100);
                if (precision != curve.precision)
                {
                    registerUndo();
                    curve.precision = precision;
                    setChanged();
                }

                float stepDistance = curve.constantSegmentLenght;
                if (stepField.LayoutField(ref stepDistance))
                {
                    registerUndo();
                    curve.constantSegmentLenght = stepDistance;
                    setChanged();
                }

                float firstPointPos = curve.firstPointPos;
                if (firstPointPosField.LayoutField(ref firstPointPos))
                {
                    registerUndo();
                    curve.firstPointPos = firstPointPos;
                    setChanged();
                }

                GUILayout.Label("Last Segment Error = " + (curve.lastSegmentErrorFactor * 100) + " %");
                GUILayout.Label("Mean Error = " + (curve.globalErrorFactor * 100) + " %");
            }

        }

        CurveControlPoint[] getPoints()
        {
            CurveControlPoint[] pts = curve.controlPoints;
            if (curve.sharePoints != null)
                pts = curve.sharePoints.controlPoints;

            return pts;
        }

        #endregion

        #region Tools Functions

        void ResetAllZ()
        {
            registerUndo();
            CurveControlPoint[] pts = getPoints();
            for (int i = 0; i < pts.Length; i++)
            {
                pts[i].main.y = 0;
                pts[i].control1.y = 0;
                pts[i].control2.y = 0;
            }

            setChanged();
        }

        void resetPivotScaleRot()
        {
            registerUndo();
            Undo.RecordObject(curve.transform, "curve orientation & scale");

            curve.resetPivotScaleRot();

            setChanged();
        }

        void resetAllOrientations()
        {
            registerUndo();
            CurveControlPoint[] pts = getPoints();
            if (pts.Length == 0) return;

            for (int i = 0; i < pts.Length; i++)
            {
                pts[i].CurveUpOrientation = 0;
            }

            setChanged();
        }



        void changeAllOrientation(float delta)
        {
            registerUndo();
            CurveControlPoint[] pts = getPoints();
            if (pts.Length == 0) return;

            for (int i = 0; i < pts.Length; i++)
            {
                pts[i].CurveUpOrientation += delta;
            }

            setChanged();
        }

        void resetAllSizes()
        {
            registerUndo();
            CurveControlPoint[] pts = getPoints();
            if (pts.Length == 0) return;

            for (int i = 0; i < pts.Length; i++)
            {
                pts[i].Size = 1;
            }

            setChanged();

        }

        // return delta pos
        // vect in world coord
        Vector3 verticalRaycastPoint(Vector3 vect, float height)
        {
            Vector3 startPos = vect + Vector3.up * 10000;
            Ray ray = new Ray(startPos, -Vector3.up);

            RaycastHit hit = new RaycastHit();
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                return hit.point + Vector3.up * height - vect;
            }
            return Vector3.zero;
        }

        Vector3 cameraRaycastPoint(Vector3 vect, float height)
        {
            Transform cameraTransform = Camera.current.transform;
            Vector3 fwd = (vect - cameraTransform.position - Vector3.up * height).normalized;

            Vector3 startPos = cameraTransform.position;
            Ray ray = new Ray(startPos, fwd);

            RaycastHit hit = new RaycastHit();
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                return hit.point + Vector3.up * height - vect;
            }
            return Vector3.zero;
        }

        void rayCastAllControls()
        {
            registerUndo();
            CurveControlPoint[] pts = getPoints();
            if (pts.Length == 0) return;

            for (int i = 0; i < pts.Length; i++)
            {
                Vector3 deltaPos = verticalRaycastPoint(curve.transform.TransformPoint(pts[i].main), curve.Raycast_Height);
                pts[i].main += deltaPos;
                pts[i].control1 += deltaPos;
                pts[i].control2 += deltaPos;
            }

            setChanged();
        }

        void centerPivot()
        {
            registerUndo();
            Undo.RecordObject(curve.transform, "curve position");


            CurveControlPoint[] pts = getPoints();
            if (pts.Length == 0) return;


            Vector3[] polyline = curve.WorldPolyLine;

            Vector3 center = Vector3.zero;
            if (polyline.Length == 0) return;

            // reset all points position according to localScale
            for (int i = 0; i < polyline.Length; i++)
            {
                center += polyline[i];
            }

            center = center / polyline.Length;

            // Debug.Log("Center " + center);

            Vector3 delta = center - curve.transform.position;
            for (int i = 0; i < pts.Length; i++)
            {
                pts[i].main -= delta;
                pts[i].control1 -= delta;
                pts[i].control2 -= delta;
            }

            curve.transform.position = center;

            setChanged();
        }

        void ClearPts()
        {
            registerUndo();
            if (curve.sharePoints != null)
            {
                curve.sharePoints.controlPoints = new CurveControlPoint[0];
            }
            else
                curve.controlPoints = new CurveControlPoint[0];


            curPointIndex = -1;
            setChanged();
        }

        #endregion

        #region Points Edition
        bool isPointSelected()
        {
            CurveControlPoint[] pts = getPoints();
            return curPointIndex >= 0 && curPointIndex < pts.Length;
        }

        void DeleteCurrent()
        {
            if (!isPointSelected())
                return;

            Delete(curPointIndex);
        }

        void Delete(int index)
        {
            registerUndo();
            CurveControlPoint[] pts = getPoints();

            ArrayList list = new ArrayList(pts);
            list.RemoveAt(index);

            if (curve.sharePoints != null)
            {
                curve.sharePoints.controlPoints = (CurveControlPoint[])list.ToArray(typeof(CurveControlPoint));
            }
            else
                curve.controlPoints = (CurveControlPoint[])list.ToArray(typeof(CurveControlPoint));

            pts = (CurveControlPoint[])list.ToArray(typeof(CurveControlPoint));
            setChanged();
        }

        void InsertAfter()
        {
            if (!isPointSelected())
                insert(-1);
            else
                insert(curPointIndex + 1);
        }

        void InsertBefore()
        {
            if (!isPointSelected())
                insert(0);
            else
                insert(curPointIndex);
        }

        void insert(int position)
        {
            registerUndo();
            CurveControlPoint[] pts = curve.controlPoints;
            if (curve.sharePoints != null)
                pts = curve.sharePoints.controlPoints;

            ArrayList list = new ArrayList(pts);

            CurveControlPoint newPt = new CurveControlPoint();

            if (pts.Length == 0)
            {
                newPt.main = Vector3.zero;
                newPt.pointMode = CurveControlPoint.mode.Line;

                newPt.control1 = Vector3.right;
                newPt.control2 = Vector3.left;

                list.Add(newPt);
            }
            else if (pts.Length == 1)
            {
                newPt.CurveUpOrientation = pts[0].CurveUpOrientation;
                newPt.Size = pts[0].Size;

                if (position == 0)
                {
                    newPt.main = pts[0].main / 2;
                    newPt.control1 = -pts[0].main / 2;
                    newPt.control2 = -newPt.control1;
                    newPt.pointMode = CurveControlPoint.mode.Line;

                    list.Insert(0, newPt);
                }
                else
                {
                    newPt.pointMode = CurveControlPoint.mode.Symetric;

                    newPt.main = pts[0].main * 2;
                    newPt.control1 = newPt.main - pts[0].main / 2;
                    newPt.control2 = newPt.main + pts[0].main / 2;

                    list.Add(newPt);
                }
            }
            else
            {
                if (position == -1) // last
                    position = pts.Length;

                bool getted = false;
                int prev = position - 1;
                int cur = position;

                if (!curve.loop__)
                {
                    if (position == 0)
                    {
                        newPt.main = 2 * pts[0].main - pts[1].main;
                        newPt.control1 = 0.5f * (newPt.main + pts[0].main);
                        newPt.control2 = 2 * newPt.main - newPt.control1;
                        newPt.pointMode = pts[0].pointMode;

                        newPt.Size = pts[0].Size;
                        newPt.CurveUpOrientation = pts[0].CurveUpOrientation;

                        list.Insert(0, newPt);
                        getted = true;
                    }
                    else if (position == pts.Length)
                    {
                        newPt.main = 2 * pts[position - 1].main - pts[position - 2].main;
                        newPt.control1 = 0.5f * (newPt.main + pts[position - 1].main);
                        newPt.control2 = 2 * newPt.main - newPt.control1;
                        newPt.pointMode = pts[position - 1].pointMode;

                        newPt.Size = pts[position - 1].Size;
                        newPt.CurveUpOrientation = pts[position - 1].CurveUpOrientation;

                        list.Add(newPt);
                        getted = true;
                    }
                }
                else
                {
                    if (position == 0 || position == pts.Length)
                    {
                        position = pts.Length;
                        prev = pts.Length - 1;
                        cur = 0;
                    }
                }

                if (!getted)
                {
                    Vector3 H = (pts[prev].control2 + pts[cur].control1) * 0.5f;
                    Vector3 L2 = (pts[prev].main + pts[prev].control2) * 0.5f;
                    Vector3 R3 = (pts[cur].control1 + pts[cur].main) * 0.5f;
                    newPt.pointMode = pts[prev].pointMode;

                    newPt.main = curve.getPos(prev, 0.5f);
                    newPt.control1 = 0.5f * (H + L2);
                    newPt.control2 = 0.5f * (H + R3);

                    newPt.Size = (pts[prev].Size + pts[cur].Size) / 2;
                    newPt.CurveUpOrientation = (pts[prev].CurveUpOrientation + pts[cur].CurveUpOrientation) / 2;

                    // between 2 points, use middle
                    list.Insert(position, newPt);
                }

                curPointIndex = position;
            }

            if (curve.sharePoints != null)
            {
                curve.sharePoints.controlPoints = (CurveControlPoint[])list.ToArray(typeof(CurveControlPoint));
            }
            else
                curve.controlPoints = (CurveControlPoint[])list.ToArray(typeof(CurveControlPoint));

            setChanged();
        }
        #endregion

        #region skinning

        void loadSkin()
        {
            if (styles.isLoaded) return;
            GUI.skin = skin;
            styles.load();
            icons.load();


        }

        public ButtonsIcons icons = new ButtonsIcons();
        public MyGuiStyle styles = new MyGuiStyle();
        #endregion

        #region Scene View Handles

        void drawControlBar()
        {
            PointsMode newPointsMode = (PointsMode)GUILayout.SelectionGrid((int)pointsMode, new GUIContent[]
                    {
                        new GUIContent("Positions"),
                        new GUIContent("Orientation"),
                        new GUIContent("Size"),

                    }, 3, MyGuiStyle.LargeButton);

            if (newPointsMode != pointsMode)
            {
                pointsMode = newPointsMode;
                SceneView.RepaintAll();
            }

            GUILayout.BeginHorizontal();

            //  if (pointsMode == PointsMode.Position)
            if (GUILayout.Button(ButtonsIcons.add, MyGuiStyle.BigSqButton))
            {
                InsertBefore();
            }

            if (GUILayout.Button(ButtonsIcons.prev, MyGuiStyle.BigSqButton))
            {
                selectPrev();
            }

            if (GUILayout.Button(ButtonsIcons.center, MyGuiStyle.BigSqButton))
            {
                centerCurrent();
            }

            // if (pointsMode == PointsMode.Position)
            if (GUILayout.Button(ButtonsIcons.delete, MyGuiStyle.BigSqButton))
            {
                DeleteCurrent();
            }

            if (GUILayout.Button(ButtonsIcons.next, MyGuiStyle.BigSqButton))
            {
                selectNext();
            }

            //     if (pointsMode == PointsMode.Position)
            if (GUILayout.Button(ButtonsIcons.add, MyGuiStyle.BigSqButton))
            {
                InsertAfter();
            }

            GUILayout.EndHorizontal();
        }

        void controlDirectionGUI()
        {
            if (!isPointSelected()) return;

            CurveControlPoint Point = getPoints()[curPointIndex];
            GUILayout.BeginHorizontal();
            GUILayout.Space(75);
            GUILayout.BeginVertical();

            GUILayout.BeginHorizontal();
            if (GUILayout.Button(ButtonsIcons.Mini_U_L, MyGuiStyle.SmallSqButton))
            {
                registerUndo();
                Point.CurveUpOrientation = -45;
                setChanged();
            }
            if (GUILayout.Button(ButtonsIcons.Mini_U, MyGuiStyle.SmallSqButton))
            {
                registerUndo();
                Point.CurveUpOrientation = 0;
                setChanged();
            }
            if (GUILayout.Button(ButtonsIcons.Mini_U_R, MyGuiStyle.SmallSqButton))
            {
                registerUndo();
                Point.CurveUpOrientation = 45;
                setChanged();
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            if (GUILayout.Button(ButtonsIcons.Mini_L, MyGuiStyle.SmallSqButton))
            {
                registerUndo();
                Point.CurveUpOrientation = -90;
                setChanged();
            }
            if (GUILayout.Button(ButtonsIcons.Mini_C, MyGuiStyle.SmallSqButton))
            {

            }
            if (GUILayout.Button(ButtonsIcons.Mini_R, MyGuiStyle.SmallSqButton))
            {
                registerUndo();
                Point.CurveUpOrientation = 90;
                setChanged();
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            if (GUILayout.Button(ButtonsIcons.Mini_D_L, MyGuiStyle.SmallSqButton))
            {
                registerUndo();
                Point.CurveUpOrientation = -135;
                setChanged();
            }
            if (GUILayout.Button(ButtonsIcons.Mini_D, MyGuiStyle.SmallSqButton))
            {
                registerUndo();
                Point.CurveUpOrientation = 180;
                setChanged();
            }
            if (GUILayout.Button(ButtonsIcons.Mini_D_R, MyGuiStyle.SmallSqButton))
            {
                registerUndo();
                Point.CurveUpOrientation = 135;
                setChanged();
            }

            GUILayout.EndHorizontal();

            GUILayout.EndVertical();

            GUILayout.EndHorizontal();
        }


        void controlsPointsSceneGUI()
        {
            if (curve.sharePoints != null)
                return;

            Handles.BeginGUI();

            loadSkin();

            var pts = getPoints();

            GUIStyle framest = MyGuiStyle.getFrame(pointsMode, curve.curve_type__);

            FramePositon positionFrame = new FramePositon(FramePositon.Anchor.TopLeft,
                new Rect(
                    framest.margin.left,
                    framest.margin.top,
                    framest.fixedWidth,
                    framest.fixedHeight));

            Rect rc = positionFrame.MainRc;

            string text = "Points : ";
            if (isPointSelected())
            {
                text += "(" + (curPointIndex + 1) + " / " + pts.Length + ")";
            }
            else
                text += "No Selection (" + pts.Length + ")";

            GUI.Box(rc, text, GUI.skin.box);
            rc = rc.applyPadding(framest);
            GUILayout.BeginArea(rc);

            drawControlBar();

            switch (pointsMode)
            {
                case PointsMode.Position:
                    PointModeGUI();
                    break;

                case PointsMode.Rotation:
                    controlDirectionGUI();
                    break;

                case PointsMode.Size:
                    break;

                default:
                    break;
            }

            GUILayout.EndArea();

            Handles.EndGUI();
        }

        void drawControlPoint(Curve curve, int indexPoint, Vector3 pos)
        {
            Transform tr = curve.transform;

            CurveControlPoint[] pts = getPoints();
            Vector3 posC1 = tr.TransformPoint(pts[indexPoint].control1);
            Vector3 posC2 = tr.TransformPoint(pts[indexPoint].control2);

            //Handles.SphereCap(-1, pos, Quaternion.identity, size);
            Vector3 newMainPos = Handles.PositionHandle(pos, Quaternion.identity);
           


            Vector3 newPosC1 = posC1;
            Vector3 newPosC2 = posC2;
            bool changed = false;

            Handles.color = Color.magenta;
            if (curve.curve_type__ == Curve.CurveType.Bezier)
            {
                if (pts[indexPoint].pointMode != CurveControlPoint.mode.Line)
                {
                    if (curve.bezierType__ == Curve.BezierType.Cubic)
                    {
                        if (curve.loop__ || indexPoint != pts.Length - 1)
                        {
                            newPosC2 = Handles.PositionHandle(posC2, Quaternion.identity);
                            Handles.Label(posC2, "   C2");
                            Handles.DrawLine(posC2, pos);
                        }

                        if (curve.loop__ || indexPoint != 0)
                        {
                            newPosC1 = Handles.PositionHandle(posC1, Quaternion.identity);
                            Handles.Label(posC1, "   C1");
                            Handles.DrawLine(posC1, pos);
                        }
                    }
                    else
                    {
                        if (curve.loop__ || indexPoint != 0)
                        {
                            newPosC1 = Handles.PositionHandle(posC1, Quaternion.identity);
                            Handles.Label(posC1, "   C1");
                            Handles.DrawLine(posC1, pos);
                        }
                        if (indexPoint >= 1)
                        {
                            Handles.DrawLine(posC1, tr.TransformPoint(pts[indexPoint - 1].main));
                        }
                        else if (curve.loop__)
                        {
                            Handles.DrawLine(posC1, tr.TransformPoint(pts[pts.Length - 1].main));
                        }
                    }
                }
            }

            Handles.color = Color.white;

            if (pts[indexPoint].pointMode == CurveControlPoint.mode.Symetric)
            {
                if (posC1 != newPosC1)
                {
                    newPosC2 = 2 * pos - posC1;
                    changed = true;
                }
                else if (posC2 != newPosC2)
                {
                    newPosC1 = 2 * pos - posC2;
                    changed = true;
                }
                else if (pos != newMainPos)
                {
                    Vector3 delta = newMainPos - pos;

                    newPosC2 += delta;
                    newPosC1 += delta;

                    changed = true;
                }
            }
            else
            {
                if (pos != newMainPos)
                {
                    Vector3 delta = newMainPos - pos;
                    newPosC2 += delta;
                    newPosC1 += delta;

                    changed = true;
                }
                else if (posC2 != newPosC2 || posC1 != newPosC1)
                    changed = true;
            }

            if (changed)
            {
                registerUndo();

                Vector3 deltaPos = Vector3.zero;
                if (pos != newMainPos && curve.VerticalRaycast__)
                {
                    deltaPos = cameraRaycastPoint(newMainPos, curve.raycastHeight__);
                }

                pts[indexPoint].main = tr.InverseTransformPoint(newMainPos) + deltaPos;
                pts[indexPoint].control1 = tr.InverseTransformPoint(newPosC1) + deltaPos;
                pts[indexPoint].control2 = tr.InverseTransformPoint(newPosC2) + deltaPos;

                setChanged();
            }
        }

        void drawOrientationPoint(Curve curve, int indexPoint, Vector3 pos)
        {
            //		Transform tr = curve.transform;
            CurveControlPoint[] pts = getPoints();
            float size = HandleUtility.GetHandleSize(pos);

            // direction of the control point
            if (pts[indexPoint].forward != Vector3.zero)
            {
                Handles.color = Color.cyan;

                Vector3 forward = curve.transform.TransformDirection(pts[indexPoint].forward);

                //			Quaternion plane = Quaternion.LookRotation(forward);

                Handles.DrawWireDisc(pos, forward, size * 0.5f);

                float orientation = pts[indexPoint].CurveUpOrientation;

                //Vector3 upDir = Quaternion.LookRotation(pts[indexPoint].forward) * Quaternion.Euler(0, 0, pts[indexPoint].CurveUpOrientation) * Vector3.up;

                Vector3 upDir = Quaternion.LookRotation(pts[indexPoint].forward) * Quaternion.Euler(0, 0, orientation) * Vector3.up;
                Quaternion direction = Quaternion.LookRotation(upDir);

                upDir *= size * 0.5f;

                orientation = (Handles.ScaleValueHandle(
                    (100 + orientation),
                    pos + upDir,
                    direction,
                    size, Handles.ConeHandleCap, 0) - 100);

                if (orientation != pts[indexPoint].CurveUpOrientation)
                {
                    registerUndo();
                    while (orientation > 180)
                        orientation -= 360;
                    while (orientation < -180)
                        orientation += 360;

                    pts[indexPoint].CurveUpOrientation = orientation;
                    setChanged();
                }
            }
        }

        void drawSizePoint(Curve curve, int indexPoint, Vector3 pos)
        {
            CurveControlPoint[] pts = getPoints();
            //  float size = HandleUtility.GetHandleSize(pos);
            Handles.color = Color.red;

            float size = pts[indexPoint].Size;

            Vector3 res = curve.transform.TransformVector(size, size, size);
            float worldSize = res.x;

            Handles.DrawWireDisc(pos, Vector3.up, worldSize);
            Handles.DrawWireDisc(pos, Vector3.left, worldSize);
            Handles.DrawWireDisc(pos, Vector3.forward, worldSize);

            float resultSize = Handles.ScaleValueHandle(
                    size,
                    pos,
                    Quaternion.identity,
                    HandleUtility.GetHandleSize(pos), Handles.CubeHandleCap, 0);

            if (resultSize != pts[indexPoint].Size)
            {
                if (resultSize < 0)
                    resultSize = -resultSize;

                registerUndo();

                pts[indexPoint].Size = resultSize;
                setChanged();
            }
        }

        int lastHotControl = -1;
        void controlPointsHandles(Curve curve)
        {
            Default_Handler_Hidden = true;
            Transform tr = curve.transform;

            CurveControlPoint[] pts = getPoints();

            int hotControl = GUIUtility.hotControl;
            if (hotControl != lastHotControl && hotControl != 0)
            {
                lastHotControl = hotControl;
            }

            Vector3 posCurrent = Vector3.zero;
            for (int indexPoint = 0; indexPoint < pts.Length; indexPoint++)
            {
                Vector3 pos = tr.TransformPoint(pts[indexPoint].main);

                Handles.Label(pos, "   " + indexPoint);

                float size = HandleUtility.GetHandleSize(pos);
               
                Handles.ScaleValueHandle(0, pos, Quaternion.identity, size, (controlID, position, rotation, size2, eventType) =>
                    {
                        if (controlID == lastHotControl && curPointIndex != indexPoint)
                        {
                            curPointIndex = indexPoint;
                            EditorUtility.SetDirty(curve);
                        }
                        Handles.SphereHandleCap(controlID, position, rotation, size2, eventType);
                    }
                    , 0);


                Handles.color = Color.red;
                Handles.DrawLine(pos, pos + pts[indexPoint].forward * size*0.5f);
                Handles.color = Color.white;

                if (curPointIndex == indexPoint)
                {
                    posCurrent = pos;
                }
            }
           
            if (isPointSelected())
            {
                switch (pointsMode)
                {
                    case PointsMode.Position:
                        drawControlPoint(curve, curPointIndex, posCurrent);
                        break;
                    case PointsMode.Rotation:
                        drawOrientationPoint(curve, curPointIndex, posCurrent);
                        break;
                    case PointsMode.Size:
                        drawSizePoint(curve, curPointIndex, posCurrent);
                        break;
                    default:
                        break;
                }

            }
        }

        void setChanged()
        {
            if (curve.sharePoints != null)
                curve.sharePoints.rebuildLine();

            curve.rebuildLine();
            EditorUtility.SetDirty(target);
            //      SceneView.RepaintAll();
        }

        #endregion

        #region main overloaded functions

        // PointsMode newPointMode =

        void OnSceneGUI()
        {
            Vector3[] polyline = curve.WorldPolyLine;
            if (curve.drawGizmos)
            {
                if (curve.drawGizmosPolyline)
                {
                    for (int i = 0; i < polyline.Length; i++)
                    {
                        float size = HandleUtility.GetHandleSize(polyline[i]);
                        Handles.CubeHandleCap(-1, polyline[i], Quaternion.identity, 0.05f * size, Event.current.type);
                    }
                }

                if (curve.drawGizmosControlPoints && editionMode != EditionMode.EditPoints)
                {
                    CurveControlPoint[] pts = getPoints();

                    Handles.color = Color.cyan;
                    for (int i = 0; i < pts.Length; i++)
                    {
                        Vector3 pos = curve.transform.TransformPoint(pts[i].main);

                        float size = HandleUtility.GetHandleSize(pos);

                        Handles.SphereHandleCap(-1, pos, Quaternion.identity, 0.15f * size, Event.current.type);
                    }
                }
                Quaternion[] orientation = null;
                if (curve.drawGizmosOrientation)
                {
                    orientation = curve.getWorldOrientations();
                    for (int i = 0; i < polyline.Length; i++)
                    {
                        float size = HandleUtility.GetHandleSize(polyline[i]);
                        Handles.CubeHandleCap(-1, polyline[i], Quaternion.identity, 0.05f * size, Event.current.type);

                        Vector3 forward = orientation[i] * Vector3.forward;
                        Vector3 right = orientation[i] * Vector3.right;
                        Vector3 up = orientation[i] * Vector3.up;

                        Handles.color = Color.blue;

                        Handles.DrawLine(polyline[i], polyline[i] + forward * size);

                        Handles.color = Color.red;
                        Handles.DrawLine(polyline[i], polyline[i] + right * size);
                        Handles.color = Color.yellow;
                        Handles.DrawLine(polyline[i], polyline[i] + up * size);
                    }
                }



                if (curve.drawGizmosSizes)
                {
                    float[] sizes = curve.Sizes;
                    float lossyScale = curve.transform.lossyScale.magnitude;

                    if (orientation == null)
                        orientation = curve.getWorldOrientations();

                    for (int i = 0; i < polyline.Length; i++)
                    {
                        float size = sizes[i] * lossyScale / 2;

                        Vector3 forward = orientation[i] * Vector3.forward;
                        Handles.color = Color.cyan;

                        Handles.DrawWireDisc(polyline[i], forward, size);
                    }
                }


                if (curve.drawRatios)
                {
                    Handles.color = Color.yellow;
                    float[] ratios = curve.upOrientations;

                    for (int i = 0; i < polyline.Length; i++)
                    {
                        Handles.Label(polyline[i], " \n" + i + " : " + ratios[i]);
                    }
                }

            }

            Handles.color = Color.white;
            Handles.DrawPolyLine(polyline);

            if (editionMode == EditionMode.EditPoints && curve.sharePoints == null)
            {
                Event e = Event.current;
                if (e.type == EventType.KeyDown)
                {
                    switch (e.keyCode)
                    {
                        case KeyCode.E:
                            pointsMode = PointsMode.Rotation;
                            return;

                        case KeyCode.W:
                            pointsMode = PointsMode.Position;
                            return;

                        case KeyCode.R:
                            pointsMode = PointsMode.Size;
                            return;
                    }
                }
                else
                {
                    controlPointsHandles(curve);

                    if (drawSceneViewBar)
                        controlsPointsSceneGUI();
                }
            }
            else
            {
                Default_Handler_Hidden = false;
            }
        }

        override public void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            loadSkin();

            mainGUI();
        }

        #endregion
    }

}