using UnityEditor;
using UnityEngine;
using System.Collections;
using System;
using System.Reflection;
using Curves.Dependencies;

namespace Curves.UI
{
    public class ButtonsIcons
    {
        bool loaded = false;

        static public Texture2D next;
        static public Texture2D prev;
        static public Texture2D center;


        static public Texture2D less;

        static public Texture2D add;
        static public Texture2D delete;
        static public Texture2D rotate;
        static public Texture2D move;
        static public Texture2D flat;
        static public Texture2D line;
        static public Texture2D line2;
        static public Texture2D quad;

        static public Texture2D symetric;
        static public Texture2D broken;

        static public Texture2D Mini_U;
        static public Texture2D Mini_U_L;
        static public Texture2D Mini_U_R;
        static public Texture2D Mini_R;
        static public Texture2D Mini_L;
        static public Texture2D Mini_D;
        static public Texture2D Mini_D_L;
        static public Texture2D Mini_D_R;
        static public Texture2D Mini_C;

        public void load()
        {
            if (!loaded)
            {
                string[] ids = AssetDatabase.FindAssets("CurveEditor");
                if (ids == null) return;

                string path = AssetDatabase.GUIDToAssetPath(ids[0]);
                path = path.Substring(0, path.Length - "CurveEditor.cs".Length) + "Skin/";

                next = AssetDatabase.LoadAssetAtPath<Texture2D>(path + "Buttons/Next.png");
                prev = AssetDatabase.LoadAssetAtPath<Texture2D>(path + "Buttons/Prev.png");
                center = AssetDatabase.LoadAssetAtPath<Texture2D>(path + "Buttons/center.png");

                less = AssetDatabase.LoadAssetAtPath<Texture2D>(path + "Buttons/less.png");
                add = AssetDatabase.LoadAssetAtPath<Texture2D>(path + "Buttons/add.png");

                delete = AssetDatabase.LoadAssetAtPath<Texture2D>(path + "Buttons/delete.png");
                rotate = AssetDatabase.LoadAssetAtPath<Texture2D>(path + "Buttons/rotate.png");

                move = AssetDatabase.LoadAssetAtPath<Texture2D>(path + "Buttons/move.png");
                flat = AssetDatabase.LoadAssetAtPath<Texture2D>(path + "Buttons/flat.png");
                line = AssetDatabase.LoadAssetAtPath<Texture2D>(path + "Buttons/line.png");
                line2 = AssetDatabase.LoadAssetAtPath<Texture2D>(path + "Buttons/line2.png");

                quad = AssetDatabase.LoadAssetAtPath<Texture2D>(path + "Buttons/quad.png");

                symetric = AssetDatabase.LoadAssetAtPath<Texture2D>(path + "Buttons/symetric.png");
                broken = AssetDatabase.LoadAssetAtPath<Texture2D>(path + "Buttons/broken.png");

                Mini_U = AssetDatabase.LoadAssetAtPath<Texture2D>(path + "Buttons/MiniArrows/U.png");
                Mini_U_L = AssetDatabase.LoadAssetAtPath<Texture2D>(path + "Buttons/MiniArrows/UL.png");
                Mini_U_R = AssetDatabase.LoadAssetAtPath<Texture2D>(path + "Buttons/MiniArrows/UR.png");
                Mini_R = AssetDatabase.LoadAssetAtPath<Texture2D>(path + "Buttons/MiniArrows/R.png");
                Mini_L = AssetDatabase.LoadAssetAtPath<Texture2D>(path + "Buttons/MiniArrows/L.png");
                Mini_D = AssetDatabase.LoadAssetAtPath<Texture2D>(path + "Buttons/MiniArrows/D.png");
                Mini_D_L = AssetDatabase.LoadAssetAtPath<Texture2D>(path + "Buttons/MiniArrows/DL.png");
                Mini_D_R = AssetDatabase.LoadAssetAtPath<Texture2D>(path + "Buttons/MiniArrows/DR.png");

                Mini_C = AssetDatabase.LoadAssetAtPath<Texture2D>(path + "Buttons/MiniArrows/C.png");
            }
        }
    }



 
}