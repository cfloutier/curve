

using UnityEngine;

namespace Curves
{

	/// <summary>
	/// a simple class to handle float values like a distance. it is composed of a float input field (that only accept numbers) and a slider
	/// </summary>
	class IntField
	{
		public enum SliderMode
		{
			None,
			Relative,
			Absolut
		}

		public SliderMode sliderMode = SliderMode.None;
		public int step = 0;
        public int min;
        public int max;

		public IntField(string fieldName)
		{
			this.fieldName = fieldName;

            this.min = int.MinValue;
            this.max = int.MaxValue;
           
		}

		public IntField(string fieldName,
			SliderMode sliderMode,
			int min, int max)
		{
			this.fieldName = fieldName;
			this.sliderMode = sliderMode;

            this.min = min;
            this.max = max;

		}

		public IntField(string fieldName,
            int min, int max, int step)
		{
			this.fieldName = fieldName;
			this.sliderMode = SliderMode.Absolut;
            this.min = min;
            this.max = max;
            this.step = step;
		}


		public string fieldName;

        string strText = "";
		int lastValue;


        public bool LayoutField(ref int value)
		{
			GUILayout.BeginHorizontal();
            GUILayout.Label(fieldName , GUILayout.ExpandWidth(false));

            bool changed = false;
			if (lastValue != value || strText == "")
			{
				jogval = 1;
				strText = value.ToString();
				jogCenterValue = value;
				lastValue = value;
			}

			string newValueStr = GUILayout.TextField(strText, GUILayout.Width(100));
			if (newValueStr != strText)
			{
				int newValue;
				if (int.TryParse(newValueStr, out newValue))
				{
					strText = newValueStr;
					if (newValue != value)
					{
						lastValue = value = newValue;
						jogCenterValue = lastValue;
						jogval = 1;
						changed = true;

                        if (value < min)
                            value = min;
                        if (value > max)
                            value = max;

					
					}
				}
			}

			switch (sliderMode)
			{
				case SliderMode.None:
					break;
				case SliderMode.Relative:
					if (Jog(ref value))
						return true;
					break;
				case SliderMode.Absolut:
					if (slider(ref value))
						return true;
					break;
			}

			GUILayout.EndHorizontal();
			return changed;
		}


		float jogval = 1;
		int jogCenterValue;

		bool Jog(ref int value)
		{
			float newJogVal = GUILayout.HorizontalSlider(jogval, 0, 2f, GUILayout.ExpandWidth(true));

          //  GUIUtility.GetControlID();
			if (newJogVal != jogval)
			{
				jogval = newJogVal;
                int newValue = value;
				if (jogval < 1)
				{
                    newValue = (int) (jogCenterValue * jogval);
				}
				else if (jogval == 1)
				{
                    newValue = jogCenterValue;
                }
				else
				{
                    newValue = (int) (jogCenterValue * jogval);				
				}

                if (newValue < min)
                    newValue = min;
                else if (newValue > max)
                    newValue = max;

                if (newValue != value)
                {
                    lastValue = value = newValue;
                    strText = value.ToString();
                    return true;
                }

                return false;
            }

            if (GUILayout.Button(" C ", GUILayout.Width(30)))
            {
				jogCenterValue = value;
				jogval = 1;
			}

			return false;
		}

		bool slider(ref int value)
		{
			GUIStyle st = GUI.skin.FindStyle("General.MiniButton");


			float last = value;
			if (step != 0)
			{
				if (GUILayout.Button(GUI.skin.FindStyle("Buttons.less").normal.background, st))
				{
					value = value - step;
				}
			}

			value =(int)  GUILayout.HorizontalSlider(value, min, max);

			if (step != 0)
			{
				if (GUILayout.Button(GUI.skin.FindStyle("Buttons.add").normal.background, st))
				{
					value = value + step;
				}
			}

			if (value != last)
			{
				strText = value.ToString();
				lastValue = value;
				return true;
			}
			return false;
		}
	}

}