

using UnityEngine;


namespace Curves.UI
{

	/// <summary>
	/// a simple class to handle float values like a distance. it is composed of a float input field (that only accept numbers) and a slider
	/// </summary>
	class FloatField
	{
		public enum SliderMode
		{
			None,
			Relative,
			Absolut
		}

		public SliderMode sliderMode = SliderMode.None;
		public float step = 0;

		public FloatField(string fieldName)
		{
			this.fieldName = fieldName;

			this.minMax = new Vector2(float.MinValue, float.MaxValue);
		}

		public FloatField(string fieldName,
			SliderMode sliderMode,
			Vector2 minMax)
		{
			this.fieldName = fieldName;
			this.sliderMode = sliderMode;
			this.minMax = minMax;
		}

		public FloatField(string fieldName,
			Vector2 minMax, float step)
		{
			this.fieldName = fieldName;
			this.sliderMode = SliderMode.Absolut;
			this.minMax = minMax;
			this.step = step;
		}

		public string fieldName;

		string strText = "";
		Vector2 minMax;
		float lastValue;


        public bool LayoutField(ref float value)
		{
			GUILayout.BeginHorizontal();
			GUILayout.Label(fieldName, GUILayout.ExpandWidth(false));

			bool changed = false;
			if (lastValue != value || strText == "")
			{
				jogval = 1;
				strText = value.ToString();
				jobCenterValue = value;
				lastValue = value;
			}

			string newValueStr = GUILayout.TextField(strText, GUILayout.Width(100));
			if (newValueStr != strText)
			{
				float newValue;
				if (float.TryParse(newValueStr, out newValue))
				{
					strText = newValueStr;
					if (newValue != value)
					{
						lastValue = value = newValue;
						jobCenterValue = lastValue;
						jogval = 1;
						changed = true;
						value = Mathf.Clamp(value, minMax.x, minMax.y);
					}
				}
			}

			switch (sliderMode)
			{
				case SliderMode.None:
					break;
				case SliderMode.Relative:
					if (Jog(ref value))
						return true;
					break;
				case SliderMode.Absolut:
					if (slider(ref value))
						return true;
					break;
			}

			GUILayout.EndHorizontal();
			return changed;
		}


		float jogval = 1;
		float jobCenterValue;

		bool Jog(ref float value)
		{
			float newVal = GUILayout.HorizontalSlider(jogval, 0, 2, GUILayout.ExpandWidth(true));
			if (newVal != jogval)
			{
				jogval = newVal;
				if (jogval < 1)
				{
					value = jobCenterValue * jogval;
					lastValue = value;
				}
				else if (jogval == 1)
				{
					return false;
				}
				else
				{
					value = jobCenterValue * jogval * jogval;
					lastValue = value;
				}

				strText = value.ToString();
				return true;
			}

			if (GUILayout.Button(" C ", GUILayout.Width(30)))
			{
				jobCenterValue = value;
				jogval = 1;
			}

			return false;
		}

		bool slider(ref float value)
		{
			float last = value;
			if (step != 0)
			{
				if (GUILayout.Button(ButtonsIcons.less, MyGuiStyle.SmallSqButton))
				{
					value = value - step;
				}
			}

			value = GUILayout.HorizontalSlider(value, minMax.x, minMax.y);

			if (step != 0)
			{
				if (GUILayout.Button(ButtonsIcons.add, MyGuiStyle.SmallSqButton))
				{
					value = value + step;
				}
			}

			if (value != last)
			{
				strText = value.ToString();
				lastValue = value;
				return true;
			}
			return false;
		}
	}

}