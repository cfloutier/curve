using UnityEditor;
using UnityEngine;
using System.Collections;
using System;
using System.Reflection;
using Curves.Dependencies;

namespace Curves.UI
{
    public class MyGuiStyle
    {
        static public MyGuiStyle i;


        public static GUIStyle LargeButton;

        public static GUIStyle Title;
        public static GUIStyle BigSqButton;
        public static GUIStyle SmallSqButton;


        public static GUIStyle FramePosition;
        public static GUIStyle FramePositionBezier;
        public static GUIStyle FrameOrientation;
        public static GUIStyle FrameSize;

        public static GUIStyle getFrame(CurveEditor.PointsMode mode, Curve.CurveType curveType)
        {
            switch (mode)
            {
                default:
                case CurveEditor.PointsMode.Position: 
                    if (curveType == Curve.CurveType.Bezier)
                        return FramePositionBezier;
                    else
                        return FramePosition;
                case CurveEditor.PointsMode.Rotation: return FrameOrientation;
                case CurveEditor.PointsMode.Size: return FrameSize;
            }


        }

        public bool isLoaded
        {
            get
            {
                return Title != null;
            }
            
        }

        public void load()
        {
            Title = new GUIStyle(GUI.skin.label);
            Title.fontStyle = FontStyle.Bold;
            Title.fontSize = (int)(Title.fontSize * 2f);
            Title.margin.bottom = 10;
            Title.margin.top = 20;

            LargeButton = new GUIStyle(GUI.skin.button);
            LargeButton.fixedHeight = 25;
            LargeButton.margin.bottom = 10;
            
            BigSqButton = new GUIStyle(GUI.skin.button);
            BigSqButton.normal.background = null;

            BigSqButton.fixedWidth = 34;
            BigSqButton.fixedHeight = 34;
            BigSqButton.padding = new RectOffset(2, 2, 2, 2);

            SmallSqButton = new GUIStyle(GUI.skin.button);
            SmallSqButton.normal.background = null;
            SmallSqButton.fixedWidth = 25;
            SmallSqButton.fixedHeight = 25;
            SmallSqButton.padding = new RectOffset(0, 0, 0, 0);

            FramePosition = new GUIStyle(GUI.skin.box);
            FramePosition.fixedWidth = 240;
            FramePosition.fixedHeight = 100;
            FramePosition.padding = new RectOffset(2, 2, 20, 2);

            FramePositionBezier = new GUIStyle(GUI.skin.box);
            FramePositionBezier.fixedWidth = 240;
            FramePositionBezier.fixedHeight = 140;
            FramePositionBezier.padding = new RectOffset(2, 2, 20, 2);

            FrameOrientation = new GUIStyle(GUI.skin.box);
            FrameOrientation.fixedWidth = 240;
            FrameOrientation.fixedHeight = 180;
            FrameOrientation.padding = new RectOffset(2, 2, 20, 2);

            FrameSize = new GUIStyle(GUI.skin.box);
            FrameSize.fixedWidth = 240;
            FrameSize.fixedHeight = 100;
            FrameSize.padding = new RectOffset(2, 2, 20, 2);

        }
    }



}