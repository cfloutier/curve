using UnityEditor;
using UnityEngine;
using System.Collections;
using System;
using System.Reflection;
using System.Globalization;

namespace Curves.UI
{
    [CustomEditor(typeof(CurveRenderer))]
    /// <summary>
    /// Bezier curve line editor directly usable into the scene view 
    /// 
    /// There is too part where the curve is edited
    /// - In the scene view, where points can be added, moved deleted and so on
    /// - in the inspector, 
    /// </summary>
    public class CurveRendererEditor : Editor
    {
        public SerializedProperty type;
        public SerializedProperty colors;
        public SerializedProperty sides;

        public SerializedProperty useCanvasRenderer;
        public SerializedProperty addCollider;
        public SerializedProperty widthMethod;

        public SerializedProperty minWidth;
        public SerializedProperty maxWidth;

        public SerializedProperty widthCurve;

        public SerializedProperty normalizedUV;

        public SerializedProperty UvAdaptation;
        public SerializedProperty UVBorders;


        public SerializedProperty material;
        public SerializedProperty drawGizmos;

        public SerializedProperty focusOnTriangle;
        public SerializedProperty checkTrianglesInversion;

        public SerializedProperty NbTriangleCorrection;
        public SerializedProperty NbFaces;
        public SerializedProperty DeltaPos;
        
        public SerializedProperty Angle;
        public SerializedProperty FullRing;
        public SerializedProperty StartAngle;
        public SerializedProperty EndAngle;


        void OnEnable()
        {
            type = serializedObject.FindProperty("type");

            useCanvasRenderer = serializedObject.FindProperty("useCanvasRenderer");
            addCollider = serializedObject.FindProperty("addCollider");
            widthMethod = serializedObject.FindProperty("widthMethod");

            minWidth = serializedObject.FindProperty("minWidth");
            maxWidth = serializedObject.FindProperty("maxWidth");

            colors = serializedObject.FindProperty("colors");
            sides = serializedObject.FindProperty("sides");
            widthCurve = serializedObject.FindProperty("widthCurve");

            normalizedUV = serializedObject.FindProperty("normalizedUV");
            UvAdaptation = serializedObject.FindProperty("UvAdaptation");
            UVBorders = serializedObject.FindProperty("UVBorders");

            material = serializedObject.FindProperty("material");

            drawGizmos = serializedObject.FindProperty("drawGizmos");
            focusOnTriangle = serializedObject.FindProperty("focusOnTriangle");

            checkTrianglesInversion = serializedObject.FindProperty("checkTrianglesInversion");
            NbTriangleCorrection = serializedObject.FindProperty("NbTriangleCorrection");

            NbFaces = serializedObject.FindProperty("NbFaces");
            DeltaPos = serializedObject.FindProperty("deltaPos");
            Angle = serializedObject.FindProperty("Angle");
            FullRing = serializedObject.FindProperty("FullRing");
            StartAngle = serializedObject.FindProperty("StartAngle");
            EndAngle = serializedObject.FindProperty("EndAngle");
        }

        void OnDisable()
        {

        }

        GUIStyle h;
        GUIStyle Header
        {
            get
            {
                if (h == null)
                {
                    h = new GUIStyle();

                    h.fontStyle = FontStyle.Bold;
                    h.fontSize = 16;

                    h.normal.textColor = GUI.skin.label.normal.textColor;
                    h.padding = new RectOffset(30, 10, 10, 0);
                }

                return h;
            }
        }

        void HeaderField(string txt)
        {
            GUILayout.Label(txt, Header);
            GUILayout.Label("", GUI.skin.box, GUILayout.Height(2), GUILayout.ExpandWidth(true));
        }

        void materialGUI()
        {
            HeaderField("Material");
            EditorGUILayout.PropertyField(material, new GUIContent("Material"), true);
            GUILayout.Label("Colors are used for color per vertex mode. Only few shader uses it");

            EditorGUILayout.PropertyField(colors, new GUIContent("colors"), true);
        }

        void meshGUI()
        {
            HeaderField("Mesh");
            
            EditorGUILayout.PropertyField(widthMethod, new GUIContent("Width Method"), true);

            EditorGUILayout.PropertyField(useCanvasRenderer, new GUIContent("Use Canvas Renderer (UI)"), true);

            EditorGUILayout.PropertyField(addCollider, new GUIContent("Add A Mesh Collider"), true);

            CurveRenderer.WidthMethod v = (CurveRenderer.WidthMethod)widthMethod.enumValueIndex;

            if (v == CurveRenderer.WidthMethod.Curve)
            {
                GUILayout.Label("Width is set using a curve and min max values");

                EditorGUILayout.PropertyField(minWidth, new GUIContent("Min Width"), true);
                EditorGUILayout.PropertyField(maxWidth, new GUIContent("Max Width"), true);

                EditorGUILayout.PropertyField(widthCurve, new GUIContent("Width Curve"), true);
            }
            else
            {
                GUILayout.Label("Width is set using a Control Point's size values\nUse curve Editor to set them or by script");
            }

            EditorGUILayout.PropertyField(sides, new GUIContent("Sides Mode"), true);
        }

        void UVOptionsGUI(bool line)
        {
            HeaderField("UV Options");
        
            EditorGUILayout.PropertyField(normalizedUV, new GUIContent("Tiled Mapping"), true);

            if (normalizedUV.boolValue)
            {
                GUILayout.Label("The texture is mapped to the entire curve.\nUse material's tiling value to adjust if needed");

                if (line)
                    EditorGUILayout.Slider(UVBorders, 0, 0.5f, new GUIContent("Borders - Constant UV"));
            }

            if (!normalizedUV.boolValue)
            {
                GUILayout.Label("The texture is mapped using real distances.\nAdjust using this value. This mode is longer to compute");
                EditorGUILayout.PropertyField(UvAdaptation, new GUIContent("UV adaptation (in scene unit)"), true);
            }
        }

        override public void OnInspectorGUI()
        {
            // Update the serializedProperty - always do this in the beginning of OnInspectorGUI.
            serializedObject.Update();

            EditorGUILayout.PropertyField(type, new GUIContent("Type"), true);

            CurveRenderer.Type t = (CurveRenderer.Type)type.enumValueIndex;

            switch (t)
            {
                case CurveRenderer.Type.BillboardLine:

                    GUILayout.Label("Width can only be set with a linear interpolation between 2 values");

                    EditorGUILayout.PropertyField(minWidth, new GUIContent("Start Width"), true);
                    EditorGUILayout.PropertyField(maxWidth, new GUIContent("End Width"), true);

                    EditorGUILayout.PropertyField(material, new GUIContent("Material"), true);

                    GUILayout.Label("Colors are used for color per vertex mode. Only few shader uses it");
                    GUILayout.Label("only the first 2 colors are used in this mode");

                    EditorGUILayout.PropertyField(colors, new GUIContent("colors"), true);

                    break;

                case CurveRenderer.Type.Steps:
                    materialGUI();
                    meshGUI();
                    break;
                case CurveRenderer.Type.FlatLine:

                    materialGUI();
                    meshGUI();

                    UVOptionsGUI(true);

                    EditorGUILayout.PropertyField(checkTrianglesInversion, new GUIContent("Check triangle inversions"), true);

                    if (checkTrianglesInversion.boolValue)
                    {
                        GUILayout.Label("Nb of succesives triangles to check");

                        EditorGUILayout.PropertyField(NbTriangleCorrection, new GUIContent("Nb Triangles"), true);
                    }


                    EditorGUILayout.PropertyField(drawGizmos, new GUIContent("drawGizmos"), true);

                    if (drawGizmos.boolValue)
                    {
                        GUILayout.Label("index of the triangle to focus on, -1 for no focus");

                        EditorGUILayout.PropertyField(focusOnTriangle, new GUIContent("focus On Triangle"), true);
                    }
                    break;
                case CurveRenderer.Type.Extrusion:
                    {
                        materialGUI();
                        meshGUI();
                        UVOptionsGUI(false);

                        HeaderField("Extrusion");
                       

                        EditorGUILayout.PropertyField(NbFaces, new GUIContent("Nb faces"), true);
                        EditorGUILayout.PropertyField(Angle, new GUIContent("Angle"), true);
                        EditorGUILayout.PropertyField(DeltaPos, new GUIContent("Delta Pos"), true);

                        EditorGUILayout.PropertyField(FullRing, new GUIContent("Full Ring"), true);

                        if (!FullRing.boolValue)
                        {
                            EditorGUILayout.PropertyField(StartAngle, new GUIContent("Start"), true);
                            EditorGUILayout.PropertyField(EndAngle, new GUIContent("End"), true);
                        }

               //         EditorGUILayout.PropertyField(drawGizmos, new GUIContent("drawGizmos"), true);
                    }
                    break;

            }

            serializedObject.ApplyModifiedProperties();

            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("CleanUp"))
            {
                ((CurveRenderer)target).CleanUp();
            }
        
            if (GUILayout.Button("Rebuild"))
            {
                ((CurveRenderer)target).Rebuild();
            }
            EditorGUILayout.EndHorizontal();


        }
    }
}


