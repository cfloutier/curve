﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Reflection;
using System.Linq;
using System;

namespace Curves.Attributes.Drawers
{
    // The property drawer class should be placed in an editor script, inside a folder called Editor.
    [CustomPropertyDrawer(typeof(VisibleAttribute))]
    public class VisibleDrawer : PropertyDrawer
    {
        const float space = 5;

        // Draw the property inside the given rect
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (isValid(property))
                EditorGUI.PropertyField(position, property, label, true);
        }

        public bool isValid(SerializedProperty property)
        {
            return isValid(property, attribute as VisibleAttribute, 0);
        }

        public static bool isValid(SerializedProperty property, VisibleAttribute att)
        {
            return isValid(property, att, 0);
        }
        public static object GetValue(object source, string name, int index)
        {
            var enumerable = GetValue(source, name) as IEnumerable;
            var enm = enumerable.GetEnumerator();
            while (index-- >= 0)
                enm.MoveNext();
            return enm.Current;
        }

        public static object GetValue(object source, string name)
        {
            if (source == null)
                return null;
            var type = source.GetType();
            var f = type.GetField(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
            if (f == null)
            {
                var p = type.GetProperty(name, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance | BindingFlags.IgnoreCase);
                if (p == null)
                    return null;
                return p.GetValue(source, null);
            }
            return f.GetValue(source);
        }
        public static object[] GetParentsObjects(UnityEditor.SerializedProperty prop)
        {
            var path = prop.propertyPath.Replace(".Array.data[", "[");
            UnityEngine.Object[] objs = prop.serializedObject.targetObjects;
            object[] resultArray = new object[objs.Length];
            var elements = path.Split('.');

            for (int i = 0; i < objs.Length; i++)
            {
                System.Object obj = objs[i];
                foreach (var element in elements.Take(elements.Length - 1))
                {
                    if (element.Contains("["))
                    {
                        var elementName = element.Substring(0, element.IndexOf("["));
                        var index = Convert.ToInt32(element.Substring(element.IndexOf("[")).Replace("[", "").Replace("]", ""));
                        obj = GetValue(obj, elementName, index);
                        resultArray[i] = obj;
                    }
                    else
                    {
                        obj = GetValue(obj, element);

                        resultArray[i] = obj;
                    }

                }
            }

            return objs;
        }

        public static bool isValid(SerializedProperty property, VisibleAttribute att, int index)
        {
            if (index < 0 || index > att.BooleanMembers.Length)
                return !att.invert;

            object[] objs = GetParentsObjects(property);
            if (objs == null)
                return !att.invert;

            att.init(objs[0].GetType());

            if (att.methods == null || index < 0 || index >= att.methods.Length)
                return !att.invert;


            bool visible = false;
            for (int i = 0; i < objs.Length; i++)
            {
                if (att.methods[index] != null)
                    visible |= (bool)att.methods[index].Invoke(objs[i], null);
                else if (att.fields[index] != null)
                    visible |= (bool)att.fields[index].GetValue(objs[i]);
                else
                    return true; // if nothing is set, it is then visible or active
            }


            return att.invert ? !visible : visible;
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            if (isValid(property))
                return EditorGUI.GetPropertyHeight(property);

            return 0;
        }
    }

  
}