using Curves.Tools;
using UnityEditor;
using UnityEngine;


namespace Curves.UI
{
    public class DispatcherEditor : Editor
    {
        public void OnInspectorGUI(Dispatcher dispatcher)
        {
            base.OnInspectorGUI();

            GUILayout.BeginHorizontal();

            if (GUILayout.Button("Add Object", GUILayout.Height(30)))
            {
                dispatcher.Build();
            }

            if (GUILayout.Button("Clean Up", GUILayout.Height(30)))
            {
                dispatcher.CleanUp();
            }

            if (GUILayout.Button("Clean & Add", GUILayout.Height(30)))
            {
                dispatcher.CleanupAndBuild();
            }

            GUILayout.EndHorizontal();
        }
    }

    [CustomEditor(typeof(CurveDispatch))]
    public class CurveDispatcherEditor : DispatcherEditor
    {
        override public void OnInspectorGUI()
        {
            base.OnInspectorGUI(target as CurveDispatch);
        }
    }

    [CustomEditor(typeof(NonCurveDispatch))]
    public class NonCurveDispatchEditor : DispatcherEditor
    {
        override public void OnInspectorGUI()
        {
            base.OnInspectorGUI(target as NonCurveDispatch);
        }
    }
}