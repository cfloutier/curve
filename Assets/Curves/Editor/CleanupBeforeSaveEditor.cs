using Curves.Tools;
using UnityEditor;
using UnityEngine;


namespace Curves.UI
{
	[CustomEditor(typeof(CleanupBeforeSave))]
	public class CleanupBeforeSaveEditor : Editor
	{

		override public void OnInspectorGUI()
		{
			base.OnInspectorGUI();

            CleanupBeforeSave cleaner = (CleanupBeforeSave)target;


			GUILayout.BeginHorizontal();

			if (GUILayout.Button("Rebuild Now", GUILayout.Height(30)))
			{
                cleaner.RebuildNow();
			}

			if (GUILayout.Button("Clean Up Now", GUILayout.Height(30)))
			{
                cleaner.CleanUp();
			}


			GUILayout.EndHorizontal();
		}
	}
}