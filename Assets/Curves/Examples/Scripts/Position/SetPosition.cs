﻿using Curves.Tools;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Curve.Examples.Position
{
    public class SetPosition : MonoBehaviour
    {
        public Slider slider;
        public MoveOnPath mover;

        private void Start()
        {
            slider.onValueChanged.AddListener(onChanged);
        }

        private void onChanged(float pos)
        {
            mover.progress = pos;
        }
    }

}