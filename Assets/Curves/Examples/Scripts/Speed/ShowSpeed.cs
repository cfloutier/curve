﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Curve.Examples.Speed
{
    public class ShowSpeed : MonoBehaviour
    {
        public Text text;
        public VelocityEstimator velocity;

        // Update is called once per frame
        void Update()
        {
            float speed = velocity.GetVelocityEstimate().magnitude * 3.6f;
            text.text = speed.ToString("0.00") + " Km/h";
        }
    }
}