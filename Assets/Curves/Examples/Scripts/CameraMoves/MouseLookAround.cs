using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

namespace Curve.Examples.CameraMoves
{

    /*
    Very simple look around cmera. just change the rotation depending on mouse moves
     */

    public class MouseLookAround : MonoBehaviour
    {
        public bool mouseDownToRotate = true;

        public float camSens = 0.25f; //How sensitive it with mouse

        bool dragin = false;

        void Update()
        {

            if (mouseDownToRotate)
            {
                if (dragin)
                {
                    if (Input.GetMouseButtonUp(0))
                    {
                        dragin = false;
                        return;
                    }
                }
                else
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        if (EventSystem.current == null || !EventSystem.current.IsPointerOverGameObject())
                            dragin = true;

                    }
                    else
                        return;
                }
            }





            Vector3 euler = transform.localEulerAngles;

            float deltay = Input.GetAxis("Mouse Y");
            float deltax = Input.GetAxis("Mouse X");
            euler += new Vector3(-deltay, deltax, 0) * camSens * 10;
            while (euler.x > 180)
                euler.x -= 360;

            if (euler.x > 80)
                euler.x = 80;

            if (euler.x < -80)
                euler.x = -80;

            transform.localEulerAngles = euler;



        }



    }
}

